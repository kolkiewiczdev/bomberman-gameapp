package game.bomber.common;

import java.io.Serializable;

/**
 * @author KOlkiewicz
 */
public class PlayerSimple implements Serializable{

    private int id;
    private String username;

    public PlayerSimple(int id, String username) {
        this.id = id;
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }
}
