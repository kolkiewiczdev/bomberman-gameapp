package game.bomber.common;

/**
 * @author KOlkiewicz
 */
public class Position {
    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void up() {
        --y;
    }

    public void down() {
        ++y;
    }

    public void left() {
        --x;
    }

    public void right() {
        ++x;
    }

    public boolean canUp() {
        if (y==0)
            return false;
        return true;
    }

    public boolean canDown() {
        if (y==GlobalConf.HEIGHT-1)
            return false;
        return true;
    }

    public boolean canLeft() {
        if (x==0)
            return false;
        return true;
    }

    public boolean canRight() {
        if (x==GlobalConf.WITHD-1)
            return false;
        return true;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (x != position.x) return false;
        if (y != position.y) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
