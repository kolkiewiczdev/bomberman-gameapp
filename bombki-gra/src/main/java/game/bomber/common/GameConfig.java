package game.bomber.common;

import java.io.Serializable;

/**
 * @author KOlkiewicz
 */
public class GameConfig implements Serializable{

    private int SpeedPUNumber;
    private int BombRangePUNumber;
    private int ExtraLifePUNumber;
    private int IndestructibilityPUNumber;
    private int BombNumberPUNumber;
    private long maxTime;
    private int seed;

    public GameConfig() {
        SpeedPUNumber = GlobalConf.SPEED_PU_NUM;
        BombRangePUNumber = GlobalConf.BOMB_RANGE_PU_NUM;
        ExtraLifePUNumber = GlobalConf.EXTRA_LIFE_PU_NUM;
        IndestructibilityPUNumber = GlobalConf.INDESTRUCTIBILITY_PU_NUM;
        BombNumberPUNumber = GlobalConf.BOMB_NUMBER_PU_NUM;
        maxTime = GlobalConf.MAX_TIME;
        seed = 0;
    }

    public GameConfig(int speedPUNumber, int bombRangePUNumber, int extraLifePUNumber, int indestructibilityPUNumber, int bombNumberPUNumber, long maxTime, int seed) {
        SpeedPUNumber = speedPUNumber;
        BombRangePUNumber = bombRangePUNumber;
        ExtraLifePUNumber = extraLifePUNumber;
        IndestructibilityPUNumber = indestructibilityPUNumber;
        BombNumberPUNumber = bombNumberPUNumber;
        this.maxTime = maxTime;
        this.seed = seed;
    }

    public int getSpeedPUNumber() {
        return SpeedPUNumber;
    }

    public int getBombRangePUNumber() {
        return BombRangePUNumber;
    }

    public int getExtraLifePUNumber() {
        return ExtraLifePUNumber;
    }

    public int getIndestructibilityPUNumber() {
        return IndestructibilityPUNumber;
    }

    public int getBombNumberPUNumber() {
        return BombNumberPUNumber;
    }

    public long getMaxTime() {
        return maxTime;
    }

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }

}