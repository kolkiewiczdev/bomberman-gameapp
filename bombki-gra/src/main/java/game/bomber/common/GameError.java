package game.bomber.common;

/**
 * @author KOlkiewicz
 */
public class GameError extends Exception {
    public GameError() {
        super();
    }

    public GameError(String message) {
        super(message);
    }

    public GameError(String message, Throwable cause) {
        super(message, cause);
    }
}
