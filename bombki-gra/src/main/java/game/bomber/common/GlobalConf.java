package game.bomber.common;

/**
 * @author KOlkiewicz
 */
public class GlobalConf {

    // domyslne parametry gracza
    public final static int LIFES = 3;
    public final static int[] SPEED = {100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 8, 5, 2};
    //public final static int[] SPEED = {1000, 750, 500, 350, 250, 200, 170, 150, 140, 130, 120, 110, 100};

    public final static int MAX_BOMBS = 1;
    public final static int BOMB_RANGE = 2;
    // public final static int BOMB_RANGE = 1;
    
    // 1 jednostka = 20ms
    public final static long INDESTRUCTIBILITY_TIME = 3*50;
    public final static long INDESTRUCTIBILITY_POWERUP_TIME = 5*50;

    // rozmiar mapy
    public final static int WITHD = 14;
    public final static int HEIGHT = 14;

    // pozycje poczatkowe
    public final static int[][] START_XY = {{0,0}, {WITHD-1,HEIGHT-1},{WITHD-1,0},{0,HEIGHT-1}};

    // czas do wybuchu
    public final static long EXPLODE_TIME = 2*1000;
    //public final static long EXPLODE_TIME = 3*50;

    // domyslne paramenty pojedynczej gry
    public final static int SPEED_PU_NUM = 6;
    public final static int BOMB_RANGE_PU_NUM = 6;
    public final static int EXTRA_LIFE_PU_NUM = 2;
    public final static int INDESTRUCTIBILITY_PU_NUM = 6;
    public final static int BOMB_NUMBER_PU_NUM = 6;
    public final static long MAX_TIME = 0;

    // domysle parametry zawodow
    public final static int MAX_ROUNDS = 9;
    public final static int WINS = 3;

    // opis 1/8 mapy, kazdy bit to informacja czy blok jest zniszczalny czy nie
    public final static int[] MAPS= {0, 3671393, 4210881, 4215848, 3817601};
    // ile bitow opisuje 1/8 mapy
    public final static int MAP_BITS= 24;

}
