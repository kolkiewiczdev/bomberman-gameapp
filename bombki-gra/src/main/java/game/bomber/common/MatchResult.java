package game.bomber.common;

import java.util.List;

/**
 * @author KOlkiewicz
 */
public class MatchResult {
    public static class PlayerResult {
        private PlayerSimple playerSimple;
        private int roundWins;
        private int position;

        public PlayerResult(PlayerSimple playerSimple, int roundWins, int position) {
            this.playerSimple = playerSimple;
            this.roundWins = roundWins;
            this.position = position;
        }

        public PlayerSimple getPlayerSimple() {
            return playerSimple;
        }

        public int getRoundWins() {
            return roundWins;
        }

        public int getPosition() {
            return position;
        }
    }


    private List<PlayerResult> playerResults;

    public MatchResult(List<PlayerResult> playerResults) {
        this.playerResults = playerResults;
    }

    public List<PlayerResult> getPlayerResults() {
        return playerResults;
    }

    public PlayerSimple getWinner() {
        for(PlayerResult pr : playerResults)
            if(pr.getPosition()==1)
                return pr.getPlayerSimple();
        return null;
    }
}
