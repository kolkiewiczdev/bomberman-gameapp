package game.bomber.common;

import java.io.Serializable;


/**
 * @author KOlkiewicz
 */
public class MatchConfig implements Serializable{

    private int maxRound;
    private int wins;
    private GameConfig gameConfig;

    public MatchConfig(int maxRound, int wins, GameConfig gameConfig) {
        this.maxRound = maxRound;
        this.wins = wins;
        this.gameConfig = gameConfig;
    }

    public int getMaxRound() {
        return maxRound;
    }

    public int getWins() {
        return wins;
    }

    public GameConfig getGameConfig() {
        return gameConfig;
    }
}