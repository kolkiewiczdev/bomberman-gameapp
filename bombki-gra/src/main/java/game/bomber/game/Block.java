package game.bomber.game;

/**
 * @author KOlkiewicz
 */
public class Block {
    private boolean destructible;

    public Block() {
        destructible = true;
    }

    public Block(boolean destructible) {
        this.destructible = destructible;
    }

    public boolean isDestructible() {
        return destructible;
    }

    @Override
    public boolean equals(Object object) {
        if ( this == object ) {
            return true;
        }
        if ( object == null || getClass() != object.getClass() ) {
            return false;
        }

        Block block = (Block) object;

        if ( destructible != block.isDestructible() ) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ( destructible ? 1 : 0 );
        return result;
    }
}