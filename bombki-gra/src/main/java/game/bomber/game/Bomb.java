package game.bomber.game;

import game.bomber.common.GlobalConf;
import game.bomber.common.Position;

/**
 * @author KOlkiewicz
 */
public class Bomb {

    private long explodeTime;
    private Player owner;
    private Position position;

    public Bomb(long currentTime, Player owner, Position position) {
        this.explodeTime = currentTime + GlobalConf.EXPLODE_TIME;
        this.owner = owner;
        this.position = position;
    }

    public long getExplodeTime() {
        return explodeTime;
    }

    public Player getOwner() {
        return owner;
    }

    public Position getPosition() {
        return position;
    }



}