package game.bomber.game;

import game.bomber.common.GlobalConf;
import game.bomber.common.Position;

import java.security.InvalidParameterException;

/**
 * @author KOlkiewicz
 */
public class Player {

    private int id;
    private String username;
    private int lifes;
    private int speed;
    private int maxBombs;
    private int bombs;
    private int bombRange;
    private Position position;
    private boolean indestructibility;
    private long indestructibilityTime;
    private int playerNum;
    private boolean live;
    private boolean inMove;
    private long startsMove;

    public Player(int id, String username, int playerNum) {
        if (username == null || username.equals(""))
            throw new InvalidParameterException("BRAK NAZWY GRACZA");
        if (playerNum < 0 || playerNum > 3)
            throw new InvalidParameterException("playerNum musi byc w przedziale 0-3");

        live = true;
        this.id = id;
        this.playerNum = playerNum;

        this.username = username;
        position = new Position(GlobalConf.START_XY[playerNum][0], GlobalConf.START_XY[playerNum][1]);

        speed = 0;
        lifes = GlobalConf.LIFES;
        maxBombs= GlobalConf.MAX_BOMBS;
        bombs = 0;
        bombRange = GlobalConf.BOMB_RANGE;
        indestructibility = true;
        indestructibilityTime = GlobalConf.INDESTRUCTIBILITY_TIME;
        inMove = false;
        startsMove = 0;
    }


    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public int getLifes() {
        return lifes;
    }

    public int getSpeed() {
        return speed;
    }

    public int getMaxBombs() {
        return maxBombs;
    }

    public int getBombs() {
        return bombs;
    }

    public int getBombRange() {
        return bombRange;
    }

    public Position getPosition() {
        return position;
    }

    public boolean isIndestructibility() {
        return indestructibility;
    }

    public long getIndestructibilityTime() {
        return indestructibilityTime;
    }

    public int getPlayerNum() {
        return playerNum;
    }

    public boolean isLive() {
        return live;
    }

    public void speedUp() {
        if (speed < GlobalConf.SPEED.length - 1)
            ++speed;
    }

    public void maxBombsUp() {
        ++maxBombs;
    }

    public void bombRangeUp() {
        ++bombRange;
    }

    public void lifesUp() {
        ++lifes;
    }

    public void setIndestructibilityTime(long time) {
        indestructibilityTime = time;
    }

    public boolean isInMove(long time) {
        if (inMove && (time - startsMove) > GlobalConf.SPEED[speed])
            inMove = false;
        return inMove;
    }

    public void setInMove(boolean inMove) {
        this.inMove = inMove;
    }

    public long getStartsMove() {
        return startsMove;
    }

    public void startMove(long time) {
        inMove = true;
        startsMove = time;
    }

    public void putsBomb() {
        ++bombs;
    }
    public void bombExplodes() {
        --bombs;
    }

    /**
     *
     * @param time aktualny "czas"
     * @return true jesli gracz ma 0 zyc, w przeciwnym przypadku false
     */
    public boolean die(long time) {
        --lifes;
        if (lifes <= 0) {
            live = false;
            return true;
        }
        position.setX(GlobalConf.START_XY[playerNum][0]);
        position.setY(GlobalConf.START_XY[playerNum][1]);
        indestructibilityTime = time + GlobalConf.INDESTRUCTIBILITY_TIME;
        indestructibility = true;
        inMove = false;
        return false;
    }





}