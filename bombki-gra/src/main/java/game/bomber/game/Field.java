package game.bomber.game;

/**
 * @author KOlkiewicz
 */
public class Field {

    private Block block;
    private Powerup powerup;

    public Field(Block block, Powerup powerup) {
        this.block = block;
        this.powerup = powerup;
    }

    public Field(Block block) {
        this.block = block;
        this.powerup = null;
    }

    public void setPowerup(Powerup powerup) {
        this.powerup = powerup;
    }
    public boolean hasPowerup() {
        return (powerup != null);
    }

    public Block getBlock() {
        return block;
    }

    public Powerup getPowerup() {
        return powerup;
    }

    public Powerup removePowerup() {
        Powerup p = powerup;
        powerup = null;
        return p;
    }

    public boolean isEmpty() {
        return block==null;
    }

    public void destroy() {
        if (!isEmpty() && block.isDestructible()) { //test
            block = null;
        }
    }

    @Override
    public boolean equals(Object object) {
        if ( this == object ) {
            return true;
        }
        if ( object == null || getClass() != object.getClass() ) {
            return false;
        }

        Field field = (Field) object;

        if ( block != null ? !block.equals(field.getBlock()) : field.getBlock() != null ) {
            return false;
        }
        if ( powerup != null ? !powerup.equals(field.getPowerup()) : field.getPowerup() != null ) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ( block != null ? block.hashCode() : 0 );
        result = 31 * result + ( powerup != null ? powerup.hashCode() : 0 );
        return result;
    }
}