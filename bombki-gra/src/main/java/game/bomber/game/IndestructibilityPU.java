package game.bomber.game;

import game.bomber.common.GlobalConf;

/**
 * @author KOlkiewicz
 */
public class IndestructibilityPU implements Powerup {
    @Override
    public void activate(Player player) {
        player.setIndestructibilityTime(GlobalConf.INDESTRUCTIBILITY_POWERUP_TIME);
    }

    @Override
    public boolean equals(Object object) {
        if ( this == object ) {
            return true;
        }
        if ( object == null ) {
            return false;
        }
        if ( getClass() == object.getClass() ) {
            return true;
        }
        return false;
    }
}