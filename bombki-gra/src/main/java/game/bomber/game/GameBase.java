package game.bomber.game;


import game.bomber.common.PlayerSimple;
import game.bomber.common.Position;
import game.bomber.game.exceptions.WrongPlayerIdException;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author KOlkiewicz
 */
public abstract class GameBase {

    protected long time;
    protected Board board;
    protected Player[] players;
    protected PlayerSimple[] playerSimples;
    protected long maxTime; // w milisekundach!
    protected Queue<Bomb> bombs;
    protected int playerNumber;

    protected long startTime;

    public GameBase(Board board, PlayerSimple[] playerSimples, long maxTime) {
        bombs = new LinkedList<Bomb>();
        time = 0;
        this.board = board;
        this.playerSimples = playerSimples;
        this.playerNumber = playerSimples.length;

        this.maxTime = maxTime;
        startTime = 0;
    }

    protected void updateTime() {
        time = (System.currentTimeMillis() - startTime);
    }

    public long getTime() {
        return time;
    }

    public Board getBoard() {
        return board;
    }

    public Player[] getPlayers() {
        return players;
    }

    public long getMaxTime() {
        return maxTime;
    }


    /**
     * Sprawdzanie czy gra sie skonczyka. Koniec gdy czas minal lub zostal tylko jeden gracz.
     * @return TRUE jesli gra sie skonczyla, w przeciwnym wypadku FALSE.
     */
    private boolean isEnd() {
        // warunek czasowy
        if (maxTime > 0 &&maxTime <= time){
        	System.out.println("zwracam true, koniec czasu");        	
        	return true;        
        }            

        // warunek 1: został tylko 1 lub 0 graczy
        boolean atLastOneLives = false, atLastTwoLives = false;
        for (Player p : players) {
            if (p.isLive()) {
                if (atLastOneLives)
                    atLastTwoLives=true;
                else
                    atLastOneLives=true;
            }
        }
        return false;
    }

    /**
     * Inicjalizacja gry
     * @return true jeśli udane, w przeciwny wypadku false
     */
    protected abstract boolean init();

    /**
     * Pojedynczy krok glownej petli gry
     */
    protected abstract void gameLoopStep();

    /**
     * Procedura konczenia gry
     * @return id zwyciezscy lub 0 jesli remis
     */
    protected abstract int endGame();

    /**
     * Glowna petla gry
     * 0. inicjalizacja
     * 1. pobieranie i przetwarzanie akcji
     * 2. analiza stanu gry i wysylanie wydarzen
     * 3. finalizowanie gry
     */
    public int gameLoop() throws Exception{
        if (!init())
            throw new Exception("GameBase: NIE MOZNA ROZPOCZAC GRY");
        startTime = System.currentTimeMillis();

        while(!isEnd()) {
        	
            gameLoopStep();
            updateTime();
        }
        return endGame();
    }

    public Player getPlayer(int idPlayer) throws WrongPlayerIdException{
        for (Player p : players)
            if (p.getId() == idPlayer)
                return p;
        throw new WrongPlayerIdException();
    }

    public Position getPosOfPlayer(int idPlayer) throws WrongPlayerIdException{
        for (Player p : players)
            if (p.getId() == idPlayer)
                return p.getPosition();
        throw new WrongPlayerIdException();
    }

}