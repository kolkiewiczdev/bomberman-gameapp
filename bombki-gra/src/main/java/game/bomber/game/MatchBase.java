package game.bomber.game;

import game.bomber.common.GameError;
import game.bomber.common.MatchConfig;
import game.bomber.common.MatchResult;
import game.bomber.common.PlayerSimple;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author KOlkiewicz
 */
public abstract class MatchBase {
    protected MatchConfig config;
    protected PlayerSimple[] players;
    protected Map<Integer, Integer> wins;
    protected int rounds;

    public MatchBase(MatchConfig config, PlayerSimple[] playerSimples) {
        rounds = 0;
        if (playerSimples.length<2 || playerSimples.length>4)
            throw new InvalidParameterException("Minimum dwoch graczy, maksymalnie 4 graczy");
        this.config = config;
        players = playerSimples;
        wins = new HashMap<Integer, Integer>(playerSimples.length);
        for (PlayerSimple playerSimple : playerSimples) {
            wins.put(playerSimple.getId(), 0);
        }
    }

    /**
     * Tworzy i zarzadza runda, po jej zakonczeniu aktualizuje stan meczu
     * @return id gracza, ktory wygral,
     */
    public abstract int nextRound() throws GameError;

    protected void addWin(int playerId) {
        int w = wins.remove(playerId);
        ++w;
        wins.put(playerId, w);
    }

    private int getMaxWins() {
        int max = 0;
        for (Map.Entry<Integer, Integer> win : wins.entrySet()) {
            if (win.getValue() > max) {
                max = win.getValue();
            }
        }
        return max;
    }

    protected boolean isEnd() {
        if (rounds >= config.getMaxRound())
            return true;
        if ( getMaxWins() >= config.getWins())
            return true;
        return false;
    }

    private int getPlayerPosition(int playerId) {
        int pos = 1;
        for (Map.Entry<Integer, Integer> win : wins.entrySet()) {
            if (win.getValue() > wins.get(playerId)) {
                pos++;
            }
        }
        return pos;
    }

    public MatchResult getMatchResult() {
        if (isEnd()) {
            List<MatchResult.PlayerResult> playerResults = new ArrayList<MatchResult.PlayerResult>(players.length);
            for(int i = 0; i<players.length; ++i)
                playerResults.add(new MatchResult.PlayerResult(players[i], wins.get(players[i].getId()), getPlayerPosition(players[i].getId())));
            return new MatchResult(playerResults);
        }
        return null;
    }


    public int getRounds() {
        return rounds;
    }
}