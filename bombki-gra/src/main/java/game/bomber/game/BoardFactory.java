package game.bomber.game;

import game.bomber.common.GameConfig;
import game.bomber.common.GlobalConf;
import game.bomber.game.exceptions.CannotBuildMapException;
import game.bomber.game.utils.Random;

/**
 * @author KOlkiewicz
 */
public class BoardFactory {

    private static class Coordinates {
        private int x;
        private int y;

        public Coordinates(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
    private static boolean getBit(int bitNumber, int from) {
        return ((from >> bitNumber) % 2) == 1;
    }


    private static int getY(int fieldNumber) {
        int i = 0, j = 0;
        while (fieldNumber > j) {
            ++i;
            j += i+1;
        }
        return i;
    }

    private static int getX(int fieldNumber) {
        int i = 0, j = 0, k = 0;
        while (fieldNumber > j) {
            ++i;
            j += i+1;
            k += i;
        }
        return fieldNumber - k;
    }

    private static Coordinates getCord(int fieldNumber, int partNubmer) {
        if (fieldNumber<0 || fieldNumber>27)
            throw new IllegalArgumentException("fieldNumber must be in (0-27)");
        int y = getY(fieldNumber);
        int x = getX(fieldNumber);
        if (partNubmer == 0) {
            return new Coordinates(7 + x, 6 - y);
        }
        if (partNubmer == 1) {
            return new Coordinates(7 + y, 6 - x);
        }
        if (partNubmer == 2) {
            return new Coordinates(7 + y, 7 + x);
        }
        if (partNubmer == 3) {
            return new Coordinates(7 + x, 7 + y);
        }
        if (partNubmer == 4) {
            return new Coordinates(6 - x, 7 + y);
        }
        if (partNubmer == 5) {
            return new Coordinates(6 - y, 7 + x);
        }
        if (partNubmer == 6) {
            return new Coordinates(6 - y, 6 - x);
        }
        if (partNubmer == 7) {
            return new Coordinates(6 - x, 6 - y);
        }
        throw new IllegalArgumentException("partNumber must be in (0-7)");
    }


    /**
     * Metoda budujaca plansze na podstawie konfiguracji i ziarna. Dziala tylko dla planszy 14x14!
     * @param gameConfig Konfiguracja gry
     * @return Plansza
     */
    public static Board buildBoard( GameConfig gameConfig) throws CannotBuildMapException {
        int seed = gameConfig.getSeed();
        if (seed<0)
            seed *= -1;
        Random random = new Random(seed);

        Field[][] map = new Field[GlobalConf.HEIGHT][GlobalConf.WITHD];
        int fieldSet = GlobalConf.MAPS[seed % GlobalConf.MAPS.length];

        boolean destructible;
        int j;
        Coordinates coord;

        // etap 1 - ukladanie 24 * 8 blokow od srodka
        for (int i = 0; i < GlobalConf.MAP_BITS; ++i) {
            destructible = !getBit(i, fieldSet);

            // dla pol na liniach skosnych bierzemy ustawiamy tylko 4 razy
            if (i == 0 || i == 2 || i == 5 || i == 9 || i == 14 || i == 20) {
                for (j = 0; j < 8; j+=2) {
                    coord = getCord(i, j);
                    map[coord.getY()][coord.getX()] = new Field(new Block(destructible));
                }
            }
            // dla pozostalych pol
            else {
                for (j = 0; j < 8; ++j) {
                    coord = getCord(i, j);
                    map[coord.getY()][coord.getX()] = new Field(new Block(destructible));
                }
            }
        }

        // etap 2 - puste pola i pole 24
        // pole 24
        for (j = 0; j < 8; ++j) {
            coord = getCord(24, j);
            map[coord.getY()][coord.getX()] = new Field(new Block(false));
        }
        // 2 * 8 pol pustych
        for (int i = 25; i < 27; ++i) {
            for (j = 0; j < 8; ++j) {
                coord = getCord(i, j);
                map[coord.getY()][coord.getX()] = new Field(null);
            }
        }
        // 4 puste pola w rogach
        for (j = 0; j < 8; j+=2) {
            coord = getCord(27, j);
            map[coord.getY()][coord.getX()] = new Field(null);
        }

        // etap 3 - losowanie powerupow
        int numPU = 0, x, y, maxRandNum = GlobalConf.HEIGHT*GlobalConf.WITHD;
        // 3a BombNumberPU
        j = 0;
        while (numPU < gameConfig.getBombNumberPUNumber()) {
            x = random.next();
            y = random.next();
            if (map[y][x].getBlock() != null && map[y][x].getBlock().isDestructible() && map[y][x].getPowerup() == null) {
                map[y][x].setPowerup(new BombNumberPU());
                ++numPU;
            }
            ++j;
            if (j >=maxRandNum)
                throw new CannotBuildMapException();
        }

        // 3b BombRangePU
        numPU = 0;
        j = 0;
        while (numPU < gameConfig.getBombNumberPUNumber()) {
            y = random.next();
            x = random.next();
            if (map[y][x].getBlock() != null && map[y][x].getBlock().isDestructible() && map[y][x].getPowerup() == null) {
                map[y][x].setPowerup(new BombRangePU());
                ++numPU;
            }
            ++j;
            if (j >=maxRandNum)
                throw new CannotBuildMapException();
        }

        // 3c ExtraLifePU
        numPU = 0;
        j = 0;
        while (numPU < gameConfig.getBombNumberPUNumber()) {
            y = random.next();
            x = random.next();
            if (map[y][x].getBlock() != null && map[y][x].getBlock().isDestructible() && map[y][x].getPowerup() == null) {
                map[y][x].setPowerup(new ExtraLifePU());
                ++numPU;
            }
            ++j;
            if (j >=maxRandNum)
                throw new CannotBuildMapException();
        }

        // 3d IndestructibilityPU
        numPU = 0;
        j = 0;
        while (numPU < gameConfig.getBombNumberPUNumber()) {
            x = random.next();
            y = random.next();
            if (map[y][x].getBlock() != null && map[y][x].getBlock().isDestructible() && map[y][x].getPowerup() == null) {
                map[y][x].setPowerup(new IndestructibilityPU());
                ++numPU;
            }
            ++j;
            if (j >=maxRandNum)
                throw new CannotBuildMapException();
        }

        // 3e SpeedPU
        numPU = 0;
        j = 0;
        while (numPU < gameConfig.getBombNumberPUNumber()) {
            x = random.next();
            y = random.next();
            if (map[y][x].getBlock() != null && map[y][x].getBlock().isDestructible() && map[y][x].getPowerup() == null) {
                map[y][x].setPowerup(new SpeedPU());
                ++numPU;
            }
            ++j;
            if (j >=maxRandNum)
                throw new CannotBuildMapException();
        }

        return new Board(map);
    }

}