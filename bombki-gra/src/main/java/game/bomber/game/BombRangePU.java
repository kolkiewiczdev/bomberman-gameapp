package game.bomber.game;

/**
 * @author KOlkiewicz
 */
public class BombRangePU implements Powerup {
    @Override
    public void activate(Player player) {
        player.bombRangeUp();
    }

    @Override
    public boolean equals(Object object) {
        if ( this == object ) {
            return true;
        }
        if ( object == null ) {
            return false;
        }
        if ( getClass() == object.getClass() ) {
            return true;
        }
        return false;
    }
}