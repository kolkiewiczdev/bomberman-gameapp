package game.bomber.game.utils;

import game.bomber.common.GlobalConf;

/**
 * @author KOlkiewicz
 */
public class Random {
    java.util.Random rand;
    private int m = GlobalConf.HEIGHT;

    public Random(int seed) {
        rand = new java.util.Random(seed);
    }

    public int next() {
        int r = rand.nextInt() % m;
        if (r<0)
            r *= -1;
        return r;
    }

}
