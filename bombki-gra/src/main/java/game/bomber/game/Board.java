package game.bomber.game;

/**
 * @author KOlkiewicz
 */
public class Board {

    private Field[][] fields;

    public Board(Field[][] fields) {
        this.fields = fields;
    }

    public Field getField(int x, int y) {    	
        return fields[x][y];
    }

    @Override
    public boolean equals(Object object) {
        if ( this == object ) {
            return true;
        }
        if ( object == null || getClass() != object.getClass() ) {
            return false;
        }

        Board b = (Board) object;
        if (fields.length != b.fields.length) {
            return false;
        }

        for(int j, i = 0; i < fields.length; ++i) {
            if (fields[i].length != b.fields[i].length) {
                return false;
            }
            for(j = 0; j < fields[i].length; ++j) {
                if( !fields[i][j].equals(b.getField(i,j) )) {
                    return false;
                }
            }
        }

        return true;
    }
}