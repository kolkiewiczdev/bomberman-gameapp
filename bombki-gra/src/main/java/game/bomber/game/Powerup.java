package game.bomber.game;

/**
 * @author KOlkiewicz
 */
public interface Powerup {

  public void activate( Player player);

}