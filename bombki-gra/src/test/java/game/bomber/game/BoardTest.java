package game.bomber.game;

import game.bomber.common.GlobalConf;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

/**
 * @author KOlkiewicz
 */
public class BoardTest {

    private Powerup getPowerup(int i) {

        if ( i == 0 )
            return null;
        if (i == 1)
            return new BombNumberPU();
        if (i == 2)
            return new BombRangePU();
        if (i == 3 )
            return new ExtraLifePU();
        if (i == 4)
            return new IndestructibilityPU();
        return new SpeedPU();
    }


    @Test
    public void testEqualsReturnTrue() throws Exception {
        Random random = new Random(47);
        Field[][] map1 = new Field[GlobalConf.HEIGHT][GlobalConf.WITHD];
        Field[][] map2 = new Field[GlobalConf.HEIGHT][GlobalConf.WITHD];

        for(int i = 0; i < GlobalConf.HEIGHT; ++i) {
            for (int j = 0; j < GlobalConf.WITHD; ++j) {
                int k = random.nextInt() % 6;
                map1[i][j] = new Field(new Block(random.nextBoolean()), getPowerup(k));
                map2[i][j] = new Field(new Block(map1[i][j].getBlock().isDestructible()), getPowerup(k));
            }
        }
        Board b1 = new Board(map1);
        Board b2 = new Board(map2);

        assertTrue(b1.equals(b1));
        assertTrue(b2.equals(b2));
        assertTrue(b1.equals(b2));
        assertTrue(b2.equals(b1));
    }
}