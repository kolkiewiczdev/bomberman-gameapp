package game.bomber.game;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author KOlkiewicz
 */
public class FieldTest {

    @Test
    public void testEqualsReturnTrue() throws Exception {
        // true, null
        Field f1 = new Field(new Block(true), null);
        Field f2 = new Field(new Block(true), null);

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

        // false, null
        f1 = new Field(new Block(false), null);
        f2 = new Field(new Block(false), null);

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

        // true, BombNumberPU
        f1 = new Field(new Block(true), new BombNumberPU());
        f2 = new Field(new Block(true), new BombNumberPU());

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

        // false, BombNumberPU
        f1 = new Field(new Block(false), new BombNumberPU());
        f2 = new Field(new Block(false), new BombNumberPU());

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

        // true, BombRangePU
        f1 = new Field(new Block(true), new BombRangePU());
        f2 = new Field(new Block(true), new BombRangePU());

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

        // false, BombRangePU
        f1 = new Field(new Block(false), new BombRangePU());
        f2 = new Field(new Block(false), new BombRangePU());

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

        // true, ExtraLifePU
        f1 = new Field(new Block(false), new ExtraLifePU());
        f2 = new Field(new Block(false), new ExtraLifePU());

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

        // false, ExtraLifePU
        f1 = new Field(new Block(false), new ExtraLifePU());
        f2 = new Field(new Block(false), new ExtraLifePU());

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

        // true, IndestructibilityPU
        f1 = new Field(new Block(false), new IndestructibilityPU());
        f2 = new Field(new Block(false), new IndestructibilityPU());

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

        // false, IndestructibilityPU
        f1 = new Field(new Block(false), new IndestructibilityPU());
        f2 = new Field(new Block(false), new IndestructibilityPU());

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

        // true, SpeedPU
        f1 = new Field(new Block(false), new SpeedPU());
        f2 = new Field(new Block(false), new SpeedPU());

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

        // false, SpeedPU
        f1 = new Field(new Block(false), new SpeedPU());
        f2 = new Field(new Block(false), new SpeedPU());

        assertTrue(f1.equals(f1));
        assertTrue(f2.equals(f2));
        assertTrue(f1.equals(f2));
        assertTrue(f2.equals(f1));

    }
}