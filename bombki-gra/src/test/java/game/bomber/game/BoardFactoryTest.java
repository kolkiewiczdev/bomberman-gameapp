package game.bomber.game;

import game.bomber.common.GameConfig;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

/**
 * @author KOlkiewicz
 */
public class BoardFactoryTest {

    @Test
    public void testBuildBoard() throws Exception {
        GameConfig gameConfig = new GameConfig();
        Board b1 = BoardFactory.buildBoard(gameConfig);
        Board b2 = BoardFactory.buildBoard(gameConfig);

        assertTrue(b1.equals(b1));
        assertTrue(b2.equals(b2));
        assertTrue(b1.equals(b2));
        assertTrue(b2.equals(b1));

        gameConfig.setSeed(1);
        b1 = BoardFactory.buildBoard(gameConfig);
        b2 = BoardFactory.buildBoard(gameConfig);

        assertTrue(b1.equals(b1));
        assertTrue(b2.equals(b2));
        assertTrue(b1.equals(b2));
        assertTrue(b2.equals(b1));

        gameConfig.setSeed(100);
        b1 = BoardFactory.buildBoard(gameConfig);
        b2 = BoardFactory.buildBoard(gameConfig);

        assertTrue(b1.equals(b1));
        assertTrue(b2.equals(b2));
        assertTrue(b1.equals(b2));
        assertTrue(b2.equals(b1));

        Random random = new Random();
        for (int i = 0; i < 100; ++i) {
            gameConfig.setSeed(random.nextInt());
            b1 = BoardFactory.buildBoard(gameConfig);
            b2 = BoardFactory.buildBoard(gameConfig);

            assertTrue(b1.equals(b1));
            assertTrue(b2.equals(b2));
            assertTrue(b1.equals(b2));
            assertTrue(b2.equals(b1));
        }
    }
}