package game.bomber.game.utils;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author KOlkiewicz
 */
public class RandomTest {

    @Test
    public void testNext() throws Exception {
        Random r1 = new Random(0);
        Random r2 = new Random(0);
        for (int i = 0; i < 20; ++i)
            assertTrue(r1.next() == r2.next());

        r1 = new Random(3671393);
        r2 = new Random(3671393);
        for (int i = 0; i < 20; ++i)
            assertTrue(r1.next() == r2.next());

        r1 = new Random(4210881);
        r2 = new Random(4210881);
        for (int i = 0; i < 20; ++i)
            assertTrue(r1.next() == r2.next());

        r1 = new Random(4215848);
        r2 = new Random(4215848);
        for (int i = 0; i < 20; ++i)
            assertTrue(r1.next() == r2.next());

        r1 = new Random(3817601);
        r2 = new Random(3817601);
        for (int i = 0; i < 20; ++i)
            assertTrue(r1.next() == r2.next());

    }
}