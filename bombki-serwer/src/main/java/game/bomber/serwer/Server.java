package game.bomber.serwer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import game.bomber.common.PlayerSimple;
import game.bomber.common.Response;
import game.bomber.communication.global.MServerCommunicator;
import game.bomber.common.Request;
import game.bomber.common.RequestData;

/**
 * @author KOlkiewicz
 */
public class Server implements Runnable {

	private MServerCommunicator serverCommunicator;
	private HashMap<PlayerSimple, Integer> players; // 1-czeka, 2-gra, 3-wolny

	public Server() {
		try {
			serverCommunicator = new MServerCommunicator(3001);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		players = new HashMap<PlayerSimple, Integer>();
	}

	public static void main(String[] args) {
		Runnable r = new Server();
		Thread t = new Thread(r);
		t.start();
	}

	public void run() {
		while (true) {
			final RequestData data = serverCommunicator.getRequestData();
			synchronized (this) {
				/*
				 * W przypadku DISCONNECT zawsze wyslij "potwierdzenie", "true"
				 */
				if (data.getRequest().getRequestType() == Request.REQUEST.DISCONNECT) {
					if (data.getId() > 0) {
						PlayerSimple player = null;
						for (Entry<PlayerSimple, Integer> entry : players
								.entrySet()) {
							if (entry.getKey().getId() == data.getId()) {
								player = entry.getKey();
								break;
							}
						}
						players.remove(player);
					}
					Map<String, String> params = new HashMap<String, String>();
					params.put("potwierdzenie", "true");
					Response response = new Response(
							Request.REQUEST.DISCONNECT, params);
					serverCommunicator.sendResponse(response, data.getId());
				}
				/*
				 * W przypadku REGISTRATION wyslij "potwierdzenie", "true" jesli
				 * udalo si� wpisac do bazy danych "potwierdzenie", "false"
				 * jesli si� nie udalo
				 */
				if (data.getRequest().getRequestType() == Request.REQUEST.REGISTRATION) {
					System.out.println("nazwa uzytkownika: " + data.getRequest().getParam("nazwa uzytkownika"));
					System.out.println("haslo: " + data.getRequest().getParam("haslo"));
					System.out.println("pytanie: " + data.getRequest().getParam("pytanie"));
					System.out.println("odpowiedz: " + data.getRequest().getParam("odpowiedz"));
					Map<String, String> params = new HashMap<String, String>();
					params.put("potwierdzenie", "true");
					Response response = new Response(
							Request.REQUEST.REGISTRATION, params);
					serverCommunicator.sendResponse(response, data.getId());
				}
				/*
				 * W przypadku LOGIN wyslij ("potwierdzenie", "true")("id", id
				 * gracza z bazy danych) jesli udalo si� wpisac do bazy danych
				 * "potwierdzenie", "false" jesli si� nie udalo
				 */
				if (data.getRequest().getRequestType() == Request.REQUEST.LOGIN) {
					System.out.println("nazwa uzytkownika: " + data.getRequest().getParam("nazwa uzytkownika"));
					System.out.println("haslo: " + data.getRequest().getParam("haslo"));
					String username = data.getRequest().getParam(
							"nazwa uzytkownika");
					int id = 1322;
					switch (username) {
					case "Damian":
						id = 10;
						break;
					case "Kawa":
						id = 20;
						break;
					case "Andrzej":
						id = 30;
						break;
					case "Sebastian":
						id = 40;
						break;
					}
					players.put(new PlayerSimple(id, username), 3); // po
																	// zalogowaniu
																	// status
																	// jest
																	// wolny(3)
					Map<String, String> params = new HashMap<String, String>();
					params.put("potwierdzenie", "true");
					params.put("id", String.valueOf(id));
					Response response = new Response(Request.REQUEST.LOGIN,
							params);
					serverCommunicator.sendResponse(response, data.getId());
				}
				/*
				 * W przypadku GET_QUESTION wyslij ("potwierdzenie",
				 * "true")("pytanie", pytanie z bazy danych) jesli istnieje taki
				 * uzytkownik "potwierdzenie", "false" jesli si� nie udalo
				 */
				if (data.getRequest().getRequestType() == Request.REQUEST.GET_QUESTION) {
					System.out.println("nazwa uzytkownika: " + data.getRequest().getParam("nazwa uzytkownika"));
					Map<String, String> params = new HashMap<String, String>();
					params.put("potwierdzenie", "true");
					params.put("pytanie", "Przyk�adowe pytanie?");
					Response response = new Response(
							Request.REQUEST.GET_QUESTION, params);
					serverCommunicator.sendResponse(response, data.getId());
				}
				/*
				 * W przypadku ANSWER_QUESTION wyslij
				 * ("potwierdzenie","true")("haslo", nowe losowo wygenerowane
				 * haslo) jesli odpowiedz jest poprawna "potwierdzenie", "false"
				 * jesli si� nie udalo
				 */
				if (data.getRequest().getRequestType() == Request.REQUEST.ANSWER_QUESTION) {
					System.out.println("nazwa uzytkownika: " + data.getRequest().getParam("nazwa uzytkownika"));
					System.out.println("odpowiedz: " + data.getRequest().getParam("odpowiedz"));
					Map<String, String> params = new HashMap<String, String>();
					params.put("potwierdzenie", "true");
					params.put("haslo", "nowehaslo2015");
					Response response = new Response(
							Request.REQUEST.ANSWER_QUESTION, params);
					serverCommunicator.sendResponse(response, data.getId());
				}
				/*
				 * W przypadku PLAYERS_LIST wyslij (kolejne numery, nazwa
				 * uzytkownika)
				 */
				if (data.getRequest().getRequestType() == Request.REQUEST.PLAYERS_LIST) {
					Map<String, String> params = new HashMap<String, String>();
					int lp = 0;
					for (Entry<PlayerSimple, Integer> entry : players
							.entrySet()) {
						if (entry.getKey().getId() == data.getId()) {
							params.put(String.valueOf(lp), entry.getKey()
									.getUsername() + "4"); // status "JA"
						} else {
							params.put(
									String.valueOf(lp),
									entry.getKey().getUsername()
											+ String.valueOf(entry.getValue()));
						}
						lp++;
					}
					Response response = new Response(
							Request.REQUEST.ANSWER_QUESTION, params);
					serverCommunicator.sendResponse(response, data.getId());
				}
				/*
				 * W przypadku STATS wyslij "pozycja 1",ilosc "pozycja 2",ilosc
				 * "pozycja 3",ilosc "pozycja 4",ilosc "wygrane rundy",ilosc
				 * "przegrane rundy",ilosc "punkty",ilosc
				 * "pozycja w rankingu",ilosc
				 */
				if (data.getRequest().getRequestType() == Request.REQUEST.STATS) {
					System.out.println("nazwa uzytkownika: " + data.getRequest().getParam("nazwa uzytkownika")); 
					Map<String, String> params = new HashMap<String, String>();
					params.put("pozycja 1", "3");
					params.put("pozycja 2", "5");
					params.put("pozycja 3", "3");
					params.put("pozycja 4", "8");
					params.put("wygrane rundy", "77");
					params.put("przegrane rundy", "564");
					params.put("punkty", "5666");
					params.put("pozycja w rankingu", "24");
					Response response = new Response(Request.REQUEST.STATS,
							params);
					serverCommunicator.sendResponse(response, data.getId());
				}
				/*
				 * W przypadku INVITE wyslij ("potwierdzenie","true") jesli dany
				 * gracz jest wolny(status 3) "potwierdzenie", "false" jesli nie
				 * jest wolny(inny status)
				 */
				if (data.getRequest().getRequestType() == Request.REQUEST.INVITE) {
					Map<String, String> params = new HashMap<String, String>();
					final String name = data.getRequest().getParam(
							"nazwa uzytkownika");
					boolean flag = true;
					for (Entry<PlayerSimple, Integer> entry : players
							.entrySet()) {
						if (entry.getKey().getUsername().equals(name)) {
							if (entry.getValue() != 3) {
								flag = false;
								break;
							} else {
								flag = true;
								break;
							}
						}
					}
					if (flag) {
						params.put("potwierdzenie", "true");
						Runnable r = new Runnable() {
							public void run() {
								Map<String, String> params = new HashMap<String, String>();
								params.put("nazwa uzytkownika",
										getUsername(data.getId()));
								params.put(
										"do ilu wygranych",
										data.getRequest().getParam(
												"do ilu wygranych"));
								params.put("ip",
										data.getRequest().getParam("ip"));
								params.put("action port",
										data.getRequest().getParam("action port"));
								params.put("event port",
										data.getRequest().getParam("event port"));
								params.put("game config port",
										data.getRequest().getParam("game config port"));
								params.put("match config port",
										data.getRequest().getParam("match config port"));
								params.put("player port",
										data.getRequest().getParam("player port"));
								params.put("numer",
										data.getRequest().getParam("numer"));
								Request request = new Request(
										Request.REQUEST.INVITE, params);
								serverCommunicator.sendRequest(request,
										data.getId(), getId(name));
							}
						};
						Thread t = new Thread(r);
						t.start();
					} else {
						params.put("potwierdzenie", "false");
					}
					Response response = new Response(Request.REQUEST.INVITE,
							params);
					serverCommunicator.sendResponse(response, data.getId());
				}
				/*
				 * W przypadku ANSWER_TO_INVITE wyslij ("potwierdzenie","true")
				 * informacja, ze dotar�o do serwera
				 */
				if (data.getRequest().getRequestType() == Request.REQUEST.ANSWER_TO_INVITE) {
					Map<String, String> params = new HashMap<String, String>();
					params.put("potwierdzenie", "true");
					Response response = new Response(
							Request.REQUEST.ANSWER_TO_INVITE, params);
					serverCommunicator.sendResponse(response, data.getId());
					Runnable r = new Runnable() {
						public void run() {
							Map<String, String> params2 = new HashMap<String, String>();
							params2.put("potwierdzenie", data.getRequest()
									.getParam("potwierdzenie"));
							params2.put("nazwa uzytkownika",
									getUsername(data.getId()));
							Request request = new Request(
									Request.REQUEST.ANSWER_TO_INVITE, params2);
							serverCommunicator.sendRequest(
									request,
									data.getId(),
									getId(data.getRequest().getParam(
											"nazwa uzytkownika")));
						}
					};
					Thread t = new Thread(r);
					t.start();
				}
				/*
				 * W przypadku SET_STATUS wyslij "potwierdzenie","true"
				 * informacja, ze dotar�o do serwera
				 */
				if (data.getRequest().getRequestType() == Request.REQUEST.SET_STATUS) {
					Map<String, String> params = new HashMap<String, String>();
					params.put("potwierdzenie", "true");
					for (Entry<PlayerSimple, Integer> entry : players
							.entrySet()) {
						if (entry.getKey().getId() == data.getId()) {
							entry.setValue(Integer.valueOf(data.getRequest()
									.getParam("status")));
						}
					}
					Response response = new Response(
							Request.REQUEST.SET_STATUS, params);
					serverCommunicator.sendResponse(response, data.getId());
				}
			}
		}
	}

	public int getId(String name) {
		int id = 0;
		synchronized (this) {
			for (Entry<PlayerSimple, Integer> entry : players.entrySet()) {
				if (entry.getKey().getUsername().equals(name)) {
					id = entry.getKey().getId();
				}
			}
		}
		return id;
	}

	public String getUsername(int id) {
		String name = "";
		synchronized (this) {
			for (Entry<PlayerSimple, Integer> entry : players.entrySet()) {
				if (entry.getKey().getId() == id) {
					name = entry.getKey().getUsername();
				}
			}
		}
		return name;
	}

}
