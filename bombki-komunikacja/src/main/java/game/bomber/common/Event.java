package game.bomber.common;

import java.io.Serializable;

/**
 * @author KOlkiewicz
 */
public class Event implements Serializable{
    public static enum EVENT {
        // dodatkowe dane: z ktorego pola, ktory gracz
        UP,DOWN,LEFT,RIGTH,PUT_BOMB,
        // dodatkowe dane: ktore pole, ktory gracz
        GETS_POWERUP,
        // dodatkowe dane: ktore pole, zasieg
        EXPLOSION,
        // dodatkowe dane: ktory gracz
        DEATH,END_GAME,
        // dodatkowe dane: ziarno
        INIT_GAME,
        // dodatkowe dane: brak
        START_GAME
    }

    private EVENT eventType;
    private int x;
    private int y;
    private int p;

    public Event(Action action, int x, int y) {
        switch (action.getActionType()) {
            case UP: eventType = EVENT.UP; break;
            case DOWN: eventType = EVENT.DOWN; break;
            case LEFT: eventType = EVENT.LEFT; break;
            case RIGTH: eventType = EVENT.RIGTH; break;
            case PUT_BOMB: eventType = EVENT.PUT_BOMB; break;
        }
        p=action.getPlayerId();
        this.x = x;
        this.y = y;
    }

    public Event(EVENT eventType, int x, int y, int p) {
        this.eventType = eventType;
        this.x = x;
        this.y = y;
        this.p = p;
    }

    public EVENT getEventType() {
        return eventType;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getP() {
        return p;
    }
}
