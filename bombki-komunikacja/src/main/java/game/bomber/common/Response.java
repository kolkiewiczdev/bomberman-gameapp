package game.bomber.common;

import java.io.Serializable;
import java.util.Map;

/**
 * @author KOlkiewicz
 */
public class Response implements Serializable{
    private Request.REQUEST requestType;
    private Map<String, String> params;

    public Response(Request.REQUEST requestType, Map<String, String> params) {
        this.requestType = requestType;
        this.params = params;
    }

    public Request.REQUEST getRequestType() {
        return requestType;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public String getParam(String paramName) {
        return params.get(paramName);
    }
}
