package game.bomber.common;

import java.io.Serializable;
import java.util.Map;

/**
 * @author KOlkiewicz
 */
public class Request implements Serializable{
    /**
     * Rodzaje zadan, kazde charakteryzuje sie dodatkowo swoimi parametrami przekazywanymi za pomoca mapy
     *
     */
    public static enum REQUEST {
        REGISTRATION,   // wymaga: nazwa uzytkownika, haslo (zaszyfrowane), pytanie, odpowiedz (zaszyfrowana)
                        // potwierdzenie
        LOGIN,  // nazwa uzytkownika, haslo (zaszyfrowane)
                // potwierdzenie, id
        GET_QUESTION,	//	nazwa uzytkownika
        				//	potwierdzenie, pytanie
        ANSWER_QUESTION,	//	nazwa uzytkownika, odpowiedz (zaszyfrowana)
        					//	potwierdzenie, haslo
        INVITE, // nazwa uzytkownika, do ilu wygranych
                // czy wyslano zaproszenie
        ANSWER_TO_INVITE,   // potwierdzenie
        					// potwierdzenie, dane do polaczenia do serwera gry
        PLAY_NOW,   // -
                    // dane do polaczenia z serwerem gry (jesli to moje dane to ja tworze serwer)
        PLAYERS_LIST,   // -
                        // lista graczy online+status
        REFRESH_INVITED,    // id meczu
                            // informacja o statusie zaproszonych graczy
        STATS,  // nazwa uzytkownika
                // statystyki uzytkownika
        READY,	// id meczu
        		// czy udalo sie rozpoczac gre
        CHANGE_PASSWORD,	// nazwa uzytkownika, haslo
        					// informacje o bledach
        SET_STATUS,		// status
        				// potwierdzenie
        DISCONNECT,	// -
        			//	potwierdzenie
    }

    private REQUEST requestType;
    private Map<String, String> params;

    public Request(REQUEST requestType, Map<String, String> params) {
        this.requestType = requestType;
        this.params = params;
    }

    public REQUEST getRequestType() {
        return requestType;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public String getParam(String paramName) {
        return params.get(paramName);
    }
}
