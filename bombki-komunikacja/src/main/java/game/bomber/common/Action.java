package game.bomber.common;

import java.io.Serializable;

/**
 * @author KOlkiewicz
 */
public class Action implements Serializable{
    public static enum ACTION {
        UP, DOWN, LEFT, RIGTH, PUT_BOMB, READY
    };

    private ACTION actionType;
    private int playerId;

    public Action(ACTION actionType, int playerId) {
        this.actionType = actionType;
        this.playerId = playerId;
    }

    public ACTION getActionType() {
        return actionType;
    }
    public int getPlayerId() {
        return playerId;
    }

}
