package game.bomber.common;

import java.io.Serializable;

/**
 * @author KOlkiewicz
 */
public class RequestData implements Serializable{
    private Request request;
    /**
     * Kto wysyla zadanie.
     */
    private int id;

    public RequestData(Request request, int id) {
        this.request = request;
        this.id = id;
    }

    public Request getRequest() {
        return request;
    }

    public int getId() {
        return id;
    }
}
