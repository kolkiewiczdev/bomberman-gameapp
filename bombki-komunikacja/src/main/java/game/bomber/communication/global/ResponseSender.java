package game.bomber.communication.global;

import game.bomber.common.Response;

/**
 * @author KOlkiewicz
 */
public interface ResponseSender {
    /**
     * Wysylanie odpowiedzi
     * @param response odpowiedz
     * @param id do kogo wysylana jest
     */
    void sendResponse(Response response, int id);
}
