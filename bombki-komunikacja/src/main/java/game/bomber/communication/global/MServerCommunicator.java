package game.bomber.communication.global;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import game.bomber.common.Response;
import game.bomber.communication.game.MatchResultReceiver;
import game.bomber.common.MatchResult;
import game.bomber.common.RequestData;
import game.bomber.common.Request;

/**
 * @author KOlkiewicz
 */
public class MServerCommunicator implements MatchResultReceiver,
		RequestReceiver, ResponseSender {

	private ServerSocket welcomeSocket;
	private ServerSocket welcomeSocket2;
	private Runnable connector;
	private Runnable connector2;
	private LinkedList<Integer> temporary;
	private Map<Integer, Streamer> clientStreamers;
	private LinkedList<RequestData> requests;

	public MServerCommunicator(int port) throws IOException {
		System.out.println("Tworzenie serwera...");
		welcomeSocket = new ServerSocket(port);
		welcomeSocket2 = new ServerSocket(port + 1);
		System.out.println("Stworzono " + welcomeSocket.getInetAddress().getLocalHost().getHostAddress());
		temporary = new LinkedList<Integer>();
		clientStreamers = new ConcurrentHashMap<Integer, Streamer>();
		requests = new LinkedList<RequestData>();
		connector = new Runnable() {
			public void run() {
				while (true) {
					try {
						Socket incoming = welcomeSocket.accept();
						boolean flag = true;
						int playerId = 0;
						while (flag) {
							playerId--;
							synchronized (this) {
								if (!temporary.contains(playerId)) {
									flag = false;
									temporary.add(playerId);
								}
							}
						}
						Streamer r = new Streamer(incoming, playerId);
						clientStreamers.put(playerId, r);
						Thread t = new Thread(r);
						t.start();

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// TODO Auto-generated method stub
				}
			}
		};
		Thread t = new Thread(connector);
		t.start();
		connector2 = new Runnable() {
			public void run() {
				while (true) {
					try {
						Socket incoming = welcomeSocket2.accept();
						// ObjectInputStream inFromClient = new
						// ObjectInputStream(
						// incoming.getInputStream());
						ObjectInputStream inFromClient = new ObjectInputStream(
								incoming.getInputStream());
						ObjectOutputStream outToClient = new ObjectOutputStream(
								incoming.getOutputStream());
						int id = (int) inFromClient.readObject();
						synchronized (this) {
							clientStreamers.get(id).createSecondSocket(
									incoming, inFromClient, outToClient);
						}

					} catch (IOException | ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// TODO Auto-generated method stub
				}
			}
		};
		Thread t2 = new Thread(connector2);
		t2.start();
	}

	@Override
	public MatchResult getMatchResult() {
		return null;
	}

	public RequestData getRequestData() {
		while (true) {
			synchronized (this) {
				if (!requests.isEmpty()) {
					RequestData requestData = requests.pop();
					return requestData;
				}
			}
		}
	}

	public void sendResponse(Response response, int id) {
		synchronized (this) {
			Streamer streamer = clientStreamers.get(id);
			try {
				ObjectOutputStream outToClient = streamer.outToClient;
				outToClient.writeObject(response);
				if (response.getRequestType() == Request.REQUEST.DISCONNECT) {
					streamer.flag = false;
					streamer.outToClient.close();
					streamer.inFromClient.close();
					streamer.incoming.close();
					if (id > 0) {
						streamer.outToClient2.close();
						streamer.inFromClient2.close();
						streamer.incoming2.close();
					}
					clientStreamers.remove(id);
					if (id < 0) {
						temporary.remove(temporary.indexOf(id));
					}
				}
				if (response.getRequestType() == Request.REQUEST.LOGIN
						&& response.getParam("potwierdzenie").equals("true")) {
					clientStreamers.put(
							Integer.valueOf(response.getParam("id")), streamer);
					clientStreamers.remove(id);
					temporary.remove(temporary.indexOf(id));
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}
	}

	public void sendRequest(Request request, int sourceId, int targetId) {
		// Socket socket = new Socket(host, port);
		synchronized (this) {
			Streamer streamer = clientStreamers.get(targetId);
			ObjectOutputStream outToClient2 = streamer.outToClient2;
			// ObjectInputStream inFromClient2 = streamer.inFromClient2;
			try {
				outToClient2.writeObject(new RequestData(request, sourceId));
				// RequestData data = (RequestData)inFromClient2.readObject();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	class Streamer implements Runnable {
		Socket incoming;
		ObjectInputStream inFromClient;
		ObjectOutputStream outToClient;

		Socket incoming2;
		ObjectInputStream inFromClient2;
		ObjectOutputStream outToClient2;

		boolean flag;

		public Streamer(Socket clientSocket, int playerId) {
			incoming = clientSocket;
			try {
				outToClient = new ObjectOutputStream(incoming.getOutputStream());
				outToClient.writeObject(playerId);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			flag = true;
		}

		public void run() {
			try {
				inFromClient = new ObjectInputStream(incoming.getInputStream());
				flag = true;
				while (flag) {
					RequestData tmp = (RequestData) inFromClient.readObject();
					synchronized (this) {
						requests.add(tmp);
					}
				}
			} catch (IOException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
		}

		public void createSecondSocket(Socket inc, ObjectInputStream input,
				ObjectOutputStream output) {
			incoming2 = inc;
			inFromClient2 = input;
			outToClient2 = output;
		}
	}

}