package game.bomber.communication.global;

import java.io.*;
import java.net.*;
import java.util.*;

import game.bomber.common.Response;
import game.bomber.common.Request;
import game.bomber.common.RequestData;

/**
 * @author KOlkiewicz
 */
public class MClientCommunicator implements RequestSender {
	private Socket socket;
	private ObjectOutputStream outToServer;
	private ObjectInputStream inFromServer;
	private Socket socket2;
	private ObjectOutputStream outToServer2;
	private ObjectInputStream inFromServer2;
	private int playerId;
	private boolean connected;
	private LinkedList<RequestData> list;
	private Runnable requestListener;
	private Runnable requestExecutor;
	private boolean requestFlag;
	private String serverHost;
	private int serverPort;
	private MainFrame mainFrame;
	private NewGameFrame newGameFrame;

	public MClientCommunicator(String host, int port) {
		serverHost = host;
		serverPort = port;
		try {
			socket = new Socket(host, port);
			connected = true;
			outToServer = new ObjectOutputStream(socket.getOutputStream());
			inFromServer = new ObjectInputStream(socket.getInputStream());
			playerId = (Integer) inFromServer.readObject();

		} catch (ClassNotFoundException e) {
		} catch (UnknownHostException e) {
			connected = false;
		} catch (IOException e) {
		}
	}

	public void reconnect() {
		if (connected) {
			sendRequest(new Request(Request.REQUEST.DISCONNECT, null));
		}
		try {
			socket = new Socket(serverHost, serverPort);
			connected = true;
			outToServer = new ObjectOutputStream(socket.getOutputStream());
			inFromServer = new ObjectInputStream(socket.getInputStream());
			playerId = (Integer) inFromServer.readObject();
		} catch (ClassNotFoundException e) {
		} catch (UnknownHostException e) {
			connected = false;
		} catch (IOException e) {
		}
	}

	public void close() {
		if (connected != false) {
			try {
				requestFlag = false;
				outToServer.close();
				inFromServer.close();
				socket.close();
				outToServer2.close();
				inFromServer2.close();
				socket2.close();
				// mainFrame.dispose();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public boolean isConnected() {
		return connected;
	}

	@Override
	public Response sendRequest(Request request) {
		if (connected == false
				&& request.getRequestType() == Request.REQUEST.DISCONNECT) {
			Map<String, String> params = new HashMap<String, String>();
			params.put("potwierdzenie", "true");
			return new Response(Request.REQUEST.DISCONNECT, params);
		} else {
			RequestData data = new RequestData(request, playerId);
			Response response;
			try {
				outToServer.writeObject(data);
				response = (Response) inFromServer.readObject();
			} catch (IOException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response = null;
			}
			if (response.getRequestType() == Request.REQUEST.DISCONNECT) {
				try {

					inFromServer.close();
					outToServer.close();
					socket.close();
					if (requestFlag == true) {
						requestFlag = false;
						outToServer2.close();
						inFromServer2.close();
						socket2.close();
					}
					connected = false;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (response.getRequestType() == Request.REQUEST.LOGIN) {
				playerId = Integer.valueOf(response.getParam("id"));
			}
			return response;
		}
	}

	public void createRequestListener() {
		try {
			requestFlag = true;
			socket2 = new Socket(serverHost, serverPort + 1);
			outToServer2 = new ObjectOutputStream(socket2.getOutputStream());
			inFromServer2 = new ObjectInputStream(socket2.getInputStream());
			outToServer2.writeObject(playerId);
			list = new LinkedList<RequestData>();
			requestListener = new Runnable() {
				public void run() {
					try {
						while (requestFlag) {
							RequestData data = (RequestData) inFromServer2
									.readObject();
							synchronized (this) {
								list.add(data);
							}
						}
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
					}

				}
			};
			Thread t = new Thread(requestListener);
			t.start();
			requestExecutor = new Runnable() {
				public void run() {
					while (requestFlag) {
						synchronized (this) {
							if (list.size() > 0) {
								RequestData data = list.pop();
								if (data.getRequest().getRequestType() == Request.REQUEST.INVITE) {
									if (mainFrame != null) {
										mainFrame.showInvitation(
												mainFrame.getLang(),
												data.getRequest().getParam(
														"nazwa uzytkownika"),
												data.getRequest().getParam(
														"do ilu wygranych"),
												data.getRequest().getParams());
									}
								}
								if (data.getRequest().getRequestType() == Request.REQUEST.ANSWER_TO_INVITE) {
									if (newGameFrame != null) {
										boolean tmp = false;
										if (data.getRequest()
												.getParam("potwierdzenie")
												.equals("true")) {
											tmp = true;
										}
										newGameFrame.updateStatus(
												data.getRequest().getParam(
														"nazwa uzytkownika"),
												tmp);
									}
								}
							}
						}
					}
				}

			};
			Thread t2 = new Thread(requestExecutor);
			t2.start();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setMainFrame(MainFrame frame) {
		this.mainFrame = frame;
	}

	public void setNewGameFrame(NewGameFrame frame) {
		this.newGameFrame = frame;
	}

	public int getPlayerId() {
		return playerId;
	}
}
