package game.bomber.communication.global;

import game.bomber.common.RequestData;

/**
 * @author KOlkiewicz
 */
public interface RequestReceiver {
    RequestData getRequestData();
}
