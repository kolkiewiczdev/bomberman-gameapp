package game.bomber.communication.global;

import game.bomber.common.Response;
import game.bomber.common.Request;

/**
 * @author KOlkiewicz
 */
public interface RequestSender {
    /**
     *
     * @param request zadanie do serwera
     * @return odpowedz z serwera
     */
    Response sendRequest(Request request);
}
