package game.bomber.communication.game;

import game.bomber.common.MatchConfig;

/**
 * @author KOlkiewicz
 */
public interface MatchConfigReceiver {
    /**
     * Odbieranie ustawien meczu
     * @return ustawienia meczu
     */
    MatchConfig getMatchConfig();
}
