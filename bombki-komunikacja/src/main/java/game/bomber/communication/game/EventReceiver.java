package game.bomber.communication.game;

import game.bomber.common.Event;

/**
 * @author KOlkiewicz
 */
public interface EventReceiver {
    /**
     * Odbieranie wydarzenia
     * @return obiekt z danymi wydarzenia lub null jeśli nie ma wydarzenia
     */
    Event getEvent();
}
