package game.bomber.communication.game;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import game.bomber.common.Event;
import game.bomber.common.GameConfig;
import game.bomber.common.MatchConfig;
import game.bomber.common.PlayerSimple;
import game.bomber.game.exceptions.WrongUsernameException;
import game.bomber.common.Action;
import game.bomber.common.Request;

/**
 * @author KOlkiewicz
 */
public class GServerToGClients implements ActionReceiver, EventSender,
		MatchConfigSender, GameConfigSender {

	private ServerSocket actionWelcomeSocket;
	private ServerSocket eventWelcomeSocket;
	private ServerSocket gameConfigWelcomeSocket;
	private ServerSocket matchConfigWelcomeSocket;
	private BlockingQueue<Action> actions;

	private ServerSocket playerWelcomeSocket;
	private Runnable Connector;
	private Thread ConnectorThread;
	private HashMap<PlayerSimple, Sockets> sockets;
	private boolean running;
	private NewGameFrame newGameFrame;

	public GServerToGClients(int actionPort, int eventPort, int gameConfigPort,
			int matchConfigPort, int playerPort) {
		try {
			actionWelcomeSocket = new ServerSocket(actionPort);
			eventWelcomeSocket = new ServerSocket(eventPort);
			gameConfigWelcomeSocket = new ServerSocket(gameConfigPort);
			matchConfigWelcomeSocket = new ServerSocket(matchConfigPort);
			playerWelcomeSocket = new ServerSocket(playerPort);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actions = new LinkedBlockingQueue<Action>();

		sockets = new HashMap<PlayerSimple, Sockets>();
		running = true;
		Connector = new Runnable() {
			public void run() {
				while (running) {
					try {
						try {
							Socket incoming = playerWelcomeSocket.accept();
							ObjectInputStream inFromClient = new ObjectInputStream(
									incoming.getInputStream());
							ObjectOutputStream outToClient = new ObjectOutputStream(
									incoming.getOutputStream());
							PlayerSimple player = (PlayerSimple) inFromClient
									.readObject();
							int playerNumber = (int) inFromClient.readObject();

							if (playerNumber < 0) {
								playerNumber = getLowestId();
								for (Entry<PlayerSimple, Sockets> entry : sockets
										.entrySet()) {
									if (entry.getKey().getUsername()
											.equals(player.getUsername())) {
										playerNumber = 0;
										Map<String, String> myParams = new HashMap<String, String>();
										myParams.put("id",
												String.valueOf(playerNumber));
										outToClient
												.writeObject(new Request(
														Request.REQUEST.ANSWER_TO_INVITE,
														myParams));
										throw new WrongUsernameException();
									}
								}
								Map<String, String> myParams = new HashMap<String, String>();
								myParams.put("id", String.valueOf(playerNumber));
								outToClient.writeObject(new Request(
										Request.REQUEST.ANSWER_TO_INVITE,
										myParams));
							}
							boolean flag = false;
							synchronized (this) {
								for (Entry<PlayerSimple, Sockets> entry : sockets
										.entrySet()) {
									if (entry.getKey().getUsername()
											.equals(player.getUsername())) {
										entry.getValue().setPlayerSocket(
												incoming, inFromClient,
												outToClient);
										flag = true;
										Thread t = new Thread(entry.getValue());
										t.start();
									}
								}

								if (flag == false) {
									Sockets mySocket = new Sockets(playerNumber);
									mySocket.setPlayerSocket(incoming,
											inFromClient, outToClient);
									sockets.put(player, mySocket);
									Thread t = new Thread(mySocket);
									t.start();
								}
							}
						} catch (ClassNotFoundException e1) {
							e1.printStackTrace();
						} catch (IOException e) {
						}
						try {
							Socket incoming = actionWelcomeSocket.accept();
							ObjectInputStream inFromClient = new ObjectInputStream(
									incoming.getInputStream());
							ObjectOutputStream outToClient = new ObjectOutputStream(
									incoming.getOutputStream());
							PlayerSimple player = (PlayerSimple) inFromClient
									.readObject();
							int playerNumber = (int) inFromClient.readObject();

							boolean flag = false;
							synchronized (this) {
								for (Entry<PlayerSimple, Sockets> entry : sockets
										.entrySet()) {
									if (entry.getKey().getUsername()
											.equals(player.getUsername())) {
										entry.getValue().setActionSocket(
												incoming, inFromClient,
												outToClient);
										flag = true;
									}
								}
								if (flag == false) {
									Sockets mySocket = new Sockets(playerNumber);
									mySocket.setActionSocket(incoming,
											inFromClient, outToClient);

									sockets.put(player, mySocket);
								}
							}
						} catch (ClassNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e) {
						}
						try {
							Socket incoming = eventWelcomeSocket.accept();
							ObjectInputStream inFromClient = new ObjectInputStream(
									incoming.getInputStream());
							ObjectOutputStream outToClient = new ObjectOutputStream(
									incoming.getOutputStream());
							PlayerSimple player = (PlayerSimple) inFromClient
									.readObject();
							int playerNumber = (int) inFromClient.readObject();
							boolean flag = false;
							synchronized (this) {
								for (Entry<PlayerSimple, Sockets> entry : sockets
										.entrySet()) {
									if (entry.getKey().getUsername()
											.equals(player.getUsername())) {
										entry.getValue().setEventSocket(
												incoming, inFromClient,
												outToClient);
										flag = true;
									}
								}
								if (flag == false) {
									Sockets mySocket = new Sockets(playerNumber);
									mySocket.setEventSocket(incoming,
											inFromClient, outToClient);
									sockets.put(player, mySocket);
								}
							}
						} catch (ClassNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e) {
						}
						try {
							final Socket incoming = gameConfigWelcomeSocket.accept();
							Runnable r = new Runnable() {
								public void run() {
									try {
										ObjectInputStream inFromClient = new ObjectInputStream(
												incoming.getInputStream());
										ObjectOutputStream outToClient = new ObjectOutputStream(
												incoming.getOutputStream());
										PlayerSimple player = (PlayerSimple) inFromClient
												.readObject();
										int playerNumber = (int) inFromClient
												.readObject();
										boolean flag = false;
										synchronized (this) {
											for (Entry<PlayerSimple, Sockets> entry : sockets
													.entrySet()) {
												if (entry
														.getKey()
														.getUsername()
														.equals(player
																.getUsername())) {
													entry.getValue()
															.setGameConfigSocket(
																	incoming,
																	inFromClient,
																	outToClient);
													flag = true;
												}
											}

											if (flag == false) {
												Sockets mySocket = new Sockets(
														playerNumber);
												mySocket.setGameConfigSocket(
														incoming, inFromClient,
														outToClient);

												sockets.put(player, mySocket);
											}
										}
									} catch (ClassNotFoundException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
									}
								}
							};
							Thread t = new Thread(r);
							t.start();
						} catch (IOException e1) {
						}
						try {
							final Socket incoming = matchConfigWelcomeSocket.accept();
							Runnable r = new Runnable() {
								public void run() {
									try {
										ObjectInputStream inFromClient = new ObjectInputStream(
												incoming.getInputStream());
										ObjectOutputStream outToClient = new ObjectOutputStream(
												incoming.getOutputStream());
										PlayerSimple player = (PlayerSimple) inFromClient
												.readObject();
										int playerNumber = (int) inFromClient
												.readObject();
										boolean flag = false;
										synchronized (this) {
											for (Entry<PlayerSimple, Sockets> entry : sockets
													.entrySet()) {
												if (entry
														.getKey()
														.getUsername()
														.equals(player
																.getUsername())) {
													entry.getValue()
															.setMatchConfigSocket(
																	incoming,
																	inFromClient,
																	outToClient);
													flag = true;
												}
											}

											if (flag == false) {
												Sockets mySocket = new Sockets(
														playerNumber);
												mySocket.setMatchConfigSocket(
														incoming, inFromClient,
														outToClient);

												sockets.put(player, mySocket);
											}
										}
									} catch (IOException
											| ClassNotFoundException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							};
							Thread t = new Thread(r);
							t.start();
						} catch (IOException e1) {
						}
					} catch (WrongUsernameException e) {
					}
				}
			}
		};
		ConnectorThread = new Thread(Connector);
		ConnectorThread.start();

	}

	@Override
	public Action getAction() {
		return actions.poll();
	}

	@Override
	public void sendEvent(Event event) {
		for (Entry<PlayerSimple, Sockets> entry : sockets.entrySet()) {
			try {
				entry.getValue().eventToClient.writeObject(event);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void sendGameConfig(GameConfig gameConfig) {
		for (Entry<PlayerSimple, Sockets> entry : sockets.entrySet()) {
			try {
				entry.getValue().gameConfigToClient.writeObject(gameConfig);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void sendMatchConfig(MatchConfig matchConfig) {
		for (Entry<PlayerSimple, Sockets> entry : sockets.entrySet()) {
			try {
				entry.getValue().matchConfigToClient.writeObject(matchConfig);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void sendRequest(Request request) {
		synchronized (this) {
			for (Entry<PlayerSimple, Sockets> entry : sockets.entrySet()) {
				try {
					entry.getValue().playerToClient.writeObject(request);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public String getIP() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public String getActionPort() {
		return String.valueOf(actionWelcomeSocket.getLocalPort());
	}

	public String getEventPort() {
		return String.valueOf(eventWelcomeSocket.getLocalPort());
	}

	public String getGameConfigPort() {
		return String.valueOf(gameConfigWelcomeSocket.getLocalPort());
	}

	public String getMatchConfigPort() {
		return String.valueOf(matchConfigWelcomeSocket.getLocalPort());
	}

	public String getPlayerPort() {
		return String.valueOf(playerWelcomeSocket.getLocalPort());
	}

	public PlayerSimple[] getPlayersList() {
		synchronized (this) {
			int size = sockets.size();
			PlayerSimple[] list = new PlayerSimple[size];
			for (Entry<PlayerSimple, Sockets> entry : sockets.entrySet()) {
				list[entry.getValue().getPlayerNumber() - 1] = entry.getKey();
			}
			return list;
		}
	}

	private int getLowestId() {
		int i = 1;
		boolean flag = true;
		while (flag) {
			flag = false;
			for (Entry<PlayerSimple, Sockets> entry : sockets.entrySet()) {
				if (entry.getValue().getPlayerNumber() == i) {
					i++;
					flag = true;
					break;
				}
			}
		}
		return i;
	}

	public void setNewGameFrame(NewGameFrame newGameFrame) {
		this.newGameFrame = newGameFrame;
	}

	public void close() {
		running = false;
		ConnectorThread.interrupt();
		for (Entry<PlayerSimple, Sockets> entry : sockets.entrySet()) {
			entry.getValue().close();
		}
		try {
			actionWelcomeSocket.close();
			eventWelcomeSocket.close();
			gameConfigWelcomeSocket.close();
			matchConfigWelcomeSocket.close();
			playerWelcomeSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class Sockets implements Runnable {

		private boolean status;
		private int playerNumber;

		Socket actionSocket;
		ObjectInputStream actionFromClient;
		ObjectOutputStream actionConfirmationToClient;
		Runnable actionListener;
		Thread actionListenerThread;
		private boolean actionListenerRunning;

		Socket eventSocket;
		ObjectInputStream eventConfirmationFromClient;
		ObjectOutputStream eventToClient;

		Socket gameConfigSocket;
		ObjectInputStream gameConfigConfirmationFromClient;
		ObjectOutputStream gameConfigToClient;

		Socket matchConfigSocket;
		ObjectInputStream matchConfigConfirmationFromClient;
		ObjectOutputStream matchConfigToClient;

		Socket playerSocket;
		ObjectInputStream playerFromClient;
		ObjectOutputStream playerToClient;
		private boolean running;

		public Sockets(int playerNumber) {
			running = true;
			status = false;
			this.playerNumber = playerNumber;
		}

		public void setActionSocket(Socket actionSocket,
				ObjectInputStream inputStream, ObjectOutputStream outputStream) {
			this.actionSocket = actionSocket;
			actionFromClient = inputStream;
			actionConfirmationToClient = outputStream;
			actionListenerRunning = true;
			actionListener = new Runnable() {
				public void run() {
					while (actionListenerRunning) {
						try {
							Action action = (Action) actionFromClient
									.readObject();
							try {
								actions.put(action);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
						}
					}
				}
			};
			actionListenerThread = new Thread(actionListener);
			actionListenerThread.start();
		}

		public void setEventSocket(Socket eventSocket,
				ObjectInputStream inputStream, ObjectOutputStream outputStream) {
			this.eventSocket = eventSocket;
			eventConfirmationFromClient = inputStream;
			eventToClient = outputStream;
		}

		public void setGameConfigSocket(Socket gameConfigSocket,
				ObjectInputStream inputStream, ObjectOutputStream outputStream) {
			this.gameConfigSocket = gameConfigSocket;
			gameConfigConfirmationFromClient = inputStream;
			gameConfigToClient = outputStream;
		}

		public void setMatchConfigSocket(Socket matchConfigSocket,
				ObjectInputStream inputStream, ObjectOutputStream outputStream) {
			this.matchConfigSocket = matchConfigSocket;
			matchConfigConfirmationFromClient = inputStream;
			matchConfigToClient = outputStream;
		}

		public void setPlayerSocket(Socket playerSocket,
				ObjectInputStream inputStream, ObjectOutputStream outputStream) {
			this.playerSocket = playerSocket;
			playerFromClient = inputStream;
			playerToClient = outputStream;
		}

		public boolean getStatus() {
			return status;
		}

		public int getPlayerNumber() {
			return this.playerNumber;
		}

		public void close() {
			try {
				running = false;
				actionListenerRunning = false;

				actionListenerThread.interrupt();

				actionFromClient.close();
				actionConfirmationToClient.close();
				actionSocket.close();

				eventConfirmationFromClient.close();
				eventToClient.close();
				eventSocket.close();

				gameConfigConfirmationFromClient.close();
				gameConfigToClient.close();
				gameConfigSocket.close();

				matchConfigConfirmationFromClient.close();
				matchConfigToClient.close();
				matchConfigSocket.close();

				playerFromClient.close();
				playerToClient.close();
				playerSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void run() {
			Map<String, String> params = new HashMap<String, String>();
			params.put("rozmiar", String.valueOf(sockets.size()));
			int num = 1;
			for (int i = 1; i <= 4; i++) {
				for (Entry<PlayerSimple, Sockets> entry : sockets.entrySet()) {
					if (entry.getValue().getPlayerNumber() == i) {
						params.put(
								entry.getKey().getUsername(),
								String.valueOf(num)
										+ String.valueOf(entry.getValue()
												.getStatus()));
						num++;
						break;
					}
				}
			}
			Request response = new Request(Request.REQUEST.PLAYERS_LIST, params);
			sendRequest(response);
			while (running) {
				try {
					Request request = (Request) playerFromClient.readObject();
					if (request.getRequestType() == Request.REQUEST.SET_STATUS) {
						if (request.getParam("status").equals("1")) {
							status = false;
							Map<String, String> myParams = new HashMap<String, String>();
							myParams.put("rozmiar",
									String.valueOf(sockets.size()));
							boolean ready = true;
							int number = 1;
							for (int i = 1; i <= 4; i++) {
								for (Entry<PlayerSimple, Sockets> entry : sockets
										.entrySet()) {
									if (entry.getValue().getPlayerNumber() == i) {
										myParams.put(
												entry.getKey().getUsername(),
												String.valueOf(number)
														+ String.valueOf(entry
																.getValue()
																.getStatus()));
										if (!entry.getValue().getStatus()) {
											ready = false;
										}
										number++;
										break;
									}
								}
							}
							Request myResponse = new Request(
									Request.REQUEST.PLAYERS_LIST, myParams);
							sendRequest(myResponse);
							if (ready) {
								Request myResponse2 = new Request(
										Request.REQUEST.PLAY_NOW, null);
								sendRequest(myResponse2);
							}
						}
						if (request.getParam("status").equals("2")) {
							status = true;
							Map<String, String> myParams = new HashMap<String, String>();
							myParams.put("rozmiar",
									String.valueOf(sockets.size()));
							boolean ready = true;
							int number = 1;
							for (int i = 1; i <= 4; i++) {
								for (Entry<PlayerSimple, Sockets> entry : sockets
										.entrySet()) {
									if (entry.getValue().getPlayerNumber() == i) {
										myParams.put(
												entry.getKey().getUsername(),
												String.valueOf(number)
														+ String.valueOf(entry
																.getValue()
																.getStatus()));
										if (!entry.getValue().getStatus()) {
											ready = false;
										}
										number++;
										break;
									}
								}
							}
							Request myResponse = new Request(
									Request.REQUEST.PLAYERS_LIST, myParams);
							sendRequest(myResponse);
							if (ready) {
								Request myResponse2 = new Request(
										Request.REQUEST.PLAY_NOW, null);
								sendRequest(myResponse2);
							}
						}
						if (request.getParam("status").equals("3")) {
							PlayerSimple playerSimple = null;
							Map<String, String> myParams = new HashMap<String, String>();
							myParams.put("rozmiar",
									String.valueOf(sockets.size() - 1));
							boolean ready = true;
							int number = 1;
							for (int i = 1; i <= 4; i++) {
								for (Entry<PlayerSimple, Sockets> entry : sockets
										.entrySet()) {
									if (entry.getValue().getPlayerNumber() == i) {
										if (entry.getValue().getPlayerNumber() != playerNumber) {
											myParams.put(
													entry.getKey()
															.getUsername(),
													String.valueOf(number)
															+ String.valueOf(entry
																	.getValue()
																	.getStatus()));
											number++;
											if (!entry.getValue().getStatus()) {
												ready = false;
											}
										} else {
											playerSimple = entry.getKey();
										}
									}
								}
							}
							sockets.remove(playerSimple);
							Request myResponse = new Request(
									Request.REQUEST.PLAYERS_LIST, myParams);
							sendRequest(myResponse);
							if (ready) {
								Request myResponse3 = new Request(
										Request.REQUEST.PLAY_NOW, null);
								sendRequest(myResponse3);
							}
							running = false;
							if (newGameFrame != null) {
								newGameFrame.updateStatus(
										playerSimple.getUsername(), false);
							}
							Request myResponse2 = new Request(
									Request.REQUEST.SET_STATUS, null);
							synchronized (this) {
								playerToClient.writeObject(myResponse2);
							}
							this.close();

						}
					}
					if (request.getRequestType() == Request.REQUEST.PLAYERS_LIST) {

					}
					if (request.getRequestType() == Request.REQUEST.PLAY_NOW) {

					}
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
				}
			}

		}
	}
}
