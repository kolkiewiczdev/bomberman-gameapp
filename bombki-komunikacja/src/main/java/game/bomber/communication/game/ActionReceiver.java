package game.bomber.communication.game;

import game.bomber.common.Action;

/**
 * @author KOlkiewicz
 */
public interface ActionReceiver {
    /**
     * Odbieranie akcji
     * @return obiekt z danymi akcji lub null jeśli nie ma akcji
     */
    Action getAction();
}
