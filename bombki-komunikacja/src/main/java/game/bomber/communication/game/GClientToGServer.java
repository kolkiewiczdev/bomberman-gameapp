package game.bomber.communication.game;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import game.bomber.common.Action;
import game.bomber.common.Event;
import game.bomber.common.GameConfig;
import game.bomber.common.MatchConfig;
import game.bomber.common.PlayerSimple;
import game.bomber.common.Request;
import game.bomber.game.exceptions.NoSuchServerException;
import game.bomber.game.exceptions.WrongUsernameException;

/**
 * @author KOlkiewicz
 */
public class GClientToGServer implements ActionSender, EventReceiver,
		GameConfigReceiver, MatchConfigReceiver {

	private Socket actionSocket;
	private ObjectInputStream actionConfirmationFromServer;
	private ObjectOutputStream actionToServer;
	//private BlockingQueue<Action> actions;

	private Socket eventSocket;
	private ObjectInputStream eventFromServer;
	private ObjectOutputStream eventConfirmationToServer;
	private BlockingQueue<Event> events;
	private Runnable eventReceiver;
	private Thread eventReceiverThread;
	private boolean eventReceiverRunning;

	private Socket gameConfigSocket;
	private ObjectInputStream gameConfigFromServer;
	private ObjectOutputStream gameConfigConfirmationToServer;
	private BlockingQueue<GameConfig> gameConfigs;
	private Runnable gameConfigReceiver;
	private Thread gameConfigReceiverThread;
	private boolean gameConfigReceiverRunning;

	private Socket matchConfigSocket;
	private ObjectInputStream matchConfigFromServer;
	private ObjectOutputStream matchConfigConfirmationToServer;
	private BlockingQueue<MatchConfig> matchConfigs;
	private Runnable matchConfigReceiver;
	private Thread matchConfigReceiverThread;
	private boolean matchConfigReceiverRunning;

	private Socket playerSocket;
	private ObjectInputStream playerFromServer;
	private ObjectOutputStream playerToServer;
	private LinkedList<Request> requests;
	private Runnable requestListener;
	private Thread requestListenerThread;
	private boolean requestListenerRunning;
	private Runnable requestExecutor;
	private Thread requestExecutorThread;
	private boolean requestExecutorRunning;
	private String username;
	private Map<String, String> lastPlayerList;

	private GameStarterFrame gameStarterFrame;

	public GClientToGServer(String host, int actionPort, int eventPort,
			int gameConfigPort, int matchConfigPort, int playerPort,
			PlayerSimple player, int playerNumber) throws WrongUsernameException, NoSuchServerException {

		events = new LinkedBlockingQueue<Event>();
		gameConfigs = new LinkedBlockingQueue<GameConfig>();
		matchConfigs = new LinkedBlockingQueue<MatchConfig>();
		try {
			this.username = player.getUsername();
			playerSocket = new Socket(host, playerPort);
			playerToServer = new ObjectOutputStream(
					playerSocket.getOutputStream());
			playerFromServer = new ObjectInputStream(
					playerSocket.getInputStream());
			playerToServer.writeObject(player);
			playerToServer.writeObject(playerNumber);
			int newPlayerNumber=playerNumber;
			
			if(playerNumber==-1){
				try {
					Request newPlayerNumberRequest= (Request) playerFromServer.readObject();
					newPlayerNumber = Integer.valueOf(newPlayerNumberRequest.getParam("id"));
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(newPlayerNumber==0){
				throw new WrongUsernameException();
			}
			actionSocket = new Socket(host, actionPort);
			actionToServer = new ObjectOutputStream(
					actionSocket.getOutputStream());
			actionConfirmationFromServer = new ObjectInputStream(
					actionSocket.getInputStream());
			actionToServer.writeObject(player);
			actionToServer.writeObject(newPlayerNumber);

			eventSocket = new Socket(host, eventPort);
			eventConfirmationToServer = new ObjectOutputStream(
					eventSocket.getOutputStream());
			eventFromServer = new ObjectInputStream(
					eventSocket.getInputStream());
			eventConfirmationToServer.writeObject(player);
			eventConfirmationToServer.writeObject(newPlayerNumber);

			gameConfigSocket = new Socket(host, gameConfigPort);
			gameConfigConfirmationToServer = new ObjectOutputStream(
					gameConfigSocket.getOutputStream());
			gameConfigFromServer = new ObjectInputStream(
					gameConfigSocket.getInputStream());
			gameConfigConfirmationToServer.writeObject(player);
			gameConfigConfirmationToServer.writeObject(newPlayerNumber);

			matchConfigSocket = new Socket(host, matchConfigPort);
			matchConfigConfirmationToServer = new ObjectOutputStream(
					matchConfigSocket.getOutputStream());
			matchConfigFromServer = new ObjectInputStream(
					matchConfigSocket.getInputStream());
			matchConfigConfirmationToServer.writeObject(player);
			matchConfigConfirmationToServer.writeObject(newPlayerNumber);


			requests = new LinkedList<Request>();
			requestListenerRunning = true;
			requestListener = new Runnable() {
				public void run() {
					while (requestListenerRunning) {
						try {
							Request request = (Request) playerFromServer
									.readObject();
							synchronized (this) {
								requests.add(request);
							}
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							requestListenerRunning = false;
						}
					}

				}
			};
			requestListenerThread = new Thread(requestListener);
			requestListenerThread.start();
			requestExecutorRunning = true;
			requestExecutor = new Runnable() {
				public void run() {
					while (requestExecutorRunning) {
						synchronized (this) {
							if (requests.size() > 0) {
								Request request = requests.pop();
								if (request.getRequestType() == Request.REQUEST.PLAYERS_LIST) {
									lastPlayerList = request.getParams();
									if (gameStarterFrame != null) {
										gameStarterFrame.removeAll();
										gameStarterFrame.addUsers(request
												.getParams());
									}
								}
								if (request.getRequestType() == Request.REQUEST.SET_STATUS) {
									requestExecutorRunning = false;
									requestListenerRunning = false;
									try {
										this.finalize();
									} catch (Throwable e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								}
								if (request.getRequestType() == Request.REQUEST.PLAY_NOW) {
									if (gameStarterFrame != null) {
										gameStarterFrame.startGame();
									}
								}
							}
						}

					}
				}
			};
			requestExecutorThread = new Thread(requestExecutor);
			requestExecutorThread.start();

			eventReceiverRunning = true;
			eventReceiver = new Runnable() {
				public void run() {
					while (eventReceiverRunning) {
						try {
							Event event = (Event) eventFromServer.readObject();
							try {
								events.put(event);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							eventReceiverRunning=false;
						}
					}
				}
			};
			eventReceiverThread = new Thread(eventReceiver);
			eventReceiverThread.start();

			matchConfigReceiverRunning = true;
			matchConfigReceiver = new Runnable() {
				public void run() {
					while (matchConfigReceiverRunning) {
						try {
							MatchConfig matchConfig = (MatchConfig) matchConfigFromServer
									.readObject();
							try {
								matchConfigs.put(matchConfig);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							matchConfigReceiverRunning=false;
						}
					}
				}
			};
			matchConfigReceiverThread = new Thread(matchConfigReceiver);
			matchConfigReceiverThread.start();

			gameConfigReceiverRunning = true;
			gameConfigReceiver = new Runnable() {
				public void run() {
					while (gameConfigReceiverRunning) {
						try {
							GameConfig gameConfig = (GameConfig) gameConfigFromServer
									.readObject();
							try {
								gameConfigs.put(gameConfig);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							gameConfigReceiverRunning=false;
						}
					}
				}
			};
			gameConfigReceiverThread = new Thread(gameConfigReceiver);
			gameConfigReceiverThread.start();

		} catch (IOException e) {
			try {
				finalize();
			} catch (Throwable e1) {
			}
			throw new NoSuchServerException();
		} 
	}

	public MatchConfig getMatchConfig() {
		return matchConfigs.poll();
	}

	public GameConfig getGameConfig() {
		return gameConfigs.poll();
	}

	public Event getEvent() {
		return events.poll();
	}

	public void sendAction(Action action) {
		try {
			actionToServer.writeObject(action);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sendRequest(Request request) {
		synchronized (this) {
			try {
				playerToServer.writeObject(request);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void setGameStarterFrame(GameStarterFrame gameStarterFrame) {
		this.gameStarterFrame = gameStarterFrame;
		if (gameStarterFrame != null) {
			if (lastPlayerList != null) {
				gameStarterFrame.removeAll();
				
				gameStarterFrame.addUsers(lastPlayerList);
			}
		}
	}

	public String getUsername() {
		return username;
	}

	public void finalize() throws Throwable {
		requestExecutorRunning = false;
		requestListenerRunning = false;
		eventReceiverRunning = false;
		matchConfigReceiverRunning = false;
		gameConfigReceiverRunning = false;
		try {
			requestListenerThread.interrupt();
			requestExecutorThread.interrupt();
			eventReceiverThread.interrupt();
			matchConfigReceiverThread.interrupt();
			gameConfigReceiverThread.interrupt();
		} catch (Exception e) {
		}
		actionConfirmationFromServer.close();
		actionToServer.close();
		eventFromServer.close();
		eventConfirmationToServer.close();
		gameConfigFromServer.close();
		gameConfigConfirmationToServer.close();
		matchConfigFromServer.close();
		matchConfigConfirmationToServer.close();
		playerFromServer.close();
		playerToServer.close();

		playerSocket.close();
		actionSocket.close();
		eventSocket.close();
		gameConfigSocket.close();
		matchConfigSocket.close();

		super.finalize();
	}

}
