package game.bomber.communication.game;

import game.bomber.common.GameConfig;

/**
 * @author KOlkiewicz
 */
public interface GameConfigSender {
    /**
     * Wysylanie ustawien gry
     * @param gameConfig ustawienia gry
     */
    void sendGameConfig(GameConfig gameConfig);
}
