package game.bomber.communication.game;

import game.bomber.common.Event;

/**
 * @author KOlkiewicz
 */
public interface EventSender {
    /**
     * Wysylanie wydarzenia
     * @param event wydarzenie
     */
    void sendEvent(Event event);
}
