package game.bomber.communication.game;

import game.bomber.common.GameConfig;

/**
 * @author KOlkiewicz
 */
public interface GameConfigReceiver {
    /**
     * Odbieranie konfiguracji gry
     * @return Konfiguracja gry
     */
    GameConfig getGameConfig();
}
