package game.bomber.communication.game;

import game.bomber.common.MatchConfig;

/**
 * @author KOlkiewicz
 */
public interface MatchConfigSender {
    /**
     * Wysylanie ustawien meczu
     * @param matchConfig ustawienia meczu
     */
    void sendMatchConfig(MatchConfig matchConfig);
}
