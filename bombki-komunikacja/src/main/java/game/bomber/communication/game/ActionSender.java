package game.bomber.communication.game;

import game.bomber.common.Action;

/**
 * @author KOlkiewicz
 */
public interface ActionSender {
    /**
     * Wysylanie akcji
     * @param action akcja
     */
    void sendAction(Action action);
}
