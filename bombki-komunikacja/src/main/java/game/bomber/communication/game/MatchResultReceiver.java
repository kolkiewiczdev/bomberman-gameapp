package game.bomber.communication.game;

import game.bomber.common.MatchResult;

/**
 * @author KOlkiewicz
 */
public interface MatchResultReceiver {
    /**
     * Odbieranie wyniku meczu
     * @return Wynik meczu
     */
    MatchResult getMatchResult();
}
