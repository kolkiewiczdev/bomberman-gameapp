package game.bomber.communication.game;

import game.bomber.common.MatchResult;

/**
 * @author KOlkiewicz
 */
public interface MatchResultSender {
    /**
     * Wysylanie wyniku meczu
     * @param matchResult wynik meczu
     */
    void sendMatchResult(MatchResult matchResult);
}
