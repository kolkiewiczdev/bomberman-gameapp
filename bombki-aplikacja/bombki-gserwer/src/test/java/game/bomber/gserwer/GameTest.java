package game.bomber.gserwer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import game.bomber.common.Action;
import game.bomber.common.Event;
import game.bomber.common.PlayerSimple;
import game.bomber.game.Board;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.Assert.*;

public class GameTest {
    private static final long maxTime = 0;
    private static final PlayerSimple ps1 = new PlayerSimple(1, "gracz_1");
    private static final PlayerSimple ps2 = new PlayerSimple(2, "gracz_2");
    private static final PlayerSimple ps3 = new PlayerSimple(103, "gracz_103");
    private static final PlayerSimple ps4 = new PlayerSimple(444, "gracz_444");

    BlockingQueue<Action> actionQueue;
    BlockingQueue<Event> eventQueue;
    Game game;


    private Board generateBoard() {
        return null;
    }

    private PlayerSimple[] generatePlayerSimple() {
            return new PlayerSimple[] {ps1, ps2, ps3, ps4};
    }

    @Before
    public void setUp() throws Exception {
        actionQueue = new LinkedBlockingQueue<Action>();
        eventQueue = new LinkedBlockingQueue<Event>();
        // Gotowosc graczy, potrzebne dla init()
        actionQueue.add(new Action(Action.ACTION.READY, ps1.getId()));
        actionQueue.add(new Action(Action.ACTION.READY, ps2.getId()));
        actionQueue.add(new Action(Action.ACTION.READY, ps3.getId()));
        actionQueue.add(new Action(Action.ACTION.READY, ps4.getId()));

    }

    @After
    public void tearDown() throws Exception {
        actionQueue = null;
        eventQueue = null;
        game = null;
    }

    @Test
    public void testInit() throws Exception {
        game = new Game(generateBoard(), generatePlayerSimple(), maxTime, actionQueue, eventQueue);
        assertTrue("NIE ZAINICJOWANO GRY!", game.init());
        //powinny byc w kolejce: UP, DOWN, RIGHT, LEFT, START_GAMR
        for (int i = 0; i < 6; ++i) {
            Event event = eventQueue.take();
            assertNotNull("BRAKUJACY EVENT W KOLEJCE!", event);
            if (i==0) {
                assertTrue("BRAK EVENTU INIT_GAME!", event.getEventType()== Event.EVENT.INIT_GAME);
            }
            if (i==1) {
                assertTrue("BRAK EVENTU UP!", event.getEventType()== Event.EVENT.UP);
            }
            else if (i==2) {
                assertTrue("BRAK EVENTU DOWN!", event.getEventType()== Event.EVENT.DOWN);
            }
            else if (i==3) {
                assertTrue("BRAK EVENTU RIGTH!", event.getEventType()== Event.EVENT.RIGTH);
            }
            else if (i==4) {
                assertTrue("BRAK EVENTU LEFT!", event.getEventType()== Event.EVENT.LEFT);
            }
            else if (i==5) {
                assertTrue("BRAK EVENTU START_GAME!", event.getEventType()== Event.EVENT.START_GAME);
            }
        }
    }
}