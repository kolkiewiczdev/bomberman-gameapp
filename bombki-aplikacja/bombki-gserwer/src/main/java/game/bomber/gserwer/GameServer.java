package game.bomber.gserwer;

import game.bomber.common.Action;
import game.bomber.common.Event;
import game.bomber.common.PlayerSimple;
import game.bomber.communication.game.ActionReceiver;
import game.bomber.communication.game.EventSender;
import game.bomber.communication.game.MatchResultSender;
import game.bomber.common.MatchConfig;
import game.bomber.common.MatchResult;

import java.util.concurrent.*;

/**
 * @author KOlkiewicz
 */
public class GameServer implements Runnable {
    MatchConfig config;
    PlayerSimple[] playerSimples;
    ActionReceiver actionReceiver;
    EventSender eventSender;
    BlockingQueue<Action> actionQueue;
    BlockingQueue<Event> eventQueue;
    MatchResultSender matchResultSender;

    /**
     *
     * @param config Konfiguracja gry
     * @param playerSimples Tablica informacji o graczach
     * @param actionReceiver Interfejs odbierajacy akcje
     * @param eventSender Interfejs wysylajacy wydarzenia
     * @param matchResultSender Interfejs wysylajacy wyniki meczu
     */
    public GameServer(MatchConfig config, PlayerSimple[] playerSimples, ActionReceiver actionReceiver, EventSender eventSender, MatchResultSender matchResultSender) {
        this.config = config;
        this.playerSimples = playerSimples;
        this.actionReceiver = actionReceiver;
        this.eventSender = eventSender;
        actionQueue = new LinkedBlockingQueue<Action>();
        eventQueue = new LinkedBlockingQueue<Event>();
        this.matchResultSender = matchResultSender;
    }

    /**
     * Serwer gry rozpoczyna watek odbierania akcji, wysylania wydarzen, rozgrywania meczu.
     * Klasa Match implementuje callable, metoda call zwraca wynik gry, ktory jest wysylany do serwera.
     * Watki komunikuja sie za pomoca dwoch kolejek, jedna przechowywuje odebrane akcje, druga wydarzenia do wyslania.
     */
    @Override
    public void run() {
        ExecutorService pool = Executors.newFixedThreadPool(3);
        pool.submit(new Sender(eventQueue, eventSender));
        pool.submit(new Receiver(actionReceiver, actionQueue));
        Future<MatchResult> future = pool.submit(new Match(config, playerSimples, actionQueue, eventQueue));

        // po zakonczeniu gry pobrac wyniki i wyslac je
        try {
            matchResultSender.sendMatchResult(future.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
