package game.bomber.gserwer;

import game.bomber.common.Action;
import game.bomber.communication.game.ActionReceiver;

import java.util.concurrent.BlockingQueue;

/**
 * @author KOlkiewicz
 */
public class Receiver implements Runnable{
    ActionReceiver actionReceiver;
    BlockingQueue<Action> actionQueue;

    public Receiver(ActionReceiver actionReceiver, BlockingQueue<Action> actionQueue) {
        this.actionReceiver = actionReceiver;
        this.actionQueue = actionQueue;
    }

    /**
     * pobieranie akcji i przekazanie jej do kolejki
     */
    @Override
    public void run() {
        boolean end = false;
        while(!end) {
            try {
                // pobranie akcji
                Action action = actionReceiver.getAction();
                if (action != null) {
                    // dodanie jej do kolejki
                    actionQueue.put(action);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }
}
