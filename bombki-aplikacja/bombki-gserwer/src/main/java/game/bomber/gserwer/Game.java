package game.bomber.gserwer;

import game.bomber.common.*;
import game.bomber.game.Board;
import game.bomber.game.Bomb;
import game.bomber.game.GameBase;
import game.bomber.game.Player;
import game.bomber.game.exceptions.WrongPlayerIdException;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author KOlkiewicz
 */
public class Game extends GameBase {

    private BlockingQueue<Action> actionQueue;
    private BlockingQueue<Event> eventQueue;

    public Game(Board board, PlayerSimple[] players, long maxTime, BlockingQueue<Action> actionQueue, BlockingQueue<Event> eventQueue) {
        super(board, players, maxTime);
        this.actionQueue = actionQueue;
        this.eventQueue = eventQueue;

    }

    /**
     * 1. Wyslanie INIT_GAME, tworzenie tablicy graczym wyslanie, rozstawienie graczy
     */
    @Override
    protected boolean init() {
        eventQueue.add(new Event(Event.EVENT.INIT_GAME, 0, 0, 0));
        players = new Player[playerSimples.length];

        // rozstawienie graczy
        if(playerNumber <= 0)
            return false;
        players[0] = new Player(playerSimples[0].getId(), playerSimples[0].getUsername(), 0);
        eventQueue.add(new Event(Event.EVENT.UP, GlobalConf.START_XY[0][0], GlobalConf.START_XY[0][1], players[0].getId()));

        if(playerNumber <= 1)
            return true;
        players[1] = new Player(playerSimples[1].getId(), playerSimples[1].getUsername(), 1);
        eventQueue.add(new Event(Event.EVENT.DOWN, GlobalConf.START_XY[1][0], GlobalConf.START_XY[1][1], players[1].getId()));

        if(playerNumber <= 2)
            return true;
        players[2] = new Player(playerSimples[2].getId(), playerSimples[2].getUsername(), 2);
        eventQueue.add(new Event(Event.EVENT.RIGTH, GlobalConf.START_XY[2][0], GlobalConf.START_XY[2][1], players[2].getId()));

        if(playerNumber <= 3)
            return true;
        players[3] = new Player(playerSimples[3].getId(), playerSimples[3].getUsername(), 3);
        eventQueue.add(new Event(Event.EVENT.LEFT, GlobalConf.START_XY[3][0], GlobalConf.START_XY[3][1], players[0].getId()));

        if (waitForReady()) {
            eventQueue.add(new Event(Event.EVENT.START_GAME, 0, 0, 0));
            return true;
        }
        return false;
    }

    private boolean waitForReady() {
        long waitStart = System.currentTimeMillis();
        Set<Integer> ready = new HashSet<Integer>();
        Action a = null;
        while (System.currentTimeMillis() - waitStart < 60*1000) { 

            try {
                a = actionQueue.poll(1, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (a != null) {
                if (a.getActionType() == Action.ACTION.READY) {
                    ready.add(a.getPlayerId());
                }
                if (ready.size()==playerNumber)
                    return true;
            }
        }
        return false;
    }



    private int getWinner() {
        boolean atLastOneLives = false, atLastTwoLives = false;
        int winPlayerId = 0;
        for (Player p : players) {
            if (p.isLive()) {
                if (atLastOneLives) {
                    atLastTwoLives = true;
                }
                else {
                    atLastOneLives = true;
                    winPlayerId = p.getId();
                }
            }
        }
        if (atLastTwoLives)
            return 0;
        if (atLastOneLives)
            return winPlayerId;
        return 0;
    }

    private boolean isFieldEmpty(Position pos) {
        if (board.getField(pos.getX(), pos.getY()).isEmpty()) {
            for (int i = 0; i < players.length; ++i) {
                if (players[i].getPosition().getX()==pos.getX() && players[i].getPosition().getY()==pos.getY())
                    return true;
                	//return false;
            }
            return true;
        }
        return false;
    }

    private void checkPowerup(Position position, Player player) {
        if (board.getField(position.getX(), position.getY()).hasPowerup()) {
            board.getField(position.getX(), position.getY()).removePowerup().activate(player);
            eventQueue.add( new Event(Event.EVENT.GETS_POWERUP, position.getX(), position.getY(), player.getId() ) );
        }
    }

    private void doAction(Action action) {
		if (action == null)
			return;
		try {
			Position pos = new Position(getPlayer(action.getPlayerId())
					.getPosition().getX(), getPlayer(action.getPlayerId())
					.getPosition().getY());
			Player player = getPlayer(action.getPlayerId());
			if (action.getActionType() == Action.ACTION.DOWN) {
				if (!player.isInMove(time) && pos.canDown()) {
					pos.down();
					if (isFieldEmpty(pos)) {
						player.startMove(time);
						getPlayer(action.getPlayerId()).getPosition().down();
						eventQueue
								.add(new Event(action, pos.getX(), pos.getY()));
						checkPowerup(pos, player);
					}
				}
			} else if (action.getActionType() == Action.ACTION.UP) {				
				if (!player.isInMove(time) && pos.canUp()) {					
					pos.up();
					if (isFieldEmpty(pos)) {						
						player.startMove(time);
						getPlayer(action.getPlayerId()).getPosition().up();
						eventQueue
								.add(new Event(action, pos.getX(), pos.getY()));
						checkPowerup(pos, player);
					}
				}
			} else if (action.getActionType() == Action.ACTION.LEFT) {
				if (!player.isInMove(time) && pos.canLeft()) {
					pos.left();
					if (isFieldEmpty(pos)) {
						player.startMove(time);
						getPlayer(action.getPlayerId()).getPosition().left();
						eventQueue
								.add(new Event(action, pos.getX(), pos.getY()));
						checkPowerup(pos, player);
					}
				}
			} else if (action.getActionType() == Action.ACTION.RIGTH) {
				if (!player.isInMove(time) && pos.canRight()) {
					pos.right();
					if (isFieldEmpty(pos)) {
						player.startMove(time);
						getPlayer(action.getPlayerId()).getPosition().right();
						eventQueue
								.add(new Event(action, pos.getX(), pos.getY()));
						checkPowerup(pos, player);
					}
				}
			} else if (action.getActionType() == Action.ACTION.PUT_BOMB) {
				if (player.getBombs() < player.getMaxBombs()) {
					bombs.add(new Bomb(time, player, pos));
					eventQueue.add(new Event(action, pos.getX(), pos.getY()));
				}
			}
		} catch (WrongPlayerIdException e) {
			e.printStackTrace();
		}
	}

    private Player getPlaterFrom(int x, int y) {
        for (Player p : players) {
            if (p.getPosition().getX() == x && p.getPosition().getY() == y)
                return p;
        }
        return null;
    }

    private void explode(Bomb bomb) {
        int x = bomb.getPosition().getX();
        int y = bomb.getPosition().getY();
        int range = bomb.getOwner().getBombRange();
        boolean endExplosion[] = {false, false, false, false};
        for (int i = 0; i < range; ++i) {
            // jesli eksplozja moze isc dalej w tym kierunku
            if ( !endExplosion[0] && x + i < GlobalConf.WITHD) {
                Player player = getPlaterFrom(x+i,y);
                // jesli jest tam gracz
                if (player!=null) {
                    // niech ginie!
                    player.die(bomb.getExplodeTime());
                    eventQueue.add(new Event(Event.EVENT.DEATH, x+i, y, player.getId() ));
                    endExplosion[0] = true;
                }
                // nie ma gracza i jesli pole nie jest puste
                else if (!board.getField(x + i, y).isEmpty()) {
                    // wybuch i koniec eksplozji w tym kierunku
                    endExplosion[0] = true;
                    board.getField(x+i, y).destroy();
                }
            }

            if ( !endExplosion[1] && x - i >= 0) {
                Player player = getPlaterFrom(x-i,y);
                if (player!=null) {
                    player.die(bomb.getExplodeTime());
                    eventQueue.add(new Event(Event.EVENT.DEATH, x-i, y, player.getId() ));
                    endExplosion[1] = true;
                }
                else if (!board.getField(x - i, y).isEmpty()) {
                    endExplosion[1] = true;
                    board.getField(x-i, y).destroy();
                }
            }

            if ( !endExplosion[2] && y + i < GlobalConf.HEIGHT) {
                Player player = getPlaterFrom(x,y+i);
                if (player!=null) {
                    player.die(bomb.getExplodeTime());
                    eventQueue.add(new Event(Event.EVENT.DEATH, x, y+i, player.getId() ));
                    endExplosion[2] = true;
                }
                else if (!board.getField(x, y+i).isEmpty()) {
                    endExplosion[2] = true;
                    board.getField(x, y+i).destroy();
                }
            }

            if ( !endExplosion[3] && y - i >= 0) {
                Player player = getPlaterFrom(x,y-i);
                if (player!=null) {
                    player.die(bomb.getExplodeTime());
                    eventQueue.add(new Event(Event.EVENT.DEATH, x, y-i, player.getId() ));
                    endExplosion[3] = true;
                }
                else if (!board.getField(x, y-i).isEmpty()) {
                    endExplosion[3] = true;
                    board.getField(x, y-i).destroy();
                }
            }
        }

        bomb.getOwner().bombExplodes();        
        eventQueue.add(new Event(Event.EVENT.EXPLOSION, bomb.getPosition().getX(), bomb.getPosition().getY(), range ));
    }

    private void checkBombs() {
        if(!bombs.isEmpty()) {
            Bomb bomb = bombs.peek();
            if (bomb.getExplodeTime() <= time) {
                explode(bombs.remove());
            }
        }
    }

    protected void gameLoopStep() {    	
        // pobierz akcje
        try {
            Action action = actionQueue.poll(10, TimeUnit.MILLISECONDS);
            // przetworz akcje i wyslij wydarzenie
            doAction(action);
            // sprawdz bomby i wyslij wydarzenia
            checkBombs();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected int endGame() {
        int winner = getWinner();
        eventQueue.add(new Event(Event.EVENT.END_GAME, 0, 0, winner));
        return winner;
    }


}
