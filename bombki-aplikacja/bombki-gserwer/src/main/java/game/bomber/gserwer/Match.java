package game.bomber.gserwer;

import game.bomber.common.Action;
import game.bomber.common.Event;
import game.bomber.common.PlayerSimple;
import game.bomber.game.Board;
import game.bomber.game.BoardFactory;
import game.bomber.game.MatchBase;
import game.bomber.common.MatchConfig;
import game.bomber.common.MatchResult;
import game.bomber.game.exceptions.CannotBuildMapException;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;

/**
 * @author KOlkiewicz
 */
public class Match extends MatchBase implements Callable<MatchResult> {

    BlockingQueue<Action> actionQueue;
    BlockingQueue<Event> eventQueue;

    public Match(MatchConfig config, PlayerSimple[] playerSimples, BlockingQueue<Action> actionQueue, BlockingQueue<Event> eventQueue) {
        super(config, playerSimples);
        this.actionQueue = actionQueue;
        this.eventQueue = eventQueue;
    }


    @Override
    public int nextRound() throws GServerError{
        actionQueue.clear();
        eventQueue.clear();
        try {
            Board board = BoardFactory.buildBoard(config.getGameConfig());
            Game game = new Game(board, players, config.getGameConfig().getMaxTime(), actionQueue, eventQueue);
            return game.gameLoop();
        } catch ( CannotBuildMapException e ) {
            throw new GServerError("Cannot start round.", e);
        } finally {
            return 0;
        }

    }

    /**
     * petla glowna meczu
     * watek rozpoczyna mecz, tworzy i rozpoczyna rundy gry
     * @return Wyniki meczu
     */
    @Override
    public MatchResult call() {
        try {
                int winner = nextRound();
                ++rounds;
                if (winner>0) {
                    addWin(winner);
                }
        } catch (GServerError e) {

        } finally {
            return getMatchResult();
        }
    }

}
