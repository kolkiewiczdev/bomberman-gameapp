package game.bomber.gserwer;

import game.bomber.common.Event;
import game.bomber.communication.game.EventSender;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author KOlkiewicz
 */
public class Sender implements Runnable {
    BlockingQueue<Event> eventQueue;
    EventSender eventSender;

    public Sender(BlockingQueue<Event> eventQueue, EventSender eventSender) {
        this.eventQueue = eventQueue;
        this.eventSender = eventSender;
    }

    /**
     * pobieranie wydarzen z kolejki i wysylanie ich do klientow
     */
    @Override
    public void run() {
        boolean end = false;
        while(!end) {
            try {
                Event event = eventQueue.poll(10, TimeUnit.MILLISECONDS);
                if (event != null) {
                    eventSender.sendEvent(event);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }
}
