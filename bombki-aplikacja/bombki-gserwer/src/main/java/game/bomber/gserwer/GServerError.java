package game.bomber.gserwer;

import game.bomber.common.GameError;

/**
 * @author KOlkiewicz
 */
public class GServerError extends GameError {
    public GServerError() {
        super();
    }

    public GServerError(String message) {
        super(message);
    }

    public GServerError(String message, Throwable cause) {
        super(message, cause);
    }
}
