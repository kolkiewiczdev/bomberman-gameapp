package game.bomber.gklient;

import java.util.concurrent.BlockingQueue;

import javax.swing.JFrame;

import game.bomber.common.Action;
import game.bomber.common.Event;
import game.bomber.common.GameError;
import game.bomber.common.MatchConfig;
import game.bomber.common.PlayerSimple;
import game.bomber.game.Board;
import game.bomber.game.BoardFactory;
import game.bomber.game.MatchBase;
import game.bomber.game.exceptions.CannotBuildMapException;

/**
 * @author KOlkiewicz
 */
public class clientMatch extends MatchBase implements Runnable {
    BlockingQueue<Action> actionQueue;
    BlockingQueue<Event> eventQueue;
    int myID;
    JFrame parent;
	
	public clientMatch(MatchConfig config, PlayerSimple[] playerSimples, BlockingQueue<Action> actionQueue, BlockingQueue<Event> eventQueue, int myID, JFrame parent) {
		super(config, playerSimples);
		
		this.actionQueue = actionQueue;
        this.eventQueue = eventQueue;		
        this.myID = myID;
        this.parent = parent;
	}
	
    /**
     * Tworzy i zarzadza runda, po jej zakonczeniu aktualizuje stan meczu
     * @return id gracza, ktory wygral,
     */
	@Override
	public int nextRound() throws GameError {	
		System.out.println("Wywo�ano NEXTROUND()"); //
		
		actionQueue.clear();
        eventQueue.clear();
        try {
        	System.out.println("I'm here"); //
            Board board = BoardFactory.buildBoard(config.getGameConfig());
            System.out.println("Board created..."); // here is stopping
            clientGame game = new clientGame(board, players, config.getGameConfig().getMaxTime(), actionQueue, eventQueue, myID, parent);
            System.out.println("Utworzono ClientGame"); //         
            return game.gameLoop();
        } catch ( CannotBuildMapException e ) {        	
            throw new GServerError("Cannot start round.", e);
        } finally {
            return 0;
        } 	
	}

	@Override
	public void run() {
		try {			
			int result = nextRound();
			System.out.println("KlientGame Result: " + result);
		} catch (GameError e) {			
			e.printStackTrace();
		}
	}
}
