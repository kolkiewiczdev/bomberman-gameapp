package game.bomber.gklient;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import game.bomber.common.Action;
import game.bomber.communication.game.ActionSender;

/**
 * @author KOlkiewicz
 */
public class Sender implements Runnable{
	BlockingQueue<Action> actionQueue;
	ActionSender actionSender;
	
	public Sender(BlockingQueue<Action> actionQueue, ActionSender actionSender)
	{
		this.actionQueue = actionQueue;
		this.actionSender = actionSender;
	}
	
    /**
     * pobieranie akcji z kolejki i wysylanie ich do serwera
     */
	@Override
	public void run() {
		boolean end = false;
		while(!end){
			try {
				Action action = actionQueue.poll(10, TimeUnit.MILLISECONDS);
				if (action != null){
					actionSender.sendAction(action);
				}
					
			} catch (InterruptedException e) {				
				e.printStackTrace();
			}
		}
	}

}