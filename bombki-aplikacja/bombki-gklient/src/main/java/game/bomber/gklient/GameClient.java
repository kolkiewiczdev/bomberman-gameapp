package game.bomber.gklient;

import game.bomber.common.Action;
import game.bomber.common.Event;
import game.bomber.common.PlayerSimple;
import game.bomber.communication.game.ActionSender;
import game.bomber.communication.game.EventReceiver;
import game.bomber.common.MatchConfig;

import java.util.concurrent.*;

import javax.swing.JFrame;

/**
 * @author KOlkiewicz
 */
public class GameClient implements Runnable {
	PlayerSimple[] playerSimples;
    MatchConfig config;   
    ActionSender actionSender;
    EventReceiver eventReceiver;
    BlockingQueue<Action> actionQueue;
    BlockingQueue<Event> eventQueue;  
    int myID;
    JFrame parent;

    /**
     *
     * @param config Konfiguracja gry  
     * @param playerSimples Informacje o graczach   
     * @param actionSender Interfejs wysylajacy akcje
     * @param eventReceiver Interfejs odbierajacy wydarzenia
     * @param myID ID gracza.     
     */
    public GameClient(MatchConfig config, PlayerSimple[] playerSimples, ActionSender actionSender, EventReceiver eventReceiver, int myID, JFrame parent) {
        this.config = config;
        this.playerSimples = playerSimples;
        this.actionSender = actionSender;
        this.eventReceiver = eventReceiver;
        this.myID = myID;
        this.parent = parent;
        actionQueue = new LinkedBlockingQueue<Action>();
        eventQueue = new LinkedBlockingQueue<Event>();        
    }

    /**
     * Klient gry rozpoczyna watek odbierania wydarze?, wysylania akcji, rysowania ekranu gry.     
     * Watki komunikuja sie za pomoca dwoch kolejek, jedna przechowuje odebrane wydarzenia, druga akcje do wyslania.
     */
    @Override
    public void run() {
        ExecutorService pool = Executors.newFixedThreadPool(3);
        pool.submit(new Receiver(eventQueue, eventReceiver));
        pool.submit(new Sender(actionQueue, actionSender));
        pool.submit(new clientMatch(config, playerSimples, actionQueue, eventQueue, myID, parent));    
    }
}