package game.bomber.gklient;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import game.bomber.common.Action;
import game.bomber.common.Event;
import game.bomber.common.PlayerSimple;
import game.bomber.common.Position;
import game.bomber.common.Action.ACTION;
import game.bomber.game.*;

/**
 * @author KOlkiewicz
 */
public class clientGame extends GameBase {
	
    private BlockingQueue<Action> actionQueue;
    private BlockingQueue<Event> eventQueue;
    private int myPlayerID;
    private JFrame parent;
    
    Canvas canvas;   
    
    public static enum GameState {
        // Stan rysowania prosby o potwierdzenie gotowosci gracza
        CONFIRM_READY,
        // Stan rysowania informacji o oczekiwaniu na gotowosc przeciwnika
        WAIT_FOR_OPPONENT,
        // Stan rysowania aktualnego stanu gry
        START_GAME,
        // Stan rysowania informacji o smierci.
        DEATH, 
        // Stan rysowania informacji o koncu gry,
        END_GAME,   
    }
    
    private GameState gameState;
	
	private long InitstartTime;
	private long ReadyTime;
	private final long MaxTime2getReady = 60000; // minuta, czas na wyslanie sygnalu 'Ready'
	
	public clientGame(Board board, PlayerSimple[] playerSimples, long maxTime, BlockingQueue<Action> actionQueue, BlockingQueue<Event> eventQueue, int myID, JFrame parent) {
		super(board, playerSimples, maxTime);	
		
		this.actionQueue = actionQueue;		
        this.eventQueue = eventQueue;  
        this.myPlayerID = myID;
        this.parent = parent;
        
        gameState = GameState.CONFIRM_READY;  
	}

	/**
     * Inicjalizacja gry
     * @return false je?eli gracz nie zglosi 'Ready' w ciagu minuty od inicjalizacji.
     * @return true je?eli gracz zg?osi 'Ready' przed up?ywem minuty od inicjalizacji.
     */
	@Override
	protected boolean init() {
		
		boolean received = false;
		
		// oczekuj na event.init_game
		while(!received) {
        try {    	
        	Event event = eventQueue.poll(10, TimeUnit.MILLISECONDS);
            
            if (event != null && event.getEventType() == Event.EVENT.INIT_GAME)
            {
            	received = true;
            	
            	//inicjuj tablice players
            	players = new Player[playerSimples.length];
            	
            	for (int i = 0; i < playerSimples.length; i++)
            	{
            		players[i] = new Player(playerSimples[i].getId(), playerSimples[i].getUsername(), i);
            	}
            }            	
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	}

		boolean haveAllStartPos = false;	
		int getPosCount = 0;
		
		while(!haveAllStartPos){			
        // oczekuj na event z pozycjami startowymi graczy        
        try {        	
            Event event = eventQueue.poll(10, TimeUnit.MILLISECONDS);
            
            if (event != null)
            {
            	// przypisz pozycje startow� dla gracza
            	for(int i = 0; i < players.length; i++)
            	{
            		if(players[i].getId() == event.getP())
            		{
            			players[i].getPosition().setX(event.getX());
            			players[i].getPosition().setY(event.getY());
            			
            			getPosCount++;
            			
            			if(getPosCount == players.length)
            			{
            				haveAllStartPos = true;
            			}
            			break;
            		}            		
            	}
            }            	
        } catch (InterruptedException e) {
            e.printStackTrace();
        }                
	}

		try {
			actionQueue.put(new Action(ACTION.READY, players[1].getId()));
		} catch (InterruptedException e1) {			
			e1.printStackTrace();
		}
		
		InitstartTime = System.currentTimeMillis();
		
		while (gameState != GameState.START_GAME)
		{
			try {
				eventQueue.add(new Event(Event.EVENT.START_GAME, 0, 0, 0)); // test
	            Event event = eventQueue.poll(10, TimeUnit.MILLISECONDS);
	            
	            if (event != null && event.getEventType() == Event.EVENT.START_GAME) {
	            	gameState = GameState.START_GAME;
	            }
	            	         
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }	
			
			updateReadyTime();		
			
			if (ReadyTime > 0 && ReadyTime >= MaxTime2getReady)	{
				return false; //
			}				
		}
		
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>(){
			 
            @Override
            protected Void doInBackground() throws Exception {
            	
				JFrame frametest = new JFrame();
				frametest.add(canvas = new Canvas());
				frametest.setSize(new Dimension(325, 350));
				frametest.setLocationRelativeTo(parent);
				frametest.setVisible(true);
				return null;			
            }

            @Override
            protected void done() {
               System.out.println("done");
            }

       };
       worker.execute();  
   	
		System.out.println("Init true"); //
		return true;
	}
	
	/**
     * Pojedynczy krok glownej petli gry
     * Pobranie Eventu, aktualizacja stanu planszy
     */
	@Override
	protected void gameLoopStep() {		
		if(canvas != null){		
		// pobierz Event
        try {
            Event event = eventQueue.poll(10, TimeUnit.MILLISECONDS);
            if (event != null)            	
            	canvas.doEvent(event);             	
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	  }
	}
	
	 /**
     * Procedura konczenia gry
     * @return id zwyciezscy lub 0 jesli remis
     */
	@Override
	protected int endGame() {
		for(Player PlayerTmp: players)
		{
			if(PlayerTmp.isLive())
				return PlayerTmp.getId();
		}
		return 0;
	}
	
	 /**
     * Metoda aktualizujaca czas do osiagniecia gotowosci gry.  
     */
	private void updateReadyTime()
	{
		ReadyTime = (System.currentTimeMillis() - InitstartTime);
	}
	
	 /**
     * Klasa wewnetrzna rozszerzaj?ca JPanel w klasie clientGame
     * S?u?y do rysowania rozgrywki
     */
	class Canvas extends JPanel implements ActionListener
	{
		 private Image BlockDestructible;
		 private Image BlockNotDestructible;
		 private Image Bomb;
		 private Image BombNumberPU;
		 private Image BombRangePU;
		 private Image Extralife;
		 private Image SpeedPU;
		 private Image Field;
		 private Image ExplosionFire;
		 private Image IndestrubilityPU;
		 private Image BombermannUP;
		 private Image BombermannDown;
		 private Image BombermannLeft;
		 private Image BombermannRight;
		 
		 private final int blocksize = 22; // Rozmiar grafiki w pixelach
		 private final int nrofblocks = 14; // Rozmiar planszy, 14x14 p?l
		 private final int scrsize = nrofblocks * blocksize; // Rozmiar ekranu w pixelach
		 
		 private final long ExplosionActiveTime = 500; // Eksplozja, wyswietlana przez sekunde, !!! w razie potrzeby zmienic !!!
		 
		 private int[] PlayersPos = new int[players.length]; // [] - pozycja, 0 - UP, 1 - DOWN, 2 - LEFT, 3 - RIGHT, DEFAULT = 0;
		 private ArrayList<Explosion> explosions = new ArrayList<>();		 
		 
		public Canvas()
		{
			addKeyListener(new TAdapter());
	        setFocusable(true);
	        setDoubleBuffered(true);
	        loadImages();
	        generatePlayerPosArray();

		}
		
		@Override
	    public void paintComponent(Graphics g)
	    {	               
	        super.paintComponent(g);
	      
	        Graphics2D g2d = (Graphics2D) g;
	        
	        switch(gameState)
	        {
	        case CONFIRM_READY:
	        	showIntroScreen(g2d);
	        	break;
	        case WAIT_FOR_OPPONENT:
	        	showWaitForOpponent(g2d);
	        	break;
	        case START_GAME:
	        	DrawMap(g2d); // RYSUJ MAPE
	        	DrawPlayers(g2d); // RYSUJ GRACZY
	        	DrawBombs(g2d); // RYSUJ BOMBY
	        	DrawExplosions(g2d); // RYSUJ EKSPLOZJE BOMB	        	
	        	break;
	        case DEATH:
	        	showDeath(g2d);
	        	break;
	        case END_GAME:
	        	showEndGame(g2d);
	        	break;
			default:
				break;	   
	        }
	    }
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
		}
		
		 /**
	     * Za?adowanie grafik z gry.     
	     */
		private void loadImages() {        
	        BlockDestructible = new ImageIcon("images/blockdestr.png").getImage();
	        BlockNotDestructible = new ImageIcon("images/blocknotdestr.png").getImage();
	        Bomb = new ImageIcon("images/bomb.png").getImage();
	        BombNumberPU = new ImageIcon("images/bombNumberPU.png").getImage();
	        BombRangePU = new ImageIcon("images/bombRangePU.png").getImage();
	        Extralife = new ImageIcon("images/extraLife.png").getImage();
	        SpeedPU = new ImageIcon("images/SpeedPU.png").getImage(); 
	        Field = new ImageIcon("images/field.png").getImage();
	        ExplosionFire = new ImageIcon("images/ExplosionFire.png").getImage();
	        IndestrubilityPU = new ImageIcon("images/indestrubilityPU.png").getImage();
	        BombermannUP = new ImageIcon("images/bombermannUP.png").getImage();
	        BombermannDown = new ImageIcon("images/bombermannDown.png").getImage();
	        BombermannLeft = new ImageIcon("images/bombermannLeft.png").getImage();
	        BombermannRight = new ImageIcon("images/bombermannRight.png").getImage();
		}
		
		 /**
	     * Inicjowanie tablicy z pozycjami graczy (UP-DOWN-LEFT-RIGHT)
	     * Default: UP    
	     */
		private void generatePlayerPosArray()
		{
			for(int i = 0; i < PlayersPos.length; i++)
			{				
				PlayersPos[i] = 0;
			}
		}
		
		 /**
	     * Rysowanie mapy ze zmiennej Board.  
	     */
		private void DrawMap(Graphics2D g)
	    {
	    	int blockx = 0;
	    	int blocky = 0;
	    	
	    	for(int x = 0; x < 14; x++) {
	    		blockx = x * blocksize;
	    		  for (int y = 0; y < 14; y++){
	    			 blocky = y * blocksize;
	    			
	    			if(board.getField(x, y).getBlock() != null)
	    			{
	    				if(board.getField(x, y).getBlock().isDestructible()){
	    					g.drawImage(BlockDestructible, blockx, blocky, null);
	    				} else g.drawImage(BlockNotDestructible, blockx, blocky, null);    				
	    			}
	    			else if(board.getField(x, y).hasPowerup()) {
	    				 if(board.getField(x, y).getPowerup() instanceof game.bomber.game.BombNumberPU)
	    					 g.drawImage(BombNumberPU, blockx, blocky, null);
	    				 
	    				 if(board.getField(x, y).getPowerup() instanceof BombRangePU)
	    					 g.drawImage(BombRangePU, blockx, blocky, null);
	    				 
	    				 if(board.getField(x, y).getPowerup() instanceof ExtraLifePU)
	    					 g.drawImage(Extralife, blockx, blocky, null);
	    				 
	    				 if(board.getField(x, y).getPowerup() instanceof IndestructibilityPU)
	    					 g.drawImage(IndestrubilityPU, blockx, blocky, null);
	    				 
	    				 if(board.getField(x, y).getPowerup() instanceof game.bomber.game.SpeedPU)
	    					 g.drawImage(SpeedPU, blockx, blocky, null);
	    			}
	    			else if(board.getField(x, y).getBlock() == null && !board.getField(x, y).hasPowerup())
	    	    			g.drawImage(Field, blockx, blocky, null);
	    		}
	    	}
	    }
		
		/**
	     * Rysowanie postaci graczy na mapie   
	     */
		private void DrawPlayers(Graphics2D g)
		{
			for(int i = 0; i < players.length; i++)
			{
				if(players[i].isLive()) {
				switch(PlayersPos[i])
				{
				case 0:
					g.drawImage(BombermannUP, players[i].getPosition().getX() * blocksize, players[i].getPosition().getY() * blocksize, null);
					break;
				case 1:
					g.drawImage(BombermannDown, players[i].getPosition().getX() * blocksize, players[i].getPosition().getY() * blocksize, null);
					break;
				case 2:
					g.drawImage(BombermannLeft, players[i].getPosition().getX() * blocksize, players[i].getPosition().getY() * blocksize, null);
					break;
				case 3:
					g.drawImage(BombermannRight, players[i].getPosition().getX() * blocksize, players[i].getPosition().getY() * blocksize, null);
					break;
				default:
					break;
				}
			  }
			}
		}
		
		/**
	     * Rysowanie aktywnych bomb na mapie   
	     */
		private void DrawBombs(Graphics2D g)
		{
			for(game.bomber.game.Bomb bombtmp: bombs){
				g.drawImage(Bomb, bombtmp.getPosition().getX() * blocksize, bombtmp.getPosition().getY() * blocksize, null);
			}
		}
		
		/**
	     * Rysowanie eksplozji bomb 
	     */
		private void DrawExplosions(Graphics2D g)
		{
			for(int i = 0; i < explosions.size(); i++)
			{
				if(System.currentTimeMillis() > explosions.get(i).getTime() + ExplosionActiveTime) {
					explosions.remove(i);					
					continue;
				}
				else{
					System.out.println("Rysowanie eksplozji");
					// Rysunek wybuchu w miejscu postawienia bomby
					g.drawImage(ExplosionFire, explosions.get(i).getX() * blocksize, explosions.get(i).getY() * blocksize, null);
					
					System.out.println("explosion x: " + explosions.get(i).getX());
					System.out.println("explosion range: " + explosions.get(i).getRange());
					
					// Przejscie po x w prawo
					for (int x = explosions.get(i).getX(); x < explosions.get(i).getX() + explosions.get(i).getRange(); x++)
						g.drawImage(ExplosionFire, x * blocksize, explosions.get(i).getY() * blocksize, null);						
					
					System.out.println("Po pierwszym for");
					
					// Przejscie po x w lewo
					for (int x = explosions.get(i).getX(); x > explosions.get(i).getX() - explosions.get(i).getRange(); x--)
						g.drawImage(ExplosionFire, x * blocksize, explosions.get(i).getY() * blocksize, null);
					
					System.out.println("Po drugim for");
					
					// Przejscie po y w gore
					for (int y = explosions.get(i).getY(); y < explosions.get(i).getY() + explosions.get(i).getRange(); y++)
						g.drawImage(ExplosionFire, explosions.get(i).getX() * blocksize, y * blocksize, null);
					
					System.out.println("Po trzecim for");
					
					// Przejscie po y w dol
					for (int y = explosions.get(i).getY(); y > explosions.get(i).getY() - explosions.get(i).getRange(); y--)
						g.drawImage(ExplosionFire, explosions.get(i).getX() * blocksize, y * blocksize, null);
					
					System.out.println("Po czwartym for");
				}
			}
		}
		
		 /**
	     * Przetworzenie otrzymanego obiektu Event,
	     * Interpretacja i wykonanie odpowiedniego dzia?ania.
	     * Nastepnie wywolanie przerysowania planszy.    
	     */
		public void doEvent(Event event)
		{
			System.out.println("Poczatek doEvent");
			switch (event.getEventType()) 
			{
            case UP:
            	MovePlayerUP(event.getP()); 
            	repaint();
            	break;
            case DOWN:             	
            	MovePlayerDOWN(event.getP()); 
            	repaint();
            	break;
            case LEFT: 
            	MovePlayerLEFT(event.getP()); 
            	repaint();
            	break;
            case RIGTH: 
            	MovePlayerRIGHT(event.getP()); 
            	repaint();
            	break;
            case PUT_BOMB: 
            	addBomb(event);
            	repaint();
            	break;
            case GETS_POWERUP:
            	getsPowerUP(event);
            	repaint();
            	break;
            case EXPLOSION:            	
            	UpdateExplosions(event);
            	repaint();
            	break;
            case DEATH:
            	KillPlayer(event);
            	gameState = GameState.DEATH;
            	repaint();
            	break;
            case END_GAME:
            	gameState = GameState.END_GAME;
            	repaint();
            	break;
            case INIT_GAME:
            	gameState = GameState.WAIT_FOR_OPPONENT;
            	repaint();            	
            	break;
            case START_GAME:
            	 gameState = GameState.START_GAME;
            	 repaint();
            	break;
			}
		}
		
		 /**
	     * Rysowanie ekranu powitalnego, 
	     * Prosba wcisniecia przycisku 's', w celu wyslania do serwera komunikatu 'READY'     
	     */
		private void showIntroScreen(Graphics2D g2d) {

	        g2d.setColor(new Color(0, 32, 48));
	        g2d.fillRect(50, scrsize / 2 - 30, scrsize - 100, 50);
	        g2d.setColor(Color.white);
	        g2d.drawRect(50, scrsize / 2 - 30, scrsize - 100, 50);

	        String s = "Press s to start."; // KOMUNIKAT MOZNA POZNIEJ ZMIENIC
	        Font small = new Font("Helvetica", Font.BOLD, 14);
	        FontMetrics metr = this.getFontMetrics(small);

	        g2d.setColor(Color.white);
	        g2d.setFont(small);
	        g2d.drawString(s, (scrsize - metr.stringWidth(s)) / 2, scrsize / 2);
	    }
		
		 /**
	     * Rysowanie ekranu informacyjnego 
	     * Komunikat o oczekiwaniu na gotowosc przeciwnika - wyslania przez niego do serwera komunikatu 'READY'     
	     */
		private void showWaitForOpponent(Graphics2D g2d) {

			g2d.setColor(new Color(0, 32, 48));
		    g2d.fillRect(50, scrsize / 2 - 30, scrsize - 100, 50);
			g2d.setColor(Color.white);
			g2d.drawRect(50, scrsize / 2 - 30, scrsize - 100, 50);

			String s = "Waiting for your opponent"; // KOMUNIKAT MOZNA POZNIEJ ZMIENIC
			Font small = new Font("Helvetica", Font.BOLD, 14);
			FontMetrics metr = this.getFontMetrics(small);

			g2d.setColor(Color.white);
			g2d.setFont(small);
			g2d.drawString(s, (scrsize - metr.stringWidth(s)) / 2, scrsize / 2);
		}
		
		/**
	     * Rysowanie ekranu informacyjnego 
	     * Komunikat o 'smierci' gracza.    
	     */
		private void showDeath(Graphics2D g2d) {

			g2d.setColor(new Color(0, 32, 48));
		    g2d.fillRect(50, scrsize / 2 - 30, scrsize - 100, 50);
			g2d.setColor(Color.white);
			g2d.drawRect(50, scrsize / 2 - 30, scrsize - 100, 50);

			String s = "Death, "; // KOMUNIKAT MOZNA POZNIEJ ZMIENIC
			Font small = new Font("Helvetica", Font.BOLD, 14);
			FontMetrics metr = this.getFontMetrics(small);

			g2d.setColor(Color.white);
			g2d.setFont(small);
			g2d.drawString(s, (scrsize - metr.stringWidth(s)) / 2, scrsize / 2);
		}
		
		/**
	     * Zmiana pozycji gracza.
	     * Przesuni?cie o pozycj? w g?r?.
	     */
		private void MovePlayerUP(int playerID)
		{
			for (int i = 0; i < players.length; i++)
			{
				if(playerID == players[i].getId())
				{
					players[i].getPosition().setY(players[i].getPosition().getY()-1);
					PlayersPos[i] = 0;
					break;
				}
			}			
		}
		
		/**
	     * Zmiana pozycji gracza.
	     * Przesuni?cie o pozycj? w d?.
	     */
		private void MovePlayerDOWN(int playerID)
		{
			for (int i = 0; i < players.length; i++)
			{
				if(playerID == players[i].getId())
				{					
					players[i].getPosition().setY(players[i].getPosition().getY()+1);
					PlayersPos[i] = 1;
					break;
				}
			}			
		}
		
		/**
	     * Zmiana pozycji gracza.
	     * Przesuni?cie o pozycj? w lewo.
	     */
		private void MovePlayerLEFT(int playerID)
		{
			for (int i = 0; i < players.length; i++)
			{
				if(playerID == players[i].getId())
				{
					players[i].getPosition().setX(players[i].getPosition().getX()-1);
					PlayersPos[i] = 2;
					break;
				}
			}			
		}
		
		/**
	     * Zmiana pozycji gracza.
	     * Przesuni?cie o pozycj? w prawo.
	     */
		private void MovePlayerRIGHT(int playerID)
		{
			for (int i = 0; i < players.length; i++)
			{
				if(playerID == players[i].getId())
				{
					players[i].getPosition().setX(players[i].getPosition().getX()+1);
					PlayersPos[i] = 3;
					break;
				}
			}			
		}
		
		/**
	     * Dodanie nowej bomby do listy.     
	     */
		private void addBomb(Event event)
		{			
			bombs.add(new Bomb(System.currentTimeMillis(), null, new Position(event.getX(), event.getY())));
		}
		
		/**
	     * Usuniecie bomby z listy,
	     * Metoda do wywo?ania w momencie jej wybuchu.    
	     */
		private void removeBomb(Event event)
		{
			for(Bomb bombtmp: bombs){
				if(bombtmp.getPosition().getX() == event.getX())
					if(bombtmp.getPosition().getY() == event.getY())
						{
							bombs.remove(bombtmp);
							break;
						}
				}				
		}
		
		/**
	     * Aktualizacja listy aktywnych explozji
	     * Aktualizacja planszy, block'ow ktore zostana zniszczone.    
	     */
		private void UpdateExplosions(Event event)
		{
			explosions.add(new Explosion(event.getX(), event.getY(), event.getP(), System.currentTimeMillis()));
			
			removeBomb(event);
			
			// Aktualizacja planszy:
			
			
			// Przejscie po x w prawo
			for (int x = event.getX(); x < event.getX() + event.getP(); x++){
				if(x >= 14 || x <= 0)
					continue;				
				
				if(!board.getField(x, event.getY()).isEmpty()){
					board.getField(x, event.getY()).destroy();
					break;
				}
			}
			
			
			// Przejscie po x w lewo
			for (int x = event.getX(); x > event.getX() - event.getP(); x--){
				if(x >= 14 || x <= 0)
					continue;
				
				if(!board.getField(x, event.getY()).isEmpty()){
					board.getField(x, event.getY()).destroy();
					break;
				}
			}
				
			
			// Przejscie po y w gore
			for (int y = event.getY(); y < event.getY() + event.getP(); y++){
				if(y >= 14 || y <= 0)
					continue;				
				
				if(!board.getField(event.getX(), y).isEmpty()){
					board.getField(event.getX(), y).destroy();		
					break;
				}
			}
				
			
			// Przejscie po y w dol
			for (int y = event.getY(); y > event.getY() - event.getP(); y--){
				if(y >= 14 || y <= 0)
					continue;				
				
				if(!board.getField(event.getX(), y).isEmpty()){
					board.getField(event.getX(), y).destroy();
					break;
				}
			} 
				 
		}
		
		/**
	     * Usuniecie PowerUP'a z planszy
	     * Metoda do wywo?ania w momencie pobrania ulepszenia przez gracza.    
	     */
		private void getsPowerUP(Event event)
		{
			board.getField(event.getX(), event.getY()).removePowerup();
		}
		
		/**
	     * Zmiana statusu gracza, live = false;
	     * Metoda do wywo?ania w momencie pobrania eventu typu Death   
	     */
		private void KillPlayer(Event event)
		{
			for(Player playerTmp: players){
				if(playerTmp.getId() == event.getP()) {
					playerTmp.die(System.currentTimeMillis());
					break;
				}
			}				
		}
		
		/**
	     * Rysowanie ekranu informacyjnego 
	     * Komunikat o koncu gry     
	     */
		private void showEndGame(Graphics2D g2d) {

			g2d.setColor(new Color(0, 32, 48));
		    g2d.fillRect(50, scrsize / 2 - 30, scrsize - 100, 50);
			g2d.setColor(Color.white);
			g2d.drawRect(50, scrsize / 2 - 30, scrsize - 100, 50);

			String s = "End Game, "; // KOMUNIKAT MOZNA POZNIEJ ZMIENIC
			Font small = new Font("Helvetica", Font.BOLD, 14);
			FontMetrics metr = this.getFontMetrics(small);

			g2d.setColor(Color.white);
			g2d.setFont(small);
			g2d.drawString(s, (scrsize - metr.stringWidth(s)) / 2, scrsize / 2);
		}
		
		 /**
	     * S?uchacz akcji gracza (klawiszy klawiatury)
	     * Dodaje akcje (obiekt Action) do actionQueue
	     */
		class TAdapter extends KeyAdapter {			
	        @Override
	        public void keyPressed(KeyEvent e) {

	            int key = e.getKeyCode();
	           
	            try{
	              if (gameState == GameState.START_GAME) {            	
	                if (key == KeyEvent.VK_LEFT) {
	                	actionQueue.put(new Action(ACTION.LEFT, myPlayerID));	                	
	                } else if (key == KeyEvent.VK_RIGHT) {
	                	actionQueue.put(new Action(ACTION.RIGTH, myPlayerID));
	                } else if (key == KeyEvent.VK_UP) {
	                	actionQueue.put(new Action(ACTION.UP, myPlayerID));
	                } else if (key == KeyEvent.VK_DOWN){
	                	actionQueue.put(new Action(ACTION.DOWN, myPlayerID));  
	                } else if (key == KeyEvent.VK_SPACE){
	                	actionQueue.put(new Action(ACTION.PUT_BOMB, myPlayerID)); 
	                } repaint();
	            	} else if (gameState == GameState.CONFIRM_READY) {               
	                if (key == KeyEvent.VK_S) {	                	                	
	                	actionQueue.put(new Action(ACTION.READY, myPlayerID));
	                	gameState = GameState.WAIT_FOR_OPPONENT;
	                	repaint(); //
	                }
	            }
	            } catch(InterruptedException event)
	            {
	            	event.printStackTrace();
	            }
	            }
	        }		
	}
	
	 /**
     * Klasa reprezentujaca eksplozje bomb   
     */
	class Explosion {
		private int x;
		private int y;
		private int range;
		private long time;
		
		public Explosion(int Xpos, int Ypos, int BombRange, long StartTime)
		{
			x = Xpos; 
			y = Ypos;
			range = BombRange;
			time = StartTime;
		}
		
		 /**
	     * Pobranie wspolrzednej X wybuchajacej bomby  
	     */
		public int getX()
		{
			return x;
		}
		
		 /**
	     * Pobranie wspolrzednej Y wybuchajacej bomby  
	     */
		public int getY()
		{
			return y;
		}
		
		 /**
	     * Pobranie zasiegu wybuchajacej bomby  
	     */
		public int getRange()
		{
			return range;
		}
		
		 /**
	     * Pobranie czasu rozpoczecia eksplozji wybuchajacej bomby  
	     */
		public long getTime()
		{
			return time;
		}
	}
}