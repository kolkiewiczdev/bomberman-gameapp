package game.bomber.gklient;

import java.util.concurrent.BlockingQueue;

import game.bomber.common.Event;
import game.bomber.communication.game.EventReceiver;

/**
 * @author KOlkiewicz
 */
public class Receiver implements Runnable{
	BlockingQueue<Event> eventQueue;
	EventReceiver eventReceiver;
	
	public Receiver(BlockingQueue<Event> eventQueue, EventReceiver eventReceiver)
	{
		this.eventQueue = eventQueue;
		this.eventReceiver = eventReceiver;
	}
	
	/**
     * pobieranie wydarzenia i przekazanie jej do kolejki
     */
	@Override
	public void run() {		
		boolean end = false;
		while(!end) {
			try {
				//Pobranie event'u
				Event event = eventReceiver.getEvent();
				if (event != null) {
					eventQueue.put(event);
                }
			} catch (InterruptedException e) {				
				e.printStackTrace();
			} 
		}
	}

}