package game.bomber.app;

import javax.swing.*;

import game.bomber.common.Request;
import game.bomber.common.Response;
import game.bomber.communication.global.MClientCommunicator;

import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author KOlkiewicz
 */
public class LANFrame extends JFrame {
	private JLabel title, LANTitle;
	private JButton create, join, back;
	private JPanel createJp, joinJp, backJp;
	private JFrame parent;
	private MClientCommunicator clientCommunicator;
	
	public LANFrame(final JFrame parent, final LanguagePack language, final MClientCommunicator communicator) {
		clientCommunicator = communicator;
		setTitle(language.LANFrameWindowName);
		title = new JLabel(language.LANFrameTitle);
		LANTitle = new JLabel(language.LANFrameLANTitle);
		create = new JButton(language.LANFrameCreate);
		join = new JButton(language.LANFrameJoin);
		back = new JButton(language.LANFrameBack);
		createJp = new JPanel();
		joinJp = new JPanel();
		backJp = new JPanel();
		this.parent = parent;
		
		create.setPreferredSize(new Dimension(140, 40));
		join.setPreferredSize(new Dimension(140, 40));
		back.setPreferredSize(new Dimension(140, 40));
		
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 10, 10, 10);
		add(title, gbc);
		
		gbc.gridy++;
		gbc.insets = new Insets(0, 10, 10, 10);
		add(LANTitle, gbc);
		
		gbc.gridy++;
		gbc.insets = new Insets(20, 10, 0, 10);
		createJp.add(create);
		add(createJp, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0, 10, 5, 10);
		joinJp.add(join);
		add(joinJp, gbc);
		
		gbc.gridy++;
		gbc.weighty=1;
		gbc.anchor = GridBagConstraints.PAGE_END;
		gbc.insets = new Insets(5, 10, 10, 10);
		backJp.add(back);
		add(backJp, gbc);
		
		setSize(400, 550);
		addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
            	if(communicator!=null){
					Map<String, String> params = new HashMap<String, String>();
					params.put("status", "3");
					Response response = clientCommunicator.sendRequest(new Request(
							Request.REQUEST.SET_STATUS, params));
				}
				parent.setVisible(true);
				dispose();
            }
        });
		setVisible(true);
		setLocationRelativeTo(parent);
		
		create.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				new NewServerFrame(getJFrame(), language);
				setVisible(false);
			}
		});
		join.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				new JoinServerFrame(getJFrame(), language);
				setVisible(false);
			}
		});
		back.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(communicator!=null){
					Map<String, String> params = new HashMap<String, String>();
					params.put("status", "3");
					Response response = clientCommunicator.sendRequest(new Request(
							Request.REQUEST.SET_STATUS, params));
				}
				parent.setVisible(true);
				dispose();
			}
		});
	}
	
	public void changeLanguage(LanguagePack language){
		setTitle(language.LANFrameWindowName);
		title.setText(language.LANFrameTitle);
		LANTitle.setText(language.LANFrameLANTitle);
		create.setText(language.LANFrameCreate);
		join.setText(language.LANFrameJoin);
		back.setText(language.LANFrameBack);
	}
	private LANFrame getJFrame(){
		return this;
	}
}
