package game.bomber.app;

import javax.swing.*;

import game.bomber.common.PlayerSimple;
import game.bomber.common.Request;
import game.bomber.common.Response;
import game.bomber.communication.game.GClientToGServer;
import game.bomber.communication.game.GServerToGClients;
import game.bomber.communication.global.MClientCommunicator;
import game.bomber.game.exceptions.NoSuchServerException;
import game.bomber.game.exceptions.WrongUsernameException;

import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author KOlkiewicz
 */
public class NewGameFrame extends JFrame {
	private JLabel title, newGameTitle;
	private JLabel wins, time, player1, player2, player3, player4;
	private JComboBox<String> winsCB, timeCB;
	private String[] winsOption, timeOption;
	private JTextField player1TF, player2TF, player3TF, player4TF;
	private JButton send2, send3, send4;
	private Icon icon2, icon3, icon4;
	private JLabel icon2Label, icon3Label, icon4Label;
	private JButton list, next, back;
	private JPanel listJp, nextJp, backJp, winsCBJp, timeCBJp;
	private MainFrame parent;
	private MClientCommunicator clientCommunicator;
	private GServerToGClients gServerToGClients;
	private GClientToGServer gClientToGServer;
	private boolean []players;

	public NewGameFrame(final MainFrame parent, final LanguagePack language,
			MClientCommunicator communicator) {
		players=new boolean[4];
		players[0]=true;
		clientCommunicator = communicator;
		gServerToGClients = new GServerToGClients(5001, 5002, 5003, 5004, 5005);
		
		PlayerSimple player = new PlayerSimple(
				clientCommunicator.getPlayerId(), parent.getUsername());
		try {
			gClientToGServer = new GClientToGServer(gServerToGClients.getIP(),
					Integer.valueOf(gServerToGClients.getActionPort()),
					Integer.valueOf(gServerToGClients.getEventPort()),
					Integer.valueOf(gServerToGClients.getGameConfigPort()),
					Integer.valueOf(gServerToGClients.getMatchConfigPort()),
					Integer.valueOf(gServerToGClients.getPlayerPort()), player, 1);
		} catch (WrongUsernameException | NoSuchServerException e2) {
			e2.printStackTrace();
		}
		
		setTitle(language.newGameFrameWindowName);
		title = new JLabel(language.newGameFrameTitle);
		newGameTitle = new JLabel(language.newGameFrameNewGameTitle);
		wins = new JLabel(language.newGameFrameWins);
		time = new JLabel(language.newGameFrameTime);
		player1 = new JLabel(language.newGameFramePlayer + " 1");
		player2 = new JLabel(language.newGameFramePlayer + " 2");
		player3 = new JLabel(language.newGameFramePlayer + " 3");
		player4 = new JLabel(language.newGameFramePlayer + " 4");
		winsOption = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		timeOption = new String[11];
		for (int i = 0; i <= 10; i++) {
			timeOption[i] = String.valueOf(i) + language.newGameFrameMinutes;
		}
		winsCB = new JComboBox<String>(winsOption);
		timeCB = new JComboBox<String>(timeOption);
		player1TF = new JTextField(20);
		player2TF = new JTextField(20);
		player3TF = new JTextField(20);
		player4TF = new JTextField(20);
		send2 = new JButton(language.newGameFrameSend);
		send3 = new JButton(language.newGameFrameSend);
		send4 = new JButton(language.newGameFrameSend);
		icon2 = new ImageIcon("accept.png");
		icon3 = new ImageIcon("reject.png");
		icon4 = new ImageIcon("wait.gif");
		icon2Label = new JLabel(icon3);
		icon3Label = new JLabel(icon3);
		icon4Label = new JLabel(icon3);
		list = new JButton(language.newGameFrameList);
		next = new JButton(language.newGameFrameNext);
		back = new JButton(language.newGameFrameBack);
		listJp = new JPanel();
		nextJp = new JPanel();
		backJp = new JPanel();
		winsCBJp = new JPanel();
		timeCBJp = new JPanel();
		this.parent = parent;

		list.setPreferredSize(new Dimension(140, 40));
		next.setPreferredSize(new Dimension(140, 40));
		back.setPreferredSize(new Dimension(140, 40));
		winsCB.setPreferredSize(new Dimension(200,
				timeCB.getPreferredSize().height));
		timeCB.setPreferredSize(new Dimension(200,
				timeCB.getPreferredSize().height));
		player1TF.setPreferredSize(new Dimension(
				player1TF.getPreferredSize().width,
				send2.getPreferredSize().height));
		player2TF.setPreferredSize(new Dimension(
				player1TF.getPreferredSize().width,
				send2.getPreferredSize().height));
		player3TF.setPreferredSize(new Dimension(
				player1TF.getPreferredSize().width,
				send2.getPreferredSize().height));
		player4TF.setPreferredSize(new Dimension(
				player1TF.getPreferredSize().width,
				send2.getPreferredSize().height));
		winsCB.setSelectedIndex(2);
		player1TF.setEditable(false);
		player2TF.setEditable(false);
		player3TF.setEditable(false);
		player4TF.setEditable(false);

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 5;
		gbc.insets = new Insets(20, 10, 5, 10);
		add(title, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0, 10, 20, 10);
		add(newGameTitle, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0, 10, 0, 10);
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.gridwidth = 2;
		add(wins, gbc);

		gbc.gridy++;
		add(time, gbc);

		gbc.gridy--;
		gbc.gridx = 2;
		gbc.gridwidth = 3;
		winsCBJp.add(winsCB);
		add(winsCBJp, gbc);

		gbc.gridy++;
		timeCBJp.add(timeCB);
		add(timeCBJp, gbc);

		gbc.gridy++;
		gbc.gridx = 0;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(20, 10, 0, 10);
		add(player1, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(5, 10, 0, 10);
		add(player2, gbc);

		gbc.gridy++;
		add(player3, gbc);

		gbc.gridy++;
		add(player4, gbc);

		gbc.gridy = 4;
		gbc.gridx++;
		gbc.insets = new Insets(20, 10, 0, 10);
		gbc.gridwidth = 2;
		add(player1TF, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(5, 10, 0, 10);
		add(player2TF, gbc);

		gbc.gridy++;
		add(player3TF, gbc);

		gbc.gridy++;
		add(player4TF, gbc);

		gbc.gridy = 5;
		gbc.gridx = 3;
		gbc.gridwidth = 1;
		add(send2, gbc);

		gbc.gridy++;
		add(send3, gbc);

		gbc.gridy++;
		add(send4, gbc);

		gbc.gridy = 5;
		gbc.gridx++;
		add(icon2Label, gbc);

		gbc.gridy++;
		add(icon3Label, gbc);

		gbc.gridy++;
		add(icon4Label, gbc);

		gbc.gridy++;
		gbc.gridx = 0;
		gbc.gridwidth = 5;
		gbc.weighty = 1;
		gbc.anchor = GridBagConstraints.PAGE_END;
		listJp.add(list);
		add(listJp, gbc);

		gbc.gridy++;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.CENTER;
		nextJp.add(next);
		add(nextJp, gbc);

		gbc.gridy++;
		backJp.add(back);
		add(backJp, gbc);

		setSize(400, 550);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				Map<String, String> params = new HashMap<String, String>();
				params.put("status", "3");
				Response response = clientCommunicator.sendRequest(new Request(
						Request.REQUEST.SET_STATUS, params));
				Map<String, String> params2 = new HashMap<String, String>();
				params2.put("rozmiar", "0");
				Request request = new Request(Request.REQUEST.PLAYERS_LIST,
						params2);
				gServerToGClients.sendRequest(request);
				try {
					gClientToGServer.finalize();
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				gServerToGClients.close();
				parent.setVisible(true);
				dispose();
			}
		});
		setVisible(true);
		setLocationRelativeTo(parent);
		setUsername(parent.getUsername(), 1);

		send2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new InvitingFrame(getJFrame(), 2, language, clientCommunicator);
				setVisible(false);
			}
		});
		send3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new InvitingFrame(getJFrame(), 3, language, clientCommunicator);
				setVisible(false);
			}
		});
		send4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new InvitingFrame(getJFrame(), 4, language, clientCommunicator);
				setVisible(false);
			}
		});
		list.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new PlayersOnlineFrame(getJFrame(), language,
						clientCommunicator);
				setVisible(false);
			}
		});
		next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GameStarterFrame gameStarterFrame = new GameStarterFrame(
						getJFrame(), language, clientCommunicator,
						gClientToGServer, gServerToGClients, parent.getUsername());
				setVisible(false);
			}
		});
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Map<String, String> params = new HashMap<String, String>();
				params.put("status", "3");
				Response response = clientCommunicator.sendRequest(new Request(
						Request.REQUEST.SET_STATUS, params));
				Map<String, String> params2 = new HashMap<String, String>();
				params2.put("rozmiar", "0");
				Request request = new Request(Request.REQUEST.PLAYERS_LIST,
						params2);
				gServerToGClients.sendRequest(request);
				try {
					gClientToGServer.finalize();
				} catch (Throwable e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				gServerToGClients.close();
				parent.setVisible(true);
				dispose();
			}
		});
		gServerToGClients.setNewGameFrame(this);
		next.setEnabled(false);
	}

	void setUsername(String username, int playerNumber) {
		boolean flag = true;
		if (playerNumber != 1) {
			if (username.equals("")) {
				if (player2TF.getText().equals("")
						&& player3TF.getText().equals("")
						&& player4TF.getText().equals("")) {
					winsCB.setEnabled(true);
				}
			} else {
				winsCB.setEnabled(false);
				Map<String, String> params = new HashMap<String, String>();
				params.put("nazwa uzytkownika", username);
				params.put("numer", String.valueOf(playerNumber));
				params.put("do ilu wygranych",
						(String) winsCB.getSelectedItem());
				params.put("ip", gServerToGClients.getIP());
				params.put("action port", gServerToGClients.getActionPort());
				params.put("event port", gServerToGClients.getEventPort());
				params.put("game config port",
						gServerToGClients.getGameConfigPort());
				params.put("match config port",
						gServerToGClients.getMatchConfigPort());
				params.put("player port", gServerToGClients.getPlayerPort());
				Response response = clientCommunicator.sendRequest(new Request(
						Request.REQUEST.INVITE, params));
				if (response.getParam("potwierdzenie").equals("true")) {
					flag = true;
				} else {
					flag = false;
				}
			}
		}
		switch (playerNumber) {
		case 1:
			player1TF.setText(username);
			winsCB.setEnabled(true);
			break;
		case 2:
			player2TF.setText(username);
			if (username.equals("")) {
				icon2Label.setIcon(icon3);
				players[1]=false;
			} else {
				send2.setEnabled(false);
				if (flag) {
					icon2Label.setIcon(icon4);
				} else {
					icon2Label.setIcon(icon3);
				}
			}
			break;
		case 3:
			player3TF.setText(username);
			if (username.equals("")) {
				icon3Label.setIcon(icon3);
				players[2]=false;
			} else {
				send3.setEnabled(false);
				if (flag) {
					icon3Label.setIcon(icon4);
				} else {
					icon3Label.setIcon(icon3);
				}
			}
			break;
		case 4:
			player4TF.setText(username);
			if (username.equals("")) {
				icon4Label.setIcon(icon3);
				players[3]=false;
			} else {
				send4.setEnabled(false);
				if (flag) {
					icon4Label.setIcon(icon4);
				} else {
					icon4Label.setIcon(icon3);
				}
			}
			break;
		}
		int num=0;
		for(int i=1; i<4; i++){
			if(players[i]==true){
				num++;
			}
		}
		if(num>0){
			next.setEnabled(true);
		} else {
			next.setEnabled(false);
		}
	}

	public void changeLanguage(LanguagePack language) {
		setTitle(language.newGameFrameWindowName);
		title.setText(language.newGameFrameTitle);
		newGameTitle.setText(language.newGameFrameNewGameTitle);
		wins.setText(language.newGameFrameWins);
		time.setText(language.newGameFrameTime);
		player1.setText(language.newGameFramePlayer + " 1");
		player2.setText(language.newGameFramePlayer + " 2");
		player3.setText(language.newGameFramePlayer + " 3");
		player4.setText(language.newGameFramePlayer + " 4");
		send2.setText(language.newGameFrameSend);
		send3.setText(language.newGameFrameSend);
		send4.setText(language.newGameFrameSend);
		list.setText(language.newGameFrameList);
		next.setText(language.newGameFrameNext);
		back.setText(language.newGameFrameBack);
	}

	private NewGameFrame getJFrame() {
		return this;
	}

	public void updateStatus(String username, boolean status) {
		// String name = params.get("nazwa uzytkownika");
		if (player2TF.getText().equals(username)) {
			if (/* params.get("potwierdzenie").equals("true") */status) {
				icon2Label.setIcon(icon2);
				send2.setEnabled(false);
				players[1]=true;
			} else {
				icon2Label.setIcon(icon3);
				player2TF.setText("");
				send2.setEnabled(true);
				players[1]=false;
			}
		}
		if (player3TF.getText().equals(username)) {
			if (/* params.get("potwierdzenie").equals("true") */status) {
				icon3Label.setIcon(icon2);
				send3.setEnabled(false);
				players[2]=true;
			} else {
				icon3Label.setIcon(icon3);
				player3TF.setText("");
				send3.setEnabled(true);
				players[2]=false;
			}
		}
		if (player4TF.getText().equals(username)) {
			if (/* params.get("potwierdzenie").equals("true") */status) {
				icon4Label.setIcon(icon2);
				send4.setEnabled(false);
				players[3]=true;
			} else {
				icon4Label.setIcon(icon3);
				player4TF.setText("");
				send4.setEnabled(true);
				players[3]=false;
			}
		}
		int num=0;
		for(int i=1; i<4; i++){
			if(players[i]==true){
				num++;
			}
		}
		if(num>0){
			next.setEnabled(true);
		} else {
			next.setEnabled(false);
		}
	}
}
