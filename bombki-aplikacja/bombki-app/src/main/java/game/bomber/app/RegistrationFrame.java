package game.bomber.app;

import javax.swing.*;

import game.bomber.common.Request;
import game.bomber.common.Response;
import game.bomber.communication.global.MClientCommunicator;

import java.awt.*;
import java.awt.event.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author KOlkiewicz
 */
public class RegistrationFrame extends JFrame {
	private JLabel title, registration;
	private JLabel username, password1, password2, question, answer;
	private JTextField usernameTF, questionTF, answerTF;
	JPasswordField password1TF, password2TF;
	private JButton send, back;
	private JPanel sendJp, backJp;
	private JFrame parent;
	private MClientCommunicator clientCommunicator;
	public static final int INCORRECT_USERNAME = 0;
	public static final int UNAVAILABLE_USERNAME = 1;
	public static final int INCORRECT_PASSWORD1 = 2;
	public static final int INCORRECT_PASSWORD2 = 3;
	public static final int INCORRECT_QUESTION = 4;
	public static final int INCORRECT_ANSWER = 5;
	public static final int DISCONNECTED = 6;

	public RegistrationFrame(final JFrame parent, final LanguagePack language,
			MClientCommunicator communicator) {
		clientCommunicator = communicator;
		setTitle(language.registrationFrameWindowName);
		title = new JLabel(language.registrationFrameTitle);
		registration = new JLabel(language.registrationFrameRegistration);
		username = new JLabel(language.registrationFrameUsername);
		password1 = new JLabel(language.registrationFramePassword1);
		password2 = new JLabel(language.registrationFramePassword2);
		question = new JLabel(language.registrationFrameQuestion);
		answer = new JLabel(language.registrationFrameAnswer);
		send = new JButton(language.registrationFrameSend);
		back = new JButton(language.registrationFrameBack);
		usernameTF = new JTextField(25);
		password1TF = new JPasswordField(25);
		password2TF = new JPasswordField(25);
		questionTF = new JTextField(25);
		answerTF = new JTextField(25);
		sendJp = new JPanel();
		backJp = new JPanel();
		this.parent = parent;

		send.setPreferredSize(new Dimension(140, 40));
		back.setPreferredSize(new Dimension(140, 40));

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		gbc.insets = new Insets(10, 10, 10, 10);
		add(title, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0, 10, 20, 10);
		add(registration, gbc);

		gbc.gridwidth = 1;
		gbc.gridy++;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		gbc.insets = new Insets(10, 10, 5, 0);
		add(username, gbc);

		gbc.gridy++;
		add(password1, gbc);

		gbc.gridy++;
		add(password2, gbc);

		gbc.gridy++;
		add(question, gbc);

		gbc.gridy++;
		add(answer, gbc);

		gbc.gridx++;
		gbc.gridy = 2;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(10, 10, 5, 30);
		add(usernameTF, gbc);

		gbc.gridy++;
		add(password1TF, gbc);

		gbc.gridy++;
		add(password2TF, gbc);

		gbc.gridy++;
		add(questionTF, gbc);

		gbc.gridy++;
		add(answerTF, gbc);

		gbc.gridy++;
		gbc.gridx = 0;
		gbc.gridwidth = 3;
		gbc.insets = new Insets(20, 10, 10, 10);
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.weighty = 1;
		sendJp.add(send);
		add(sendJp, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(10, 10, 10, 10);
		gbc.anchor = GridBagConstraints.PAGE_END;
		backJp.add(back);
		add(backJp, gbc);

		setSize(400, 550);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				parent.setVisible(true);
				dispose();
			}
		});
		setVisible(true);
		setLocationRelativeTo(parent);

		send.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Matcher m = Pattern.compile("\\W")
						.matcher(usernameTF.getText());
				if (usernameTF.getText().length() < 6) {
					showError(language, INCORRECT_USERNAME);
				} else {
					if (m.find()) {
						showError(language, INCORRECT_USERNAME);
					} else {
						if (password1TF.getPassword().length < 8) {
							showError(language, INCORRECT_PASSWORD1);
						} else {
							if (!String.valueOf(password2TF.getPassword())
									.equals(String.valueOf(password1TF
											.getPassword()))) {
								showError(language, INCORRECT_PASSWORD2);
							} else {
								if (questionTF.getText().equals("")) {
									showError(language, INCORRECT_QUESTION);
								} else {
									if (answerTF.getText().equals("")) {
										showError(language, INCORRECT_ANSWER);
									} else {
										if (!clientCommunicator.isConnected()) {
											showError(language, DISCONNECTED);
										} else {
											try {
												Map<String, String> params = new HashMap<String, String>();
												params.put("nazwa uzytkownika",
														usernameTF.getText());
												MessageDigest md = MessageDigest
														.getInstance("MD5");
												md.update(String.valueOf(
														password1TF
																.getPassword())
														.getBytes("UTF-8"));
												byte[] digest = md.digest();
												StringBuffer sb = new StringBuffer();
												for (byte b : digest) {
													sb.append(String.format(
															"%02x", b & 0xff));
												}
												params.put("haslo",
														sb.toString());
												params.put("pytanie",
														questionTF.getText());
												md.reset();
												md.update(answerTF.getText()
														.getBytes("UTF-8"));
												byte[] digest2 = md.digest();
												StringBuffer sb2 = new StringBuffer();
												for (byte b : digest2) {
													sb2.append(String.format(
															"%02x", b & 0xff));
												}
												params.put("odpowiedz",
														sb2.toString());
												Response response = clientCommunicator
														.sendRequest(new Request(
																Request.REQUEST.REGISTRATION,
																params));
												if (response.getParam(
														"potwierdzenie")
														.equals("true")) {
													showSuccess(language);
													parent.setVisible(true);
													dispose();
												} else {
													showError(language,
															UNAVAILABLE_USERNAME);
												}
											} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
												e1.printStackTrace();
											}
										}

									}
								}
							}
						}
					}
				}
			}
		});
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.setVisible(true);
				dispose();
			}
		});
	}

	public void showSuccess(LanguagePack language) {
		Object options[] = { language.registrationFrameShowSuccessNext };
		JOptionPane.showOptionDialog(this,
				language.registrationFrameShowSuccessMessage,
				language.registrationFrameShowSuccessWindowName,
				JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
				null, options, options[0]);
	}

	public void showError(LanguagePack language, int errorCode) {
		String message = "";
		switch (errorCode) {
		case INCORRECT_USERNAME:
			message = language.registrationFrameShowErrorMessage0;
			break;
		case UNAVAILABLE_USERNAME:
			message = language.registrationFrameShowErrorMessage1;
			break;
		case INCORRECT_PASSWORD1:
			message = language.registrationFrameShowErrorMessage2;
			break;
		case INCORRECT_PASSWORD2:
			message = language.registrationFrameShowErrorMessage3;
			break;
		case INCORRECT_QUESTION:
			message = language.registrationFrameShowErrorMessage4;
			break;
		case INCORRECT_ANSWER:
			message = language.registrationFrameShowErrorMessage5;
			break;
		case DISCONNECTED:
			message = language.registrationFrameShowErrorMessage6;
			break;
		}
		Object options[] = { language.registrationFrameShowErrorBack };
		JOptionPane.showOptionDialog(this, message,
				language.registrationFrameShowErrorWindowName,
				JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null,
				options, options[0]);

	}

	public void changeLanguage(LanguagePack language) {
		setTitle(language.registrationFrameWindowName);
		title.setText(language.registrationFrameTitle);
		registration.setText(language.registrationFrameRegistration);
		username.setText(language.registrationFrameUsername);
		password1.setText(language.registrationFramePassword1);
		password2.setText(language.registrationFramePassword2);
		question.setText(language.registrationFrameQuestion);
		answer.setText(language.registrationFrameAnswer);
		send.setText(language.registrationFrameSend);
		back.setText(language.registrationFrameBack);
	}

}
