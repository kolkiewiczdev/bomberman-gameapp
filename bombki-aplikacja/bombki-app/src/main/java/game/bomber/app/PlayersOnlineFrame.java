package game.bomber.app;

import javax.swing.*;

import game.bomber.common.Request;
import game.bomber.common.Response;
import game.bomber.communication.global.MClientCommunicator;

import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author KOlkiewicz
 */
public class PlayersOnlineFrame extends JFrame {
	private JLabel title, playersTitle;
	private JScrollPane scroll;
	private JLabel[] columnNames;
	private JButton back;
	private LinkedList<UserSample> users;
	private JPanel panel, backJp, main;
	private JFrame parent;
	private MClientCommunicator clientCommunicator;

	public PlayersOnlineFrame(final JFrame parent, LanguagePack language, MClientCommunicator communicator) {
		clientCommunicator = communicator;
		setTitle(language.playersOnlineFrameWindowName);
		title = new JLabel(language.playersOnlineFrameTitle);
		playersTitle = new JLabel(language.playersOnlineFramePlayersTitle);
		columnNames = new JLabel[] {
				new JLabel(language.playersOnlineFrameColumnNames1),
				new JLabel(language.playersOnlineFrameColumnNames2),
				new JLabel(language.playersOnlineFrameColumnNames3) };
		users = new LinkedList<UserSample>();
		back = new JButton(language.playersOnlineFrameBack);
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		backJp = new JPanel();
		scroll = new JScrollPane(panel);
		this.parent = parent;

		back.setPreferredSize(new Dimension(140, 40));

		main = new JPanel();
		main.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 4;
		gbc.insets = new Insets(10, 10, 0, 10);
		gbc.weightx = 1;
		main.add(title, gbc);

		gbc.gridy++;
		main.add(playersTitle, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(10, 0, 0, 0);
		gbc.gridwidth = 1;
		main.add(columnNames[0], gbc);

		gbc.gridx++;
		main.add(columnNames[1], gbc);

		gbc.gridx++;
		main.add(columnNames[2], gbc);

		gbc.gridx++;
		main.add(new JLabel("          "), gbc);

		backJp.add(back);

		setLayout(new BorderLayout());
		add(main, BorderLayout.NORTH);
		add(backJp, BorderLayout.SOUTH);
		add(scroll);

		setSize(400, 550);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				parent.setVisible(true);
				dispose();
			}
		});
		setVisible(true);
		setLocationRelativeTo(parent);
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.setVisible(true);
				dispose();

			}
		});
		Response response = clientCommunicator
				.sendRequest(new Request(Request.REQUEST.PLAYERS_LIST,
						null));
		int tmp=0;
		for(Entry<String, String> entry : response.getParams().entrySet()){
			String name = entry.getValue().substring(0, entry.getValue().length()-1);
			int currentStatus = Integer.valueOf(entry.getValue().substring(entry.getValue().length()-1));
			addUser(language, name, currentStatus);
		}

	}

	public void addUser(LanguagePack language, String username, int status) {
		UserSample tmp;
		boolean flag = false;
		if (users.size() == 0) {
			users.add(new UserSample(language, username, status));
			refresh();
		} else {
			for (int i = 0; i < users.size(); i++) {
				tmp = users.get(i);
				if (tmp.getUsername().compareToIgnoreCase(username) > 0
						&& flag == false) {
					users.add(i, new UserSample(language, username, status));
					flag = true;
					refresh();
					break;
				}

				if (tmp.getUsername().compareTo(username) == 0 && flag == false) {
					flag = true;
					break;
				}

			}
			if (flag == false) {
				users.add(users.size(), new UserSample(language, username, status));
				refresh();
			}
		}
	}

	public void refresh() {
		panel.removeAll();
		if (users.size() > 0) {
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1;
			for (int i = 0; i < users.size() - 1; i++) {

				panel.add(users.get(i), gbc);
				gbc.gridy++;
			}
			gbc.anchor = GridBagConstraints.PAGE_START;
			gbc.weighty = 1;
			panel.add(users.get(users.size() - 1), gbc);
		}
		panel.revalidate();
	}

	public void changeLanguage(LanguagePack language) {
		setTitle(language.playersOnlineFrameWindowName);
		title.setText(language.playersOnlineFrameTitle);
		playersTitle.setText(language.playersOnlineFramePlayersTitle);
		columnNames[0].setText(language.playersOnlineFrameColumnNames1);
		columnNames[1].setText(language.playersOnlineFrameColumnNames2);
		columnNames[2].setText(language.playersOnlineFrameColumnNames3);
		back.setText(language.playersOnlineFrameBack);
		for (int i = 0; i < users.size(); i++) {
			users.get(i).changeLanguage(language);
		}
	}
	public PlayersOnlineFrame getJFrame() {
		return this;
	}
	class UserSample extends JPanel {
		private JLabel nick, status;
		private JButton stats;
		private String waitingStatus, playingStatus, freeStatus, meStatus;
		public static final int WAIT = 1;
		public static final int PLAY = 2;
		public static final int FREE = 3;
		public static final int ME = 4;
		private int myStatus;
		private JPanel nickJp, statusJp, statsJp;

		public UserSample(final LanguagePack language, String username, int currentStatus) {
			waitingStatus = language.playersOnlineFrameWaitingStatus;
			playingStatus = language.playersOnlineFramePlayingStatus;
			freeStatus = language.playersOnlineFrameFreeStatus;
			meStatus = language.playersOnlineFrameMeStatus;
			nick = new JLabel(username);
			status = new JLabel(freeStatus);
			myStatus = 3;
			stats = new JButton(language.playersOnlineFrameStats);
			nickJp = new JPanel();
			statusJp = new JPanel();
			statsJp = new JPanel();

			// setBorder(new LineBorder(Color.BLUE));

			this.setLayout(new GridLayout(1, 4));
			nickJp.setLayout(new GridBagLayout());
			nickJp.add(nick);

			add(nickJp);

			statusJp.setLayout(new GridBagLayout());
			statusJp.add(status);
			add(statusJp);
			statsJp.add(stats);
			add(statsJp);
			add(new JPanel());
			stats.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Map<String, String> params = new HashMap<String, String>();
					params.put("nazwa uzytkownika", nick.getText());
					Response response = clientCommunicator
							.sendRequest(new Request(Request.REQUEST.STATS,
									params));
					new StatisticsFrame(getJFrame(), language, nick.getText(), response.getParams());
				}
			});
			setStatus(currentStatus);
		}

		public String getUsername() {
			return nick.getText();
		}

		public void setStatus(int arg) {
			if (arg == UserSample.WAIT) {
				status.setText(this.waitingStatus);
				myStatus = 1;
			}
			if (arg == UserSample.PLAY) {
				status.setText(this.playingStatus);
				myStatus = 2;
			}
			if (arg == UserSample.FREE) {
				status.setText(this.freeStatus);
				myStatus = 3;
			}
			if (arg == UserSample.ME) {
				status.setText(this.meStatus);
				myStatus = 4;
			}
		}

		public void changeLanguage(LanguagePack language) {
			waitingStatus = language.playersOnlineFrameWaitingStatus;
			playingStatus = language.playersOnlineFramePlayingStatus;
			freeStatus = language.playersOnlineFrameFreeStatus;
			stats.setText(language.playersOnlineFrameStats);
			setStatus(myStatus);
		}
	}

}
