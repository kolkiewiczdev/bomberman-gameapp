package game.bomber.app;

/**
 * @author KOlkiewicz
 */
public class UserBeforeNewRound {
	public static final int WAITING = 1;
	public static final int READY = 2;
	public static final int ERROR = 3;
	private String username;
	private int status;
	private int wins;
	private boolean Owner;

	public UserBeforeNewRound(String nick, int status, int wins, boolean isOwner) {
		this.username = nick;
		this.status = status;
		this.wins = wins;
		this.Owner = isOwner;
	}

	public void setStatus(int newStatus) {
		status = newStatus;
	}

	public int getStatus() {
		return status;
	}

	public String getUsername() {
		return username;
	}

	public int getWins() {
		return wins;
	}
	public boolean isOwner(){
		return Owner;
		
	}
	

}
