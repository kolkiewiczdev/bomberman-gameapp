package game.bomber.app;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author KOlkiewicz
 */
public abstract class LanguagePack {
	public static String languageName;
	public static String openingFrameRegistration, openingFrameLogin,
			openingFramePlay, openingFrameOptions, openingFrameExit,
			openingFrameTitle, openingFrameWindowName;
	public static String mainFrameAccount, mainFrameLogout, mainFrameTitle,
			mainFramePlayNow, mainFrameInvite, mainFramePlayers,
			mainFramePlayLan, mainFrameOptions, mainFrameExit,
			mainFrameWindowName, mainFrameSearchPlayerDialogSearchPlayers,
			mainFrameSearchPlayerDialogWait, mainFrameSearchPlayerDialogExit,
			mainFrameSearchPlayerDialogWindowName,
			mainFrameShowInvitationReject, mainFrameShowInvitationAccept,
			mainFrameShowInvitationMessage1, mainFrameShowInvitationMessage2,
			mainFrameShowInvitationWindowName;
	public static String registrationFrameTitle, registrationFrameRegistration,
			registrationFrameUsername, registrationFramePassword1,
			registrationFramePassword2, registrationFrameQuestion,
			registrationFrameAnswer, registrationFrameSend,
			registrationFrameBack, registrationFrameWindowName,
			registrationFrameShowSuccessNext,
			registrationFrameShowSuccessMessage,
			registrationFrameShowSuccessWindowName,
			registrationFrameShowErrorBack, registrationFrameShowErrorMessage0,
			registrationFrameShowErrorMessage1,
			registrationFrameShowErrorMessage2,
			registrationFrameShowErrorMessage3,
			registrationFrameShowErrorMessage4,
			registrationFrameShowErrorMessage5,
			registrationFrameShowErrorMessage6,
			registrationFrameShowErrorWindowName;
	public static String loginFrameTitle, loginFrameLoginTitle,
			loginFrameUsername, loginFramePassword, loginFrameLogin,
			loginFrameRecover, loginFrameBack, loginFrameWindowName,
			loginFrameShowErrorMessage0, loginFrameShowErrorMessage1,
			loginFrameShowErrorBack, loginFrameShowErrorWindowName,
			loginFrameRecoveryDialogTitle, loginFrameRecoveryDialogUsername,
			loginFrameRecoveryDialogNext, loginFrameRecoveryDialogWindowName,
			loginFrameRecoveryDialogShowSuccessTitle,
			loginFrameRecoveryDialogShowSuccessMessage,
			LoginFrameRecoveryDialogShowQuestionWindowName,
			LoginFrameRecoveryDialogShowErrorMessage,
			LoginFrameRecoveryDialogShowErrorWindowName,
			LoginFrameRecoveryDialogShowWrongAnswerMessage,
			LoginFrameRecoveryDialogShowWrongAnswerWindowName;
	public static String optionFrameTitle, optionFrameOptionTitle,
			optionFrameLanguageLabel, optionFrameSave, optionFrameBack,
			optionFrameWindowName;
	public static String LANFrameTitle, LANFrameLANTitle, LANFrameCreate,
			LANFrameJoin, LANFrameBack, LANFrameWindowName;
	public static String newServerFrameTitle, newServerFrameNewServerTitle,
			newServerFrameWins, newServerFrameTime, newServerFrameNick,
			newServerFrameNext, newServerFrameBack, newGameFrameMinutes,
			newServerFrameWindowName;
	public static String joinServerFrameTitle, joinServerFrameJoinTitle,
			joinServerFrameIp, joinServerFramePort, joinServerFrameNick,
			joinServerFrameConnect, joinServerFrameBack,
			joinServerFrameWindowName, joinServerFrameShowErrorMessage1,
			joinServerFrameShowErrorMessage2, joinServerFrameShowErrorBack,
			joinServerFrameShowErrorWindowName;
	public static String newGameFrameTitle, newGameFrameNewGameTitle,
			newGameFrameWins, newGameFrameTime, newGameFramePlayer,
			newGameFrameSend, newGameFrameList, newGameFrameNext,
			newGameFrameBack, newGameFrameWindowName;
	public static String invitingFrameTitle, invitingFrameInviteTitle,
			invitingFrameWaitingStatus, invitingFramePlayingStatus,
			invitingFrameFreeStatus, invitingFrameMeStatus,
			invitingFrameColumnNames1, invitingFrameColumnNames2,
			invitingFrameColumnNames3, invitingFrameColumnNames4,
			invitingFrameStats, invitingFrameInvite, invitingFrameBack,
			invitingFrameWindowName;
	public static String playersOnlineFrameTitle,
			playersOnlineFramePlayersTitle, playersOnlineFrameWaitingStatus,
			playersOnlineFramePlayingStatus, playersOnlineFrameFreeStatus,
			playersOnlineFrameMeStatus, playersOnlineFrameColumnNames1,
			playersOnlineFrameColumnNames2, playersOnlineFrameColumnNames3,
			playersOnlineFrameStats, playersOnlineFrameBack,
			playersOnlineFrameWindowName;
	public static String statisticsFrameTitle, statisticsFrameStatsTitle,
			statisticsFramePlayer, statisticsFramePosition1,
			statisticsFramePosition2, statisticsFramePosition3,
			statisticsFramePosition4, statisticsFrameWins,
			statisticsFrameLoses, statisticsFramePoints, statisticsFrameRank,
			statisticsFrameBack, statisticsFrameWindowName;
	public static String gameStarterFrameTitle, gameStarterFrameWaitingTitle,
			gameStarterFrameReady, gameStarterFrameNotReady,
			gameStarterFrameExit, gameStarterFrameWindowName;
	public static String roundStarterFrameTitle, roundStarterFrameWaitingTitle,
			roundStarterFrameWins, roundStarterFrameReady,
			roundStarterFrameNotReady, roundStarterFrameExit,
			roundStarterFrameWindowName;
}
