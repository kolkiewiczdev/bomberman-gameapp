package game.bomber.app;

import javax.swing.*;

import game.bomber.common.PlayerSimple;
import game.bomber.common.Request;
import game.bomber.common.Response;
import game.bomber.communication.game.GClientToGServer;
import game.bomber.communication.global.MClientCommunicator;
import game.bomber.game.exceptions.NoSuchServerException;
import game.bomber.game.exceptions.WrongUsernameException;

import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author KOlkiewicz
 */
public class MainFrame extends JFrame implements LanguageChanger {
	private JButton account, logout;
	private JLabel username, title;
	private JButton playNow, invite, players, playLan, options, exit;
	private JPanel jp, playNowJp, inviteJp, playersJp, playLanJp, optionsJp,
			exitJp;
	private SearchPlayersDialog dialog;
	private MClientCommunicator clientCommunicator;
	private LanguagePack lang;

	public MainFrame(final LanguagePack language, final MClientCommunicator communicator) {
		lang = language;
		clientCommunicator = communicator;

		setTitle(language.mainFrameWindowName);
		account = new JButton(language.mainFrameAccount);
		logout = new JButton(language.mainFrameLogout);
		username = new JLabel("U�ytkownik");
		title = new JLabel(language.mainFrameTitle);
		playNow = new JButton(language.mainFramePlayNow);
		invite = new JButton(language.mainFrameInvite);
		players = new JButton(language.mainFramePlayers);
		playLan = new JButton(language.mainFramePlayLan);
		options = new JButton(language.mainFrameOptions);
		exit = new JButton(language.mainFrameExit);
		jp = new JPanel();
		playNowJp = new JPanel();
		inviteJp = new JPanel();
		playersJp = new JPanel();
		playLanJp = new JPanel();
		optionsJp = new JPanel();
		exitJp = new JPanel();
		dialog = new SearchPlayersDialog(this, language);
		playNow.setPreferredSize(new Dimension(140, 40));
		invite.setPreferredSize(new Dimension(140, 40));
		players.setPreferredSize(new Dimension(140, 40));
		playLan.setPreferredSize(new Dimension(140, 40));
		options.setPreferredSize(new Dimension(140, 40));
		exit.setPreferredSize(new Dimension(140, 40));

		jp.setLayout(new FlowLayout());
		jp.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		jp.add(logout);
		jp.add(account);

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(0, 390 - jp.getPreferredSize().width, 10, 5);
		add(jp, gbc);

		gbc.gridx = 0;
		gbc.gridy++;
		gbc.insets = new Insets(0, 0, 10, 0);
		gbc.anchor = GridBagConstraints.CENTER;
		add(username, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0, 0, 15, 0);
		add(title, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0, 0, 5, 0);
		gbc.weighty = 0.5;
		gbc.fill = GridBagConstraints.BOTH;
		playNowJp.add(playNow);
		add(playNowJp, gbc);

		gbc.gridy++;
		inviteJp.add(invite);
		add(inviteJp, gbc);

		gbc.gridy++;
		playersJp.add(players);
		add(playersJp, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(100, 0, 5, 0);
		playLanJp.add(playLan);
		add(playLanJp, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0, 0, 5, 0);
		optionsJp.add(options);
		add(optionsJp, gbc);

		gbc.gridy++;
		exitJp.add(exit);
		add(exitJp, gbc);

		setSize(400, 550);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (clientCommunicator.isConnected()) {
					Response response = clientCommunicator
							.sendRequest(new Request(
									Request.REQUEST.DISCONNECT, null));
					clientCommunicator.close();
				}
				dispose();
			}
		});
		setVisible(true);
		setLocationRelativeTo(null);

		account.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// new NewServerFrame(getJFrame(), language);
				// setVisible(false);
			}
		});
		logout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clientCommunicator.reconnect();
				OpeningFrame openingFrame = new OpeningFrame(language, clientCommunicator);
				openingFrame.setLocationRelativeTo(getJFrame());
				dispose();
			}
		});
		playNow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Map<String, String> params = new HashMap<String, String>();
				params.put("status", "2");
				Response response = clientCommunicator.sendRequest(new Request(
						Request.REQUEST.SET_STATUS, params));
				NewGameFrame newGameFrame = new NewGameFrame(getJFrame(),
						language, clientCommunicator);
				clientCommunicator.setNewGameFrame(newGameFrame);
				setVisible(false);
			}
		});
		invite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// new NewServerFrame(getJFrame(), language);
				// setVisible(false);
			}
		});
		players.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new PlayersOnlineFrame(getJFrame(), language,
						clientCommunicator);
				setVisible(false);
			}
		});
		playLan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Map<String, String> params = new HashMap<String, String>();
				params.put("status", "2");
				Response response = clientCommunicator.sendRequest(new Request(
						Request.REQUEST.SET_STATUS, params));
				new LANFrame(getJFrame(), language, communicator);
				setVisible(false);
			}
		});
		options.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new OptionFrame(getJFrame(), language);
				setVisible(false);
				// dispose();
			}
		});
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (clientCommunicator.isConnected()) {
					Response response = clientCommunicator
							.sendRequest(new Request(
									Request.REQUEST.DISCONNECT, null));
					clientCommunicator.close();
				}
				dispose();
			}
		});

	}

	public void setUsername(String username) {
		this.username.setText(username);
	}

	public String getUsername() {
		return username.getText();
	}

	public MainFrame changeLanguage(LanguagePack language) {
		/*
		 * setTitle(language.mainFrameWindowName);
		 * account.setText(language.mainFrameAccount);
		 * logout.setText(language.mainFrameLogout);
		 * username.setText("U�ytkownik");
		 * title.setText(language.mainFrameTitle);
		 * playNow.setText(language.mainFramePlayNow);
		 * invite.setText(language.mainFrameInvite);
		 * players.setText(language.mainFramePlayers);
		 * playLan.setText(language.mainFramePlayLan);
		 * options.setText(language.mainFrameOptions);
		 * exit.setText(language.mainFrameExit);
		 * dialog.exit.setText(language.mainFrameSearchPlayerDialogExit);
		 * dialog.wait.setText(language.mainFrameSearchPlayerDialogWait);
		 * dialog.searchPlayers
		 * .setText(language.mainFrameSearchPlayerDialogSearchPlayers);
		 * dialog.setTitle(language.mainFrameSearchPlayerDialogWindowName);
		 * dialog.pack();
		 */
		MainFrame mainFrame = new MainFrame(language, clientCommunicator);
		mainFrame.setVisible(false);
		dispose();
		mainFrame.setUsername(username.getText());
		return mainFrame;
	}

	public void showInvitation(LanguagePack language, String hostUsername,
			String wins, Map<String, String> map) {
		Map<String, String> params1 = new HashMap<String, String>();
		params1.put("status", "1");
		Response response1 = clientCommunicator.sendRequest(new Request(
				Request.REQUEST.SET_STATUS, params1));
		Object options[] = { language.mainFrameShowInvitationReject,
				language.mainFrameShowInvitationAccept };
		int i = JOptionPane.showOptionDialog(this,
				language.mainFrameShowInvitationMessage1 + wins
						+ language.mainFrameShowInvitationMessage2
						+ hostUsername,
				language.mainFrameShowInvitationWindowName,
				JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
				options, options[1]);
		Map<String, String> params = new HashMap<String, String>();
		if (i == 1) {
			params.put("potwierdzenie", "true");
			PlayerSimple player = new PlayerSimple(
					clientCommunicator.getPlayerId(), username.getText());
			GClientToGServer gClientToGServer = null;
			try {
				gClientToGServer = new GClientToGServer(
						map.get("host"), Integer.valueOf(map.get("action port")),
						Integer.valueOf(map.get("event port")), Integer.valueOf(map
								.get("game config port")), Integer.valueOf(map
								.get("match config port")), Integer.valueOf(map
								.get("player port")), player, Integer.valueOf(map
								.get("numer")));
			} catch (WrongUsernameException | NoSuchServerException e) {
				e.printStackTrace();
			}
			GameStarterFrame gameStarterFrame = new GameStarterFrame(this,
					language, clientCommunicator, gClientToGServer, null, username.getText());
			setVisible(false);

		} else {
			params.put("potwierdzenie", "false");
			Map<String, String> params2 = new HashMap<String, String>();
			params2.put("status", "3");
			Response response2 = clientCommunicator.sendRequest(new Request(
					Request.REQUEST.SET_STATUS, params2));
		}
		params.put("nazwa uzytkownika", hostUsername);
		Response response = clientCommunicator.sendRequest(new Request(
				Request.REQUEST.ANSWER_TO_INVITE, params));
	}

	public void searchPlayers() {
		dialog.setVisible(true);
	}

	class SearchPlayersDialog extends JDialog {
		private JLabel searchPlayers, wait;
		private JButton exit;
		private JPanel exitJp;

		public SearchPlayersDialog(JFrame parent, LanguagePack language) {
			super(parent, language.mainFrameSearchPlayerDialogWindowName, true);
			searchPlayers = new JLabel(
					language.mainFrameSearchPlayerDialogSearchPlayers);
			wait = new JLabel(language.mainFrameSearchPlayerDialogWait);
			exit = new JButton(language.mainFrameSearchPlayerDialogExit);
			exitJp = new JPanel();
			setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.insets = new Insets(10, 20, 10, 20);
			gbc.weightx = 1;
			add(searchPlayers, gbc);

			gbc.gridy++;
			add(wait, gbc);

			gbc.gridy++;
			exit.setPreferredSize(new Dimension(140, 40));
			exitJp.add(exit);
			add(exitJp, gbc);
			exit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			pack();
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			setLocationRelativeTo(parent);
		}

		public void showDialog() {
			setVisible(true);
		}
	}

	private MainFrame getJFrame() {
		return this;
	}

	public LanguagePack getLang() {
		return lang;
	}
}
