package game.bomber.app;

/**
 * @author KOlkiewicz
 */
public class Polish extends LanguagePack {

	public Polish() {
		languageName = "Polski";

		openingFrameRegistration = "Rejestracja";
		openingFrameLogin = "Zaloguj si�";
		openingFramePlay = "<html><font size=+1 face=\"arial narrow\">Graj przez LAN";
		openingFrameOptions = "<html><font size=+1 face=\"arial narrow\">Opcje";
		openingFrameExit = "<html><font size=+1 face=\"arial narrow\">Wyj�cie";
		openingFrameTitle = "<html><font size=+2>BOMBKI";
		openingFrameWindowName = "Bombki";

		mainFrameAccount = "Zarz�dzanie kontem";
		mainFrameLogout = "Wyloguj si�";
		mainFrameTitle = "<html><font size=+2>BOMBKI";
		mainFramePlayNow = "<html><font size=+1 face=\"arial narrow\">Graj teraz!";
		mainFrameInvite = "<html><font size=+1 face=\"arial narrow\">Zapro� graczy";
		mainFramePlayers = "<html><font size=+1 face=\"arial narrow\">Gracze online";
		mainFramePlayLan = "<html><font size=+1 face=\"arial narrow\">Graj przez LAN";
		mainFrameOptions = "<html><font size=+1 face=\"arial narrow\">Opcje";
		mainFrameExit = "<html><font size=+1 face=\"arial narrow\">Wyj�cie";
		mainFrameWindowName = "Bombki";
		mainFrameSearchPlayerDialogSearchPlayers = "<html><font size=+2>Szukanie graczy";
		mainFrameSearchPlayerDialogWait = "Prosz� czeka�";
		mainFrameSearchPlayerDialogExit = "<html><font size=+1 face=\"arial narrow\">Wyjd�";
		mainFrameSearchPlayerDialogWindowName = "Szukanie graczy";
		mainFrameShowInvitationReject = "Odrzu�";
		mainFrameShowInvitationAccept = "Akceptuj";
		mainFrameShowInvitationMessage1 = "Zosta�e� zaproszony do gry(do ";
		mainFrameShowInvitationMessage2 =" wygranych) przez: ";
		mainFrameShowInvitationWindowName = "Zaproszenie";

		registrationFrameTitle = "<html><font size=+2>BOMBKI";
		registrationFrameRegistration = "<html><font size=+2>Rejestracja";
		registrationFrameUsername = "Nazwa u�ytkownika";
		registrationFramePassword1 = "Has�o";
		registrationFramePassword2 = "Powt�rz has�o";
		registrationFrameQuestion = "Twoje pytanie:";
		registrationFrameAnswer = "Odpowied�:";
		registrationFrameSend = "<html><font size=+1 face=\"arial narrow\">Wy�lij";
		registrationFrameBack = "<html><font size=+1 face=\"arial narrow\">Wstecz";
		registrationFrameWindowName = "Rejestracja";
		registrationFrameShowSuccessNext = "Dalej";
		registrationFrameShowSuccessMessage = "Rejestracja udana!";
		registrationFrameShowSuccessWindowName = "Sukces";
		registrationFrameShowErrorBack = "Wstecz";
		registrationFrameShowErrorMessage0 = "Nieprawid�owa nazwa u�ytkownika!\n(Nazwa u�ytkownika moze zawiera� jedynie znaki a-z, A-Z, 0-9 i _\noraz musi zawiera� conajmniej 6 znak�w).";
		registrationFrameShowErrorMessage1 = "Nazwa u�ytkownika jest zaj�ta.";
		registrationFrameShowErrorMessage2 = "Zbyt kr�tkie has�o.\n(Has�o powinno zawiera� conajmniej 8 znak�w).";
		registrationFrameShowErrorMessage3 = "Podane has�a nie pasuj�.";
		registrationFrameShowErrorMessage4 = "Pytanie nie mo�e by� puste.";
		registrationFrameShowErrorMessage5 = "Odpowied� na pytanie nie mo�e by� pusta.";
		registrationFrameShowErrorMessage6 = "Nie uda�o si� po��czy� z serwerem, spr�buj ponownie p�niej.";
		registrationFrameShowErrorWindowName = "B��d";

		loginFrameTitle = "<html><font size=+2>BOMBKI";
		loginFrameLoginTitle = "<html><font size=+2>Logowanie";
		loginFrameUsername = "Nazwa u�ytkownika";
		loginFramePassword = "Has�o";
		loginFrameLogin = "<html><font size=+1 face=\"arial narrow\">Zaloguj";
		loginFrameRecover = "<html><font size=+1 face=\"arial narrow\">Odzyskaj has�o";
		loginFrameBack = "<html><font size=+1 face=\"arial narrow\">Wstecz";
		loginFrameWindowName = "Logowanie";
		loginFrameShowErrorMessage0 = "Nieprawid�owa nazwa u�ytkownika lub has�o!"; 
		loginFrameShowErrorMessage1 = "Nie uda�o si� po��czy� z serwerem, spr�buj ponownie p�niej.";
		loginFrameShowErrorBack = "Wstecz"; 
		loginFrameShowErrorWindowName = "B��d";
		loginFrameRecoveryDialogTitle = "<html><font size=+2>Odzyskiwanie has�a";
		loginFrameRecoveryDialogUsername = "Podaj nazwe u�ytkownika";
		loginFrameRecoveryDialogNext = "<html><font size=+1 face=\"arial narrow\">Dalej";
		loginFrameRecoveryDialogWindowName = "Odzyskiwanie has�a";
		loginFrameRecoveryDialogShowSuccessTitle = "Uwaga";
		loginFrameRecoveryDialogShowSuccessMessage = "Twoje nowe has�o to: ";
		LoginFrameRecoveryDialogShowQuestionWindowName = "Pytanie";
		LoginFrameRecoveryDialogShowErrorMessage = "Nie ma takiego u�ytkownika.";
		LoginFrameRecoveryDialogShowErrorWindowName = "B��d";
		LoginFrameRecoveryDialogShowWrongAnswerMessage = "Z�a odpowied�!";
		LoginFrameRecoveryDialogShowWrongAnswerWindowName = "B��d";

		optionFrameTitle = "<html><font size=+2>BOMBKI";
		optionFrameOptionTitle = "<html><font size=+2>Opcje";
		optionFrameLanguageLabel = "J�zyk";
		optionFrameSave = "<html><font size=+1 face=\"arial narrow\">Zapisz";
		optionFrameBack = "<html><font size=+1 face=\"arial narrow\">Wstecz";
		optionFrameWindowName = "Opcje";

		LANFrameTitle = "<html><font size=+2>BOMBKI";
		LANFrameLANTitle = "<html><font size=+2>Graj przez LAN";
		LANFrameCreate = "<html><font size=+1 face=\"arial narrow\">Stw�rz serwer";
		LANFrameJoin = "<html><font size=+1 face=\"arial narrow\">Do��cz do gry";
		LANFrameBack = "<html><font size=+1 face=\"arial narrow\">Wstecz";
		LANFrameWindowName = "Graj przez LAN";

		newServerFrameTitle = "<html><font size=+2>BOMBKI";
		newServerFrameNewServerTitle = "<html><font size=+2>Nowy serwer LAN";
		newServerFrameWins = "Do ilu wygranych";
		newServerFrameTime = "Limit czasu na runde";
		newServerFrameNick = "Tw�j nick";
		newServerFrameNext = "<html><font size=+1 face=\"arial narrow\">Dalej";
		newServerFrameBack = "<html><font size=+1 face=\"arial narrow\">Wstecz";
		newGameFrameMinutes = "minut";
		newServerFrameWindowName = "Nowy serwer LAN";

		joinServerFrameTitle = "<html><font size=+2>BOMBKI";
		joinServerFrameJoinTitle = "<html><font size=+2>Do��cz do gry";
		joinServerFrameIp = "IP";
		joinServerFramePort = "Port";
		joinServerFrameNick = "Tw�j nick";
		joinServerFrameConnect = "<html><font size=+1 face=\"arial narrow\">Po��cz";
		joinServerFrameBack = "<html><font size=+1 face=\"arial narrow\">Wstecz";
		joinServerFrameWindowName = "Do��cz do gry";
		joinServerFrameShowErrorMessage1 = "Nazwa u�ytkownika jest zaj�ta";
		joinServerFrameShowErrorMessage2 = "Nie uda�o si� po��czy� z serwerem";
		joinServerFrameShowErrorBack = "Wstecz";
		joinServerFrameShowErrorWindowName = "B��d";

		newGameFrameTitle = "<html><font size=+2>BOMBKI";
		newGameFrameNewGameTitle = "<html><font size=+2>Nowa gra";
		newGameFrameWins = "Do ilu wygranych";
		newGameFrameTime = "Limit czasu na runde";
		newGameFramePlayer = "Gracz";
		newGameFrameSend = "Wy�lij";
		newGameFrameList = "<html><font size=+1 face=\"arial narrow\">Lista graczy";
		newGameFrameNext = "<html><font size=+1 face=\"arial narrow\">Dalej";
		newGameFrameBack = "<html><font size=+1 face=\"arial narrow\">Wstecz";
		newGameFrameWindowName = "Nowa gra";

		invitingFrameTitle = "<html><font size=+2>BOMBKI";
		invitingFrameInviteTitle = "<html><font size=+2>Zapro� graczy</html>";
		invitingFrameWaitingStatus = "czeka";
		invitingFramePlayingStatus = "gra";
		invitingFrameFreeStatus = "wolny";
		invitingFrameMeStatus = "<html><font color=\"red\">ja";
		invitingFrameColumnNames1 = "Nick";
		invitingFrameColumnNames2 = "Status";
		invitingFrameColumnNames3 = "Akcje";
		invitingFrameColumnNames4 = "Zapro�  ";
		invitingFrameStats = "Statystyki";
		invitingFrameInvite = "Zapro�";
		invitingFrameBack = "<html><font size=+1 face=\"arial narrow\">Wstecz";
		invitingFrameWindowName = "Zapro� graczy";

		playersOnlineFrameTitle = "<html><font size=+2>BOMBKI";
		playersOnlineFramePlayersTitle = "<html><font size=+2>Gracze online";
		playersOnlineFrameWaitingStatus = "czeka";
		playersOnlineFramePlayingStatus = "gra";
		playersOnlineFrameFreeStatus = "wolny";
		playersOnlineFrameMeStatus = "<html><font color=\"red\">ja";
		playersOnlineFrameColumnNames1 = "Nick";
		playersOnlineFrameColumnNames2 = "Status";
		playersOnlineFrameColumnNames3 = "Akcja";
		playersOnlineFrameStats = "Statystyki";
		playersOnlineFrameBack = "<html><font size=+1 face=\"arial narrow\">Wstecz";
		playersOnlineFrameWindowName = "Gracze online";

		statisticsFrameTitle = "<html><font size=+2>BOMBKI";
		statisticsFrameStatsTitle = "<html><font size=+2>Statystyki";
		statisticsFramePlayer = "Gracz:";
		statisticsFramePosition1 = "Pozycja 1.";
		statisticsFramePosition2 = "Pozycja 2.";
		statisticsFramePosition3 = "Pozycja 3.";
		statisticsFramePosition4 = "Pozycja 4.";
		statisticsFrameWins = "Wygrane rundy:";
		statisticsFrameLoses = "Przegrane rundy:";
		statisticsFramePoints = "Punkty:";
		statisticsFrameRank = "Pozycja w rankingu:";
		statisticsFrameBack = "<html><font size=+1 face=\"arial narrow\">Wstecz";
		statisticsFrameWindowName = "Statystyki";

		gameStarterFrameTitle = "<html><font size=+2>BOMBKI";
		gameStarterFrameWaitingTitle = "<html><font size=+2>Oczekiwanie na rozpocz�cie gry";
		gameStarterFrameReady = "Gotowy";
		gameStarterFrameNotReady = "Niegotowy";
		gameStarterFrameExit = "<html><font size=+1 face=\"arial narrow\">Wyj�cie";
		gameStarterFrameWindowName = "Oczekiwanie na rozpocz�cie gry";

		roundStarterFrameTitle = "<html><font size=+2>BOMBKI";
		roundStarterFrameWaitingTitle = "<html><font size=+2>Oczekiwanie na now� rund�";
		roundStarterFrameWins = "Wygrane rundy";
		roundStarterFrameReady = "Gotowy";
		roundStarterFrameNotReady = "Niegotowy";
		roundStarterFrameExit = "<html><font size=+1 face=\"arial narrow\">Wstecz";
		roundStarterFrameWindowName = "Oczekiwanie na now� rund�";
	}
}
