package game.bomber.app;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

/**
 * @author KOlkiewicz
 */
public class OptionFrame extends JFrame {
	private JLabel title, optionTitle;
	private JLabel languageLabel;
	private JComboBox languageBox;
	private JButton save, back;
	private JPanel saveJp, backJp, languageBoxJp;
	private String[] languages;
	private LanguagePack lang;
	private LanguageChanger parent;
	public OptionFrame(final LanguageChanger parent, LanguagePack language){
		this.parent = parent;
		setTitle(language.optionFrameWindowName);
		title = new JLabel(language.optionFrameTitle);
		optionTitle = new JLabel(language.optionFrameOptionTitle);
		languageLabel = new JLabel(language.optionFrameLanguageLabel);
		save = new JButton(language.optionFrameSave);
		back = new JButton(language.optionFrameBack);
		languages = new String[]{"  Polski", "  English"};
		languageBox = new JComboBox(languages);
		saveJp = new JPanel();
		backJp = new JPanel();
		languageBoxJp = new JPanel();
		
		save.setPreferredSize(new Dimension(140, 40));
		back.setPreferredSize(new Dimension(140, 40));
		
		lang = language;
		
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth=3;
		gbc.insets = new Insets(10,10,10,10);
		add(title, gbc);
		
		gbc.gridy++;
		gbc.insets = new Insets(0,10,10,10);
		add(optionTitle, gbc);
		
		gbc.gridy++;
		gbc.gridwidth=1;
		gbc.insets = new Insets(20,10,10,10);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.2;
		add(languageLabel, gbc);
		
		gbc.gridx++;
		gbc.gridwidth=2;
		gbc.weightx = 1;
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.fill = GridBagConstraints.NONE;
		languageBox.setPreferredSize(new Dimension(200,languageBox.getPreferredSize().height));
		languageBoxJp.setLayout(new BorderLayout());
		for(int i=0; i<languages.length; i++){
			if(languages[i].equals("  " + language.languageName)){
				languageBox.setSelectedIndex(i);
			}
		}
		languageBoxJp.add(languageBox, BorderLayout.WEST);
		//languageBoxJp.setBorder(new LineBorder(Color.BLUE));
		add(languageBoxJp, gbc);
		
		gbc.gridy++;
		gbc.gridx=0;
		gbc.gridwidth=3;
		gbc.weighty=1;
		gbc.anchor = GridBagConstraints.PAGE_END;
		gbc.insets = new Insets(10,10,0,10);
		saveJp.add(save);
		add(saveJp, gbc);
		
		gbc.gridy++;
		gbc.insets = new Insets(0,10,10,10);
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.weighty=0;
		backJp.add(back);
		add(backJp, gbc);
		
		setSize(400, 550);
		addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
            	goBack();
            }
        });
		setVisible(true);
		setLocationRelativeTo((Component) parent);
		
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(languageBox.getSelectedIndex()==0){
					lang = new Polish();
				}
				if(languageBox.getSelectedIndex()==1){
					lang = new English();
				}
				changeLanguage(lang);
				LanguageChanger tmp = parent.changeLanguage(lang);
				setParent(tmp);
			}
		});
		back.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				goBack();
			}
		});
	}
	public void changeLanguage(LanguagePack language){
		setTitle(language.optionFrameWindowName);
		title.setText(language.optionFrameTitle);
		optionTitle.setText(language.optionFrameOptionTitle);
		languageLabel.setText(language.optionFrameLanguageLabel);
		save.setText(language.optionFrameSave);
		back.setText(language.optionFrameBack);
		for(int i=0; i<languages.length; i++){
			if(languages[i].equals("  " + language.languageName)){
				languageBox.setSelectedIndex(i);
			}
		}
	}
	public void setParent(LanguageChanger newParent){
		this.parent = newParent;
	}
	public void goBack(){
		parent.setVisible(true);
		dispose();
	}
}
