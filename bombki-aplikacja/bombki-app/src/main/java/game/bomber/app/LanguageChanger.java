package game.bomber.app;

/**
 * @author KOlkiewicz
 */
public interface LanguageChanger {

	public LanguageChanger changeLanguage(LanguagePack language);
	public void setVisible(boolean b);
}
