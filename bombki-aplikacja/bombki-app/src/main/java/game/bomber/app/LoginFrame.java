package game.bomber.app;

import javax.swing.*;

import game.bomber.common.Request;
import game.bomber.common.Response;
import game.bomber.communication.global.MClientCommunicator;

import java.awt.*;
import java.awt.event.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author KOlkiewicz
 */
public class LoginFrame extends JFrame {
	private JLabel title, loginTitle;
	private JLabel username, password;
	private JTextField usernameTF;
	private JPasswordField passwordTF;
	private JButton login, recover, back;
	private JPanel loginJp, recoverJp, backJp;
	private RecoveryDialog dialog;
	private JFrame parent;
	private MClientCommunicator clientCommunicator;

	public static final int INCORRECT_PASSWORD = 0;
	public static final int DISCONNECTED = 1;

	public LoginFrame(final JFrame parent, final LanguagePack language,
			MClientCommunicator communicator) {
		clientCommunicator = communicator;
		setTitle(language.loginFrameWindowName);
		title = new JLabel(language.loginFrameTitle);
		loginTitle = new JLabel(language.loginFrameLoginTitle);
		username = new JLabel(language.loginFrameUsername);
		password = new JLabel(language.loginFramePassword);
		usernameTF = new JTextField(25);
		passwordTF = new JPasswordField(25);
		login = new JButton(language.loginFrameLogin);
		recover = new JButton(language.loginFrameRecover);
		back = new JButton(language.loginFrameBack);
		loginJp = new JPanel();
		recoverJp = new JPanel();
		backJp = new JPanel();
		dialog = new RecoveryDialog(this, language);
		this.parent = parent;

		login.setPreferredSize(new Dimension(140, 40));
		recover.setPreferredSize(new Dimension(140, 40));
		back.setPreferredSize(new Dimension(140, 40));

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		gbc.insets = new Insets(10, 10, 10, 10);
		add(title, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0, 10, 20, 10);
		add(loginTitle, gbc);

		gbc.gridy++;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(5, 10, 5, 10);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		add(username, gbc);

		gbc.gridy++;
		add(password, gbc);

		gbc.gridy--;
		gbc.gridx++;
		gbc.gridwidth = 2;
		add(usernameTF, gbc);

		gbc.gridy++;
		add(passwordTF, gbc);

		gbc.gridy++;
		gbc.gridx = 0;
		gbc.gridwidth = 3;
		gbc.insets = new Insets(10, 10, 5, 10);
		loginJp.add(login);
		add(loginJp, gbc);

		gbc.gridy++;

		gbc.insets = new Insets(0, 10, 10, 10);
		recoverJp.add(recover);
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.weighty = 1;
		add(recoverJp, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(10, 10, 10, 10);
		gbc.anchor = GridBagConstraints.PAGE_END;

		backJp.add(back);
		add(backJp, gbc);

		setSize(400, 550);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				parent.setVisible(true);
				dispose();
			}
		});
		setVisible(true);
		setLocationRelativeTo(parent);

		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (clientCommunicator.isConnected() == false) {
						showError(language, DISCONNECTED);
					} else {
						usernameTF.setEnabled(false);
						Map<String, String> params = new HashMap<String, String>();
						params.put("nazwa uzytkownika", usernameTF.getText());
						MessageDigest md = MessageDigest.getInstance("MD5");
						md.update(String.valueOf(passwordTF.getPassword()).getBytes("UTF-8"));
						byte[] digest = md.digest();
						StringBuffer sb = new StringBuffer();
						for (byte b : digest) {
							sb.append(String.format("%02x", b & 0xff));
						}
						params.put("haslo", sb.toString());
						
						Response response = clientCommunicator
								.sendRequest(new Request(Request.REQUEST.LOGIN,
										params));
						if (response.getParam("potwierdzenie").equals("true")) {
							MainFrame mainFrame = new MainFrame(language, clientCommunicator);
							mainFrame.setLocationRelativeTo(getJFrame());
							mainFrame.setUsername(usernameTF.getText());
							clientCommunicator.setMainFrame(mainFrame);
							clientCommunicator.createRequestListener();
							parent.dispose();
							dispose();
						} else {
							usernameTF.setEnabled(true);
							showError(language, INCORRECT_PASSWORD);
						}
					}
				} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		recover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(true);
			}
		});
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.setVisible(true);
				dispose();
			}
		});
	}

	public void changeLanguage(LanguagePack language) {
		setTitle(language.loginFrameWindowName);
		title.setText(language.loginFrameTitle);
		loginTitle.setText(language.loginFrameLoginTitle);
		username.setText(language.loginFrameUsername);
		password.setText(language.loginFramePassword);
		login.setText(language.loginFrameLogin);
		recover.setText(language.loginFrameRecover);
		back.setText(language.loginFrameBack);

		dialog.setTitle(language.loginFrameRecoveryDialogWindowName);
		dialog.title.setText(language.loginFrameRecoveryDialogTitle);
		dialog.username.setText(language.loginFrameRecoveryDialogUsername);
		dialog.next.setText(language.loginFrameRecoveryDialogNext);
		dialog.pack();
	}

	public void showError(LanguagePack language, int errorCode) {
		String message = "";
		switch (errorCode) {
		case INCORRECT_PASSWORD:
			message = language.loginFrameShowErrorMessage0;
			break;
		case DISCONNECTED:
			message = language.loginFrameShowErrorMessage1;
			break;
		}
		Object options[] = { language.loginFrameShowErrorBack };
		JOptionPane.showOptionDialog(this, message,
				language.loginFrameShowErrorWindowName,
				JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null,
				options, options[0]);
	}
	private JFrame getJFrame(){
		return this;
	}

	class RecoveryDialog extends JDialog {
		private JLabel title, username;
		private JTextField usernameTF;
		private JButton next;
		private JPanel nextJp;

		public RecoveryDialog(JFrame parent, final LanguagePack language) {
			super(parent, language.loginFrameRecoveryDialogWindowName, true);
			this.title = new JLabel(language.loginFrameRecoveryDialogTitle);
			this.username = new JLabel(
					language.loginFrameRecoveryDialogUsername);
			this.usernameTF = new JTextField(15);
			this.next = new JButton(language.loginFrameRecoveryDialogNext);
			this.nextJp = new JPanel();

			this.next.setPreferredSize(new Dimension(140, 40));

			setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();

			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.insets = new Insets(10, 10, 5, 10);
			add(this.title, gbc);

			gbc.gridy++;
			gbc.insets = new Insets(5, 10, 5, 10);
			add(this.username, gbc);

			gbc.gridy++;
			add(this.usernameTF, gbc);

			gbc.gridy++;
			gbc.insets = new Insets(10, 10, 10, 10);
			this.nextJp.add(this.next);
			add(this.next, gbc);

			pack();
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			setLocationRelativeTo(parent);

			next.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (clientCommunicator.isConnected() == false) {
						LoginFrame.this.showError(language, DISCONNECTED);
					} else {
						try {
							Map<String, String> params = new HashMap<String, String>();
							params.put("nazwa uzytkownika",
									usernameTF.getText());
							Response response = clientCommunicator
									.sendRequest(new Request(
											Request.REQUEST.GET_QUESTION,
											params));

							if (response.getParam("potwierdzenie").equals(
									"true")) {
								params = new HashMap<String, String>();
								params.put("nazwa uzytkownika",
										usernameTF.getText());
								String value = showQuestion(language,
										response.getParam("pytanie"));
								MessageDigest md = MessageDigest.getInstance("MD5");
								md.update(value.getBytes("UTF-8"));
								byte[] digest = md.digest();
								StringBuffer sb = new StringBuffer();
								for (byte b : digest) {
									sb.append(String.format("%02x", b & 0xff));
								}
								params.put("odpowiedz", sb.toString());
								response = clientCommunicator
										.sendRequest(new Request(
												Request.REQUEST.ANSWER_QUESTION,
												params));
								if (response.getParam("potwierdzenie").equals(
										"true")) {
									showSuccess(language,
											response.getParam("haslo"));
									dispose();
								} else {
									showWrongAnswer(language);
								}

							} else {
								showError(language);
							}
						} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			});
		}

		public void showDialog() {
			setVisible(true);
		}

		public String showQuestion(LanguagePack language, String question) {
			return JOptionPane.showInputDialog(this, question,
					language.LoginFrameRecoveryDialogShowQuestionWindowName,
					JOptionPane.QUESTION_MESSAGE);
		}

		public void showError(LanguagePack language) {
			JOptionPane.showMessageDialog(this,
					language.LoginFrameRecoveryDialogShowErrorMessage,
					language.LoginFrameRecoveryDialogShowErrorWindowName,
					JOptionPane.ERROR_MESSAGE);
		}

		public void showWrongAnswer(LanguagePack language) {
			JOptionPane.showMessageDialog(this,
					language.LoginFrameRecoveryDialogShowWrongAnswerMessage,
					language.LoginFrameRecoveryDialogShowWrongAnswerWindowName,
					JOptionPane.ERROR_MESSAGE);
		}

		public void showSuccess(LanguagePack language, String password) {
			JOptionPane.showMessageDialog(this,
					language.loginFrameRecoveryDialogShowSuccessMessage
							+ password,
					language.loginFrameRecoveryDialogShowSuccessTitle,
					JOptionPane.INFORMATION_MESSAGE);
		}
	}
}
