package game.bomber.app;

import javax.swing.*;

import game.bomber.common.Request;
import game.bomber.common.Response;
import game.bomber.communication.global.MClientCommunicator;

import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author KOlkiewicz
 */
public class InvitingFrame extends JFrame {
	private JLabel title, inviteTitle;
	private JScrollPane scroll;
	private JLabel[] columnNames;
	private JButton back;
	private LinkedList<User> users;
	private JPanel panel, backJp, main;
	private NewGameFrame parent;
	private int parentNumber;
	private String value;
	private MClientCommunicator clientCommunicator;

	public InvitingFrame(final NewGameFrame parent,int parentNumber, LanguagePack language, MClientCommunicator communicator) {
		clientCommunicator = communicator;
		setTitle(language.invitingFrameWindowName);
		title = new JLabel(language.invitingFrameTitle);
		inviteTitle = new JLabel(language.invitingFrameInviteTitle);
		columnNames = new JLabel[] {
				new JLabel(language.invitingFrameColumnNames1),
				new JLabel(language.invitingFrameColumnNames2),
				new JLabel(language.invitingFrameColumnNames3),
				new JLabel(language.invitingFrameColumnNames4) };
		users = new LinkedList<User>();
		back = new JButton(language.invitingFrameBack);
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		backJp = new JPanel();
		scroll = new JScrollPane(panel);
		value = null;
		this.parent = parent;
		this.parentNumber = parentNumber;

		back.setPreferredSize(new Dimension(140, 40));

		main = new JPanel();
		main.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 4;
		gbc.insets = new Insets(10, 10, 0, 10);
		gbc.weightx = 1;
		main.add(title, gbc);

		gbc.gridy++;
		main.add(inviteTitle, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(10, 0, 0, 0);
		gbc.gridwidth = 1;
		main.add(columnNames[0], gbc);

		gbc.gridx++;
		main.add(columnNames[1], gbc);

		gbc.gridx++;
		main.add(columnNames[2], gbc);

		gbc.gridx++;
		main.add(columnNames[3], gbc);

		backJp.add(back);

		setLayout(new BorderLayout());
		add(main, BorderLayout.NORTH);
		add(backJp, BorderLayout.SOUTH);
		add(scroll);

		setSize(400, 550);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				parent.setVisible(true);
				dispose();
			}
		});
		setVisible(true);
		setLocationRelativeTo(parent);
		
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.setVisible(true);
				dispose();
				
			}
		});
		Response response = clientCommunicator
				.sendRequest(new Request(Request.REQUEST.PLAYERS_LIST,
						null));
		int tmp=0;
		for(Entry<String, String> entry : response.getParams().entrySet()){
			String name = entry.getValue().substring(0, entry.getValue().length()-1);
			int currentStatus = Integer.valueOf(entry.getValue().substring(entry.getValue().length()-1));
			addUser(language, name, currentStatus);
		}

	}

	public void addUser(LanguagePack language, String username, int currentStatus) {
		User tmp;
		boolean flag = false;
		if (users.size() == 0) {
			users.add(new User(language, username, currentStatus));
			refresh();
		} else {
			for (int i = 0; i < users.size(); i++) {
				tmp = users.get(i);
				if (tmp.getUsername().compareToIgnoreCase(username) > 0
						&& flag == false) {
					users.add(i, new User(language, username, currentStatus));
					flag = true;
					refresh();
					break;
				}

				if (tmp.getUsername().compareTo(username) == 0 && flag == false) {
					flag = true;
					break;
				}

			}
			if (flag == false) {
				users.add(users.size(), new User(language, username, currentStatus));
				refresh();
			}
		}
	}

	public void refresh() {
		panel.removeAll();
		if (users.size() > 0) {
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1;
			for (int i = 0; i < users.size() - 1; i++) {

				panel.add(users.get(i), gbc);
				gbc.gridy++;
			}
			gbc.anchor = GridBagConstraints.PAGE_START;
			gbc.weighty = 1;
			panel.add(users.get(users.size() - 1), gbc);
		}
		panel.revalidate();
	}

	public void changeLanguage(LanguagePack language) {
		setTitle(language.invitingFrameWindowName);
		title.setText(language.invitingFrameTitle);
		inviteTitle.setText(language.invitingFrameInviteTitle);
		columnNames[0].setText(language.invitingFrameColumnNames1);
		columnNames[1].setText(language.invitingFrameColumnNames2);
		columnNames[2].setText(language.invitingFrameColumnNames3);
		columnNames[3].setText(language.invitingFrameColumnNames4);
		back.setText(language.invitingFrameBack);
		for (int i = 0; i < users.size(); i++) {
			users.get(i).changeLanguage(language);
		}
	}

	public InvitingFrame getJFrame() {
		return this;
	}

	class User extends JPanel {
		private JLabel nick, status;
		private JButton stats, invite;
		private String waitingStatus, playingStatus, freeStatus, meStatus;
		public static final int WAIT = 1;
		public static final int PLAY = 2;
		public static final int FREE = 3;
		public static final int ME = 4;
		private int myStatus;
		private JPanel nickJp, statusJp, statsJp, inviteJp;

		public User(final LanguagePack language, String username, int currentStatus) {
			waitingStatus = language.invitingFrameWaitingStatus;
			playingStatus = language.invitingFramePlayingStatus;
			freeStatus = language.invitingFrameFreeStatus;
			meStatus = language.invitingFrameMeStatus;
			nick = new JLabel(username);
			status = new JLabel(freeStatus);
			myStatus = 3;
			stats = new JButton(language.invitingFrameStats);
			invite = new JButton(language.invitingFrameInvite);
			nickJp = new JPanel();
			statusJp = new JPanel();
			statsJp = new JPanel();
			inviteJp = new JPanel();

			// setBorder(new LineBorder(Color.BLUE));

			this.setLayout(new GridLayout(1, 4));
			nickJp.setLayout(new GridBagLayout());
			nickJp.add(nick);

			add(nickJp);

			statusJp.setLayout(new GridBagLayout());
			statusJp.add(status);
			add(statusJp);
			statsJp.add(stats);
			add(statsJp);
			inviteJp.add(invite);
			add(inviteJp);
			stats.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Map<String, String> params = new HashMap<String, String>();
					params.put("nazwa uzytkownika", nick.getText());
					Response response = clientCommunicator
							.sendRequest(new Request(Request.REQUEST.STATS,
									params));
					new StatisticsFrame(getJFrame(), language, nick.getText(), response.getParams());
				}
			});
			invite.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					value=nick.getText();
					parent.setUsername(nick.getText(), parentNumber);
					parent.setVisible(true);
					dispose();
				}
			});
			setStatus(currentStatus);
		}

		public String getUsername() {
			return nick.getText();
		}

		public void setStatus(int arg) {
			if (arg == User.WAIT) {
				invite.setVisible(false);
				status.setText(this.waitingStatus);
				myStatus = 1;
			}
			if (arg == User.PLAY) {
				invite.setVisible(false);
				status.setText(this.playingStatus);
				myStatus = 2;
			}
			if (arg == User.FREE) {
				invite.setVisible(true);
				status.setText(this.freeStatus);
				myStatus = 3;
			}
			if (arg == User.ME) {
				invite.setVisible(false);
				status.setText(this.meStatus);
				myStatus = 3;
			}
		}

		public void changeLanguage(LanguagePack language) {
			waitingStatus = language.invitingFrameWaitingStatus;
			playingStatus = language.invitingFramePlayingStatus;
			freeStatus = language.invitingFrameFreeStatus;
			stats.setText(language.invitingFrameStats);
			invite.setText(language.invitingFrameInvite);
			setStatus(myStatus);
		}
	}
}
