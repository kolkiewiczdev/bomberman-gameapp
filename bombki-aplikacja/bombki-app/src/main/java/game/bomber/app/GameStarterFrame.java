package game.bomber.app;

import javax.swing.*;

import game.bomber.common.GameConfig;
import game.bomber.common.MatchConfig;
import game.bomber.common.PlayerSimple;
import game.bomber.common.Request;
import game.bomber.common.Response;
import game.bomber.communication.game.GClientToGServer;
import game.bomber.communication.game.GServerToGClients;
import game.bomber.communication.game.MatchResultSender;
import game.bomber.communication.global.MClientCommunicator;
import game.bomber.gklient.GameClient;
import game.bomber.gserwer.GameServer;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author KOlkiewicz
 */

public class GameStarterFrame extends JFrame {
	private JLabel title, waitingTitle;
	private JButton ready, exit;
	private ArrayList<JLabel> players;
	private ArrayList<Icon> icons;
	private ArrayList<JLabel> iconLabels;
	private JPanel exitJp, panel;
	private int ownerNumber;
	private JFrame parent;
	private GClientToGServer gClientToGServer;
	private GServerToGClients gServerToGClients;
	private MClientCommunicator clientCommunicator;
	private LanguagePack language;
	private boolean canExit;
	private String myUsername;

	public GameStarterFrame(final JFrame parent, final LanguagePack language,
			MClientCommunicator communicator,
			final GClientToGServer gClientToGServer,
			final GServerToGClients gServerToGClients, String username) {
		myUsername = username;
		this.clientCommunicator = communicator;
		this.gClientToGServer = gClientToGServer;
		this.gServerToGClients = gServerToGClients;
		this.language = language;
		canExit = true;
		setTitle(language.gameStarterFrameWindowName);
		title = new JLabel(language.gameStarterFrameTitle);
		waitingTitle = new JLabel(language.gameStarterFrameWaitingTitle);
		ready = new JButton(language.gameStarterFrameReady);
		exit = new JButton(language.gameStarterFrameExit);
		players = new ArrayList<JLabel>();
		icons = new ArrayList<Icon>();
		iconLabels = new ArrayList<JLabel>();
		ownerNumber = 0;
		exitJp = new JPanel();
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		this.parent = parent;

		exit.setPreferredSize(new Dimension(140, 40));

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 10, 0, 10);
		add(title, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(10, 10, 20, 10);
		add(waitingTitle, gbc);

		gbc.gridy++;
		gbc.weighty = 1;
		gbc.weightx = 1;
		gbc.anchor = GridBagConstraints.PAGE_START;
		add(panel, gbc);

		gbc.gridy++;
		gbc.weighty = 0;
		gbc.weightx = 0;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(10, 10, 10, 10);
		exitJp.add(exit);
		add(exitJp, gbc);

		setSize(400, 550);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (canExit) {
					if (gServerToGClients == null) {
						Map<String, String> params = new HashMap<String, String>();
						params.put("status", "3");
						Request request = new Request(
								Request.REQUEST.SET_STATUS, params);
						gClientToGServer.sendRequest(request);
						if (clientCommunicator == null) {
							parent.setVisible(true);
							dispose();
						}
					} else {
						if (clientCommunicator == null) {
							Map<String, String> params2 = new HashMap<String, String>();
							params2.put("rozmiar", "0");
							Request request = new Request(
									Request.REQUEST.PLAYERS_LIST, params2);
							gServerToGClients.sendRequest(request);
							try {
								gClientToGServer.finalize();
							} catch (Throwable e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							gServerToGClients.close();
							parent.setVisible(true);
							dispose();
						}
					}
					if (clientCommunicator != null) {
						Map<String, String> params3 = new HashMap<String, String>();
						params3.put("status", "3");
						Response response = clientCommunicator
								.sendRequest(new Request(
										Request.REQUEST.SET_STATUS, params3));
						parent.setVisible(true);
						dispose();
					}
				}
			}
		});
		setVisible(true);
		setLocationRelativeTo(parent);

		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (gServerToGClients == null) {
					Map<String, String> params = new HashMap<String, String>();
					params.put("status", "3");
					Request request = new Request(Request.REQUEST.SET_STATUS,
							params);
					gClientToGServer.sendRequest(request);
					if (clientCommunicator == null) {
						parent.setVisible(true);
						dispose();
					}
				} else {
					if (clientCommunicator == null) {
						Map<String, String> params2 = new HashMap<String, String>();
						params2.put("rozmiar", "0");
						Request request = new Request(
								Request.REQUEST.PLAYERS_LIST, params2);
						gServerToGClients.sendRequest(request);
						try {
							gClientToGServer.finalize();
						} catch (Throwable e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						gServerToGClients.close();
						parent.setVisible(true);
						dispose();
					}
				}
				if (clientCommunicator != null) {
					Map<String, String> params3 = new HashMap<String, String>();
					params3.put("status", "3");
					Response response = clientCommunicator
							.sendRequest(new Request(
									Request.REQUEST.SET_STATUS, params3));
					parent.setVisible(true);
					dispose();
				}

			}
		});
		gClientToGServer.setGameStarterFrame(this);
		ready.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (ready.getText().equals(
						language.gameStarterFrameReady)) {
					Map<String, String> params = new HashMap<String, String>();
					params.put("status", "2");
					Request request = new Request(
							Request.REQUEST.SET_STATUS, params);
					gClientToGServer.sendRequest(request);
					ready.setEnabled(false);
				} else {
					Map<String, String> params = new HashMap<String, String>();
					params.put("status", "1");
					Request request = new Request(
							Request.REQUEST.SET_STATUS, params);
					gClientToGServer.sendRequest(request);
					ready.setEnabled(false);
				}
			}
		});
	}

	public void addUsers(Map<String, String> params) {
		String[] playersList = new String[Integer
				.valueOf(params.get("rozmiar"))];
		String[] statusesList = new String[Integer.valueOf(params
				.get("rozmiar"))];
		for (Entry<String, String> entry : params.entrySet()) {
			if (!entry.getKey().equals("rozmiar")) {
				playersList[Integer.valueOf(entry.getValue().substring(0, 1)) - 1] = entry
						.getKey();
				statusesList[Integer.valueOf(entry.getValue().substring(0, 1)) - 1] = entry
						.getValue().substring(1);
			}
		}

		for (int i = 0; i < playersList.length; i++) {
			players.add(new JLabel(playersList[i]));
			if (statusesList[i].equals("false")) {
				icons.add(new ImageIcon("wait.gif"));
			} else {
				icons.add(new ImageIcon("accept.png"));
			}
			iconLabels.add(new JLabel(icons.get(icons.size() - 1)));
			if (playersList[i].equals(gClientToGServer.getUsername())) {
				ownerNumber = i + 1;
				if (statusesList[i].equals("false")) {
					ready.setText(language.gameStarterFrameReady);
				} else {
					ready.setText(language.gameStarterFrameNotReady);
				}
				ready.setEnabled(true);
				
			}
		}
		refresh();

		if (params.get("rozmiar").equals("0")) {

			if (clientCommunicator != null) {
				Map<String, String> params3 = new HashMap<String, String>();
				params3.put("status", "3");
				clientCommunicator.sendRequest(new Request(
						Request.REQUEST.SET_STATUS, params3));
			}
			if (gServerToGClients == null) {
				parent.setVisible(true);
				dispose();
				gClientToGServer.setGameStarterFrame(null);
				try {
					gClientToGServer.finalize();
				} catch (Throwable e1) {
					e1.printStackTrace();
				}
			}
		}
		if (params.get("rozmiar").equals("1")) {
			if (clientCommunicator != null) {
				gClientToGServer.setGameStarterFrame(null);
				Map<String, String> params2 = new HashMap<String, String>();
				params2.put("status", "1");
				Request request2 = new Request(Request.REQUEST.SET_STATUS,
						params2);
				gClientToGServer.sendRequest(request2);
				parent.setVisible(true);
				dispose();
			}
		}
	}

	public void removeUser(String username) {
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i).getText().equals(username)) {
				players.remove(i);
				icons.remove(i);
				iconLabels.remove(i);
			}
		}
		refresh();
	}

	public void removeAll() {
		synchronized (this) {
			players.clear();
			icons.clear();
			iconLabels.clear();

			panel.removeAll();
			refresh();
		}
	}

	public void refresh() {
		panel.removeAll();
		if (players.size() > 0) {
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.insets = new Insets(5, 20, 5, 20);
			for (int i = 0; i < players.size() - 1; i++) {
				JPanel tmp = new JPanel();
				tmp.setLayout(new GridBagLayout());
				tmp.add(players.get(i));
				panel.add(tmp, gbc);
				gbc.gridy++;
			}
			// gbc.anchor = GridBagConstraints.PAGE_START;
			gbc.weighty = 1;
			JPanel tmp = new JPanel();
			tmp.setLayout(new GridBagLayout());
			tmp.add(players.get(players.size() - 1));
			panel.add(tmp, gbc);

			gbc.gridx = 1;
			gbc.gridy = 0;
			gbc.weighty = 0;
			for (int i = 0; i < iconLabels.size() - 1; i++) {

				panel.add(iconLabels.get(i), gbc);
				gbc.gridy++;
			}
			gbc.anchor = GridBagConstraints.PAGE_START;
			gbc.weighty = 1;
			panel.add(iconLabels.get(iconLabels.size() - 1), gbc);

			if (ownerNumber > 0) {
				gbc.gridx = 2;
				gbc.gridy = ownerNumber - 1;
				gbc.weighty = 0;
				panel.add(ready, gbc);
			}
		}

		panel.revalidate();
	}

	public LanguagePack getLanguage() {
		return language;
	}

	public void startGame() {

		GameConfig gameConfig = new GameConfig();
		
		// int maxRound, int wins, GameConfig gameConfig
		MatchConfig config = new MatchConfig(1, 3, gameConfig);
		
		// int id, String username
		PlayerSimple[] playerSimples = new PlayerSimple[players.size()];
		int id=-1;
		for(int i=0; i<players.size();i++){
			playerSimples[i] = new PlayerSimple(i, players.get(i).getText());
			System.out.println(i + ":" + players.get(i).getText());
			if(players.get(i).getText().equals(myUsername)){
				id=i;
			}
		}		

		if (clientCommunicator != null) {
			Map<String, String> params = new HashMap<String, String>();
			params.put("status", "2");
			Response response = clientCommunicator.sendRequest(new Request(
					Request.REQUEST.SET_STATUS, params));
		}
		if (players.size() > 1) {
			if (gServerToGClients != null) {
				
				MatchResultSender matchResultSender = null;
				
				Runnable r = new GameServer(config, playerSimples, gServerToGClients, gServerToGClients, matchResultSender);
				
				Thread gServerThread = new Thread(r);
				gServerThread.start();
			}

			Runnable r2 = new GameClient(config, playerSimples, gClientToGServer, gClientToGServer, id, this);
			Thread gClientThread = new Thread(r2);
			gClientThread.start();

			setVisible(false);
			ready.setEnabled(false);
			exit.setEnabled(false);
			canExit = false;
		}
	}
}
