package game.bomber.app;

import javax.swing.*;

import game.bomber.common.PlayerSimple;
import game.bomber.communication.game.GClientToGServer;
import game.bomber.game.exceptions.NoSuchServerException;
import game.bomber.game.exceptions.WrongUsernameException;

import java.awt.*;
import java.awt.event.*;

/**
 * @author KOlkiewicz
 */
public class JoinServerFrame extends JFrame {
	private JLabel title, joinTitle;
	private JLabel ip, port, nick;
	private JTextField ipTF, portTF, nickTF;
	private JButton connect, back;
	private JPanel connectJp, backJp;
	private JFrame parent;

	public JoinServerFrame(final JFrame parent, final LanguagePack language) {
		setTitle(language.joinServerFrameWindowName);
		title = new JLabel(language.joinServerFrameTitle);
		joinTitle = new JLabel(language.joinServerFrameJoinTitle);
		ip = new JLabel(language.joinServerFrameIp);
		port = new JLabel(language.joinServerFramePort);
		nick = new JLabel(language.joinServerFrameNick);
		ipTF = new JTextField(25);
		portTF = new JTextField(25);
		nickTF = new JTextField(25);
		connect = new JButton(language.joinServerFrameConnect);
		back = new JButton(language.joinServerFrameBack);
		connectJp = new JPanel();
		backJp = new JPanel();
		this.parent = parent;

		connect.setPreferredSize(new Dimension(140, 40));
		back.setPreferredSize(new Dimension(140, 40));

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 4;
		gbc.insets = new Insets(20, 10, 10, 10);
		add(title, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0, 10, 40, 10);
		add(joinTitle, gbc);

		gbc.gridy++;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(0, 10, 5, 10);
		gbc.weightx = 0.3;
		gbc.anchor = GridBagConstraints.LINE_END;
		add(ip, gbc);

		gbc.gridy++;
		add(port, gbc);

		gbc.gridy++;
		add(nick, gbc);

		gbc.gridx++;
		gbc.gridy = 2;
		gbc.gridwidth = 3;
		gbc.weightx = 1;
		gbc.anchor = GridBagConstraints.LINE_START;
		add(ipTF, gbc);

		gbc.gridy++;
		add(portTF, gbc);

		gbc.gridy++;
		add(nickTF, gbc);

		gbc.gridy++;
		gbc.gridx--;
		gbc.gridwidth = 4;
		gbc.weighty = 1;
		gbc.anchor = GridBagConstraints.PAGE_END;
		gbc.insets = new Insets(0, 10, 0, 10);
		connectJp.add(connect);
		add(connectJp, gbc);

		gbc.gridy++;
		gbc.weighty = 0;
		gbc.insets = new Insets(0, 10, 10, 10);
		backJp.add(back);
		add(backJp, gbc);

		setSize(400, 550);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				parent.setVisible(true);
				dispose();
			}
		});
		setVisible(true);
		setLocationRelativeTo(parent);

		connect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PlayerSimple player = new PlayerSimple(-1, nickTF.getText());
				int port = Integer.valueOf(portTF.getText());
				boolean flag = true;
				GClientToGServer gClientToGServer = null;
				try {
					gClientToGServer = new GClientToGServer(
							ipTF.getText(), port, port + 1, port + 2, port + 3,
							port + 4, player, -1);
				} catch (WrongUsernameException e1) {
					flag = false;
					Object options[] = { language.joinServerFrameShowErrorBack };
					JOptionPane.showOptionDialog(getJFrame(), language.joinServerFrameShowErrorMessage1,
							language.joinServerFrameShowErrorWindowName,
							JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null,
							options, options[0]);
				} catch (NoSuchServerException e1) {
					flag = false;
					Object options[] = { language.joinServerFrameShowErrorBack };
					JOptionPane.showOptionDialog(getJFrame(), language.joinServerFrameShowErrorMessage2,
							language.joinServerFrameShowErrorWindowName,
							JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null,
							options, options[0]);
				}
				if (flag) {
					new GameStarterFrame(getJFrame(), language, null,
							gClientToGServer, null, nickTF.getText());
					setVisible(false);
				}
			}
		});
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.setVisible(true);
				dispose();
			}
		});
	}

	public void changeLanguage(LanguagePack language) {
		setTitle(language.joinServerFrameWindowName);
		title.setText(language.joinServerFrameTitle);
		joinTitle.setText(language.joinServerFrameJoinTitle);
		ip.setText(language.joinServerFrameIp);
		port.setText(language.joinServerFramePort);
		nick.setText(language.joinServerFrameNick);
		connect.setText(language.joinServerFrameConnect);
		back.setText(language.joinServerFrameBack);
	}

	public void showError(){
		
	}
	private JoinServerFrame getJFrame() {
		return this;
	}
}
