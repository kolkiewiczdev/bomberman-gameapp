package game.bomber.app;

import javax.swing.*;

import game.bomber.common.PlayerSimple;
import game.bomber.communication.game.GClientToGServer;
import game.bomber.communication.game.GServerToGClients;
import game.bomber.game.exceptions.NoSuchServerException;
import game.bomber.game.exceptions.WrongUsernameException;

import java.awt.*;
import java.awt.event.*;

/**
 * @author KOlkiewicz
 */
public class NewServerFrame extends JFrame {
	private JLabel title, newServerTitle;
	private JLabel ip, port;
	private JLabel wins, time, nick;
	private JComboBox winsCB, timeCB;
	private JTextField nickTF;
	private JButton next, back;
	private Integer[] winsOption, timeOption;
	private JPanel winsCBJp, timeCBJp, nextJp, backJp;
	private JFrame parent;
	private GServerToGClients gServerToGClients;
	public NewServerFrame(final JFrame parent, final LanguagePack language) {
		gServerToGClients = new GServerToGClients(5001, 5002, 5003, 5004, 5005);
		setTitle(language.newServerFrameWindowName);
		title = new JLabel(language.newServerFrameTitle);
		newServerTitle = new JLabel(language.newServerFrameNewServerTitle);
		ip = new JLabel(gServerToGClients.getIP());
		port = new JLabel(gServerToGClients.getActionPort());
		wins = new JLabel(language.newServerFrameWins);
		time = new JLabel(language.newServerFrameTime);
		nick = new JLabel(language.newServerFrameNick);
		winsOption = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		timeOption = new Integer[11];
		for (int i = 0; i <= 10; i++) {
			timeOption[i] = i;
		}
		winsCB = new JComboBox(winsOption);
		timeCB = new JComboBox(timeOption);
		nickTF = new JTextField(20);
		next = new JButton(language.newServerFrameNext);
		back = new JButton(language.newServerFrameBack);
		winsCBJp = new JPanel();
		timeCBJp = new JPanel();
		nextJp = new JPanel();
		backJp = new JPanel();
		this.parent = parent;
		
		next.setPreferredSize(new Dimension(140, 40));
		back.setPreferredSize(new Dimension(140, 40));

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(10,10,0,10);
		add(title, gbc);

		gbc.gridy++;
		add(newServerTitle, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(5,10,0,10);
		add(ip, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0,10,25,10);
		add(port, gbc);

		gbc.gridy++;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(0,10,0,10);
		gbc.weightx=1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		//gbc.anchor = GridBagConstraints.LINE_START;
		add(wins, gbc);

		gbc.gridy++;
		add(time, gbc);

		gbc.gridy = 4;
		gbc.gridx++;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.LINE_START;
		winsCB.setPreferredSize(new Dimension(150, timeCB.getPreferredSize().height));
		winsCB.setSelectedIndex(2);
		winsCBJp.add(winsCB);
		add(winsCBJp, gbc);
		
		gbc.gridy++;
		timeCB.setPreferredSize(new Dimension(150, timeCB.getPreferredSize().height));
		timeCBJp.add(timeCB);
		add(timeCBJp, gbc);
		
		gbc.gridy++;
		gbc.gridx--;
		gbc.insets = new Insets(10,10,5,10);
		//gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.CENTER;
		add(nick, gbc);
		
		gbc.gridx++;
		gbc.anchor = GridBagConstraints.LINE_START;
		add(nickTF, gbc);
		
		gbc.gridy++;
		gbc.gridx--;
		gbc.gridwidth=2;
		gbc.weighty=1;
		gbc.insets = new Insets(0,10,0,10);
		gbc.anchor = GridBagConstraints.PAGE_END;
		nextJp.add(next);
		add(nextJp, gbc);
		
		gbc.gridy++;
		gbc.insets = new Insets(0,10,10,10);
		gbc.weighty=0;
		backJp.add(back);
		add(backJp, gbc);

		setSize(400, 550);
		addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
            	gServerToGClients.close();
            	parent.setVisible(true);
				dispose();
            }
        });
		setVisible(true);
		setLocationRelativeTo(parent);
		
		next.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				PlayerSimple player = new PlayerSimple(-1, nickTF.getText());
				GClientToGServer gClientToGServer = null;
				try {
					gClientToGServer = new GClientToGServer(gServerToGClients.getIP(),
							Integer.valueOf(gServerToGClients.getActionPort()),
							Integer.valueOf(gServerToGClients.getEventPort()),
							Integer.valueOf(gServerToGClients.getGameConfigPort()),
							Integer.valueOf(gServerToGClients.getMatchConfigPort()),
							Integer.valueOf(gServerToGClients.getPlayerPort()), player, 1);
				} catch (WrongUsernameException | NoSuchServerException e1) {
					e1.printStackTrace();
				}
				new GameStarterFrame(getJFrame(), language, null, gClientToGServer, gServerToGClients, nickTF.getText());
				setVisible(false);
			}
		});
		back.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				gServerToGClients.close();
				parent.setVisible(true);
				dispose();
			}
		});
	}
	
	public void changeServerInfo(String ip, int port){
		this.ip.setText(ip);
		this.port.setText(String.valueOf(port));
	}
	public void changeLanguage(LanguagePack language){
		setTitle(language.newServerFrameWindowName);
		title.setText(language.newServerFrameTitle);
		newServerTitle.setText(language.newServerFrameNewServerTitle);
		wins.setText(language.newServerFrameWins);
		time.setText(language.newServerFrameTime);
		nick.setText(language.newServerFrameNick);
		next.setText(language.newServerFrameNext);
		back.setText(language.newServerFrameBack);
	}
	private NewServerFrame getJFrame(){
		return this;
	}
}
