package game.bomber.app;

import javax.swing.*;

import game.bomber.common.Request;
import game.bomber.common.Response;
import game.bomber.communication.global.MClientCommunicator;

import java.awt.*;
import java.awt.event.*;

/**
 * @author KOlkiewicz
 */
public class OpeningFrame extends JFrame implements LanguageChanger {
	private JButton registration, login, play, options, exit;
	private JLabel title;
	private JPanel jp, playJp, optionsJp, exitJp;
	private MClientCommunicator clientCommunicator;

	public OpeningFrame(final LanguagePack language, MClientCommunicator communicator) {
		clientCommunicator = communicator;
		setTitle(language.openingFrameWindowName);
		jp = new JPanel();
		playJp = new JPanel();
		optionsJp = new JPanel();
		exitJp = new JPanel();
		registration = new JButton(language.openingFrameRegistration);
		login = new JButton(language.openingFrameLogin);
		play = new JButton(language.openingFramePlay);
		options = new JButton(language.openingFrameOptions);
		exit = new JButton(language.openingFrameExit);
		title = new JLabel(language.openingFrameTitle);

		play.setPreferredSize(new Dimension(140, 40));
		options.setPreferredSize(new Dimension(140, 40));
		exit.setPreferredSize(new Dimension(140, 40));

		jp.setLayout(new FlowLayout());
		jp.add(registration);
		jp.add(login);
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;

		gbc.insets = new Insets(0, 390 - jp.getPreferredSize().width, 10, 5);
		add(jp, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(10, 0, 270, 0);
		add(title, gbc);

		gbc.gridy++;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(0, 0, 5, 0);
		gbc.weighty = 0.5;
		playJp.add(play);
		add(playJp, gbc);

		gbc.gridy++;
		optionsJp.add(options);
		add(optionsJp, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0, 0, 15, 0);
		exitJp.add(exit);
		add(exitJp, gbc);

		setSize(400, 550);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (clientCommunicator.isConnected()) {
					Response response = clientCommunicator.sendRequest(new Request(
							Request.REQUEST.DISCONNECT, null));
				}
				dispose();
			}
		});
		setVisible(true);
		setLocationRelativeTo(null);

		registration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new RegistrationFrame(getJFrame(), language, clientCommunicator);
				setVisible(false);
			}
		});
		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new LoginFrame(getJFrame(), language, clientCommunicator);
				setVisible(false);
			}
		});
		play.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new LANFrame(getJFrame(), language, null);
				setVisible(false);
			}
		});
		options.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new OptionFrame(getJFrame(), language);
				setVisible(false);
				// dispose();
			}
		});
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Response response = clientCommunicator.sendRequest(new Request(
						Request.REQUEST.DISCONNECT, null));
				//clientCommunicator.close();
				dispose();

			}
		});
	}

	public LanguageChanger changeLanguage(LanguagePack language) {
		OpeningFrame openingFrame = new OpeningFrame(language,
				clientCommunicator);
		openingFrame.setVisible(false);
		dispose();
		return openingFrame;
	}

	private OpeningFrame getJFrame() {
		return this;
	}
}
