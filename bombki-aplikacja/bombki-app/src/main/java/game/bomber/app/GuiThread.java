package game.bomber.app;

import java.awt.EventQueue;

import game.bomber.communication.global.MClientCommunicator;

import javax.swing.UIManager;

/**
 * @author KOlkiewicz
 */
public class GuiThread {
	OpeningFrame openingFrame;
	LanguagePack language;
	MClientCommunicator clientCommunicator;
	
	
	public GuiThread() {
		language = new Polish();
		clientCommunicator = new MClientCommunicator("127.0.0.1", 3001);
		openingFrame = new OpeningFrame(language, clientCommunicator);
		
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}
		EventQueue.invokeLater(new Runnable(){
			@Override
			public void run() {
				new GuiThread();
				
			}			
		});	
	}

}
