package game.bomber.app;

/**
 * @author KOlkiewicz
 */
public class English extends LanguagePack {

    public English() {
        languageName = "English";

        openingFrameRegistration = "Register";
        openingFrameLogin = "Log in";
        openingFramePlay = "<html><font size=+1 face=\"arial narrow\">Play via LAN";
        openingFrameOptions = "<html><font size=+1 face=\"arial narrow\">Options";
        openingFrameExit = "<html><font size=+1 face=\"arial narrow\">Exit";
        openingFrameTitle = "<html><font size=+2>BOMBKI";
        openingFrameWindowName = "Bombki";

        mainFrameAccount = "Account management";
        mainFrameLogout = "Log out";
        mainFrameTitle = "<html><font size=+2>BOMBKI";
        mainFramePlayNow = "<html><font size=+1 face=\"arial narrow\">Play now!";
        mainFrameInvite = "<html><font size=+1 face=\"arial narrow\">Invite players";
        mainFramePlayers = "<html><font size=+1 face=\"arial narrow\">Players online";
        mainFramePlayLan = "<html><font size=+1 face=\"arial narrow\">Play via LAN";
        mainFrameOptions = "<html><font size=+1 face=\"arial narrow\">Options";
        mainFrameExit = "<html><font size=+1 face=\"arial narrow\">Exit";
        mainFrameWindowName = "Bombki";
        mainFrameSearchPlayerDialogSearchPlayers = "<html><font size=+2>Looking for players";
        mainFrameSearchPlayerDialogWait = "Please wait";
        mainFrameSearchPlayerDialogExit = "<html><font size=+1 face=\"arial narrow\">Exit";
        mainFrameSearchPlayerDialogWindowName = "Looking for players";
        mainFrameShowInvitationReject = "Reject";
        mainFrameShowInvitationAccept = "Accept";
        mainFrameShowInvitationMessage1 = "You have been invited to the game(up to ";
        mainFrameShowInvitationMessage2 = " wins) by: ";
        mainFrameShowInvitationWindowName = "Invitation";

        registrationFrameTitle = "<html><font size=+2>BOMBKI";
        registrationFrameRegistration = "<html><font size=+2>Registration";
        registrationFrameUsername = "User Name";
        registrationFramePassword1 = "Password";
        registrationFramePassword2 = "Repeat password";
        registrationFrameQuestion = "Your question:";
        registrationFrameAnswer = "Answer:";
        registrationFrameSend = "<html><font size=+1 face=\"arial narrow\">Send";
        registrationFrameBack = "<html><font size=+1 face=\"arial narrow\">Back";
        registrationFrameWindowName = "Registration";
        registrationFrameShowSuccessNext = "Next";
        registrationFrameShowSuccessMessage = "Registration successful!";
        registrationFrameShowSuccessWindowName = "Success";
        registrationFrameShowErrorBack = "Back";
        registrationFrameShowErrorMessage0 = "Invalid user name!\n(User name may only contain alphanumeric characters and an underscore\nand needs to have at least 6 characters).";
        registrationFrameShowErrorMessage1 = "Username is already taken.";
        registrationFrameShowErrorMessage2 = "Password is too short.\n(Password should contain at least 8 characters).";
        registrationFrameShowErrorMessage3 = "Passwords do not match.";
        registrationFrameShowErrorMessage4 = "Question cannot be empty.";
        registrationFrameShowErrorMessage5 = "Answer to question cannot be empty.";
        registrationFrameShowErrorMessage6 = "Unable to connect to the server, please try again later.";
        registrationFrameShowErrorWindowName = "Error";

        loginFrameTitle = "<html><font size=+2>BOMBKI";
        loginFrameLoginTitle = "<html><font size=+2>Login";
        loginFrameUsername = "User name";
        loginFramePassword = "Password";
        loginFrameLogin = "<html><font size=+1 face=\"arial narrow\">Login";
        loginFrameRecover = "<html><font size=\"4\" face=\"arial narrow\">Recover password";
        loginFrameBack = "<html><font size=+1 face=\"arial narrow\">Back";
        loginFrameWindowName = "Login";
        loginFrameShowErrorMessage0 = "Invail username or password!";
        loginFrameShowErrorMessage1 = "Unable to connect to the server, please try again later.";
        loginFrameShowErrorBack = "Back";
        loginFrameShowErrorWindowName = "B��d";
        loginFrameRecoveryDialogTitle = "<html><font size=+2>Retrieving password";
        loginFrameRecoveryDialogUsername = "Enter your user name";
        loginFrameRecoveryDialogNext = "<html><font size=+1 face=\"arial narrow\">Next";
        loginFrameRecoveryDialogWindowName = "Password recovery";
        loginFrameRecoveryDialogShowSuccessTitle = "Success";
        loginFrameRecoveryDialogShowSuccessMessage = "Your new password is: ";
        LoginFrameRecoveryDialogShowQuestionWindowName = "Question";
        LoginFrameRecoveryDialogShowErrorMessage = "There is no such user.";
        LoginFrameRecoveryDialogShowErrorWindowName = "Error";
        LoginFrameRecoveryDialogShowWrongAnswerMessage = "Wrong answer!";
        LoginFrameRecoveryDialogShowWrongAnswerWindowName = "Error";

        optionFrameTitle = "<html><font size=+2>BOMBKI";
        optionFrameOptionTitle = "<html><font size=+2>Options";
        optionFrameLanguageLabel = "Language";
        optionFrameSave = "<html><font size=+1 face=\"arial narrow\">Save";
        optionFrameBack = "<html><font size=+1 face=\"arial narrow\">Back";
        optionFrameWindowName = "Options";

        LANFrameTitle = "<html><font size=+2>BOMBKI";
        LANFrameLANTitle = "<html><font size=+2>Play via LAN";
        LANFrameCreate = "<html><font size=+1 face=\"arial narrow\">Create server";
        LANFrameJoin = "<html><font size=+1 face=\"arial narrow\">Join the game";
        LANFrameBack = "<html><font size=+1 face=\"arial narrow\">Back";
        LANFrameWindowName = "Play via LAN";

        newServerFrameTitle = "<html><font size=+2>BOMBKI";
        newServerFrameNewServerTitle = "<html><font size=+2>New LAN server";
        newServerFrameWins = "Do ilu wygranych";
        newServerFrameTime = "Time limit per round";
        newServerFrameNick = "Your nick";
        newServerFrameNext = "<html><font size=+1 face=\"arial narrow\">Next";
        newServerFrameBack = "<html><font size=+1 face=\"arial narrow\">Back";
        newServerFrameWindowName = "New LAN server";

        joinServerFrameTitle = "<html><font size=+2>BOMBKI";
        joinServerFrameJoinTitle = "<html><font size=+2>Join the game";
        joinServerFrameIp = "IP";
        joinServerFramePort = "Port";
        joinServerFrameNick = "Your nick";
        joinServerFrameConnect = "<html><font size=+1 face=\"arial narrow\">Connect";
        joinServerFrameBack = "<html><font size=+1 face=\"arial narrow\">Back";
        joinServerFrameWindowName = "Join the game";
        joinServerFrameShowErrorMessage1 = "Username is already taken";
        joinServerFrameShowErrorMessage2 = "Unable to connect to the server.";
        joinServerFrameShowErrorBack = "Back";
        joinServerFrameShowErrorWindowName = "Error";

        newGameFrameTitle = "<html><font size=+2>BOMBKI";
        newGameFrameNewGameTitle = "<html><font size=+2>New game";
        newGameFrameWins = "Do ilu wygranych";
        newGameFrameTime = "Time limit per round";
        newGameFramePlayer = "Player";
        newGameFrameSend = "Send";
        newGameFrameList = "<html><font size=+1 face=\"arial narrow\">List of players";
        newGameFrameNext = "<html><font size=+1 face=\"arial narrow\">Next";
        newGameFrameBack = "<html><font size=+1 face=\"arial narrow\">Back";
        newGameFrameMinutes = "minutes";
        newGameFrameWindowName = "New Game";

        invitingFrameTitle = "<html><font size=+2>BOMBKI";
        invitingFrameInviteTitle = "<html><font size=+2>Invite players</html>";
        invitingFrameWaitingStatus = "waiting";
        invitingFramePlayingStatus = "playing";
        invitingFrameFreeStatus = "available";
        invitingFrameMeStatus = "<html><font color=\"red\">me";
        invitingFrameColumnNames1 = "Nick";
        invitingFrameColumnNames2 = "Status";
        invitingFrameColumnNames3 = "Actions ";
        invitingFrameColumnNames4 = "Invite   ";
        invitingFrameStats = "Statistics";
        invitingFrameInvite = "Invite";
        invitingFrameBack = "<html><font size=+1 face=\"arial narrow\">Back";
        invitingFrameWindowName = "Invite players";

        playersOnlineFrameTitle = "<html><font size=+2>BOMBKI";
        playersOnlineFramePlayersTitle = "<html><font size=+2>Players online";
        playersOnlineFrameWaitingStatus = "waiting";
        playersOnlineFramePlayingStatus = "playing";
        playersOnlineFrameFreeStatus = "available";
        playersOnlineFrameMeStatus = "<html><font color=\"red\">me";
        playersOnlineFrameColumnNames1 = "Nick";
        playersOnlineFrameColumnNames2 = "Status";
        playersOnlineFrameColumnNames3 = "Actions";
        playersOnlineFrameStats = "Statistics";
        playersOnlineFrameBack = "<html><font size=+1 face=\"arial narrow\">Back";
        playersOnlineFrameWindowName = "Players online";

        statisticsFrameTitle = "<html><font size=+2>BOMBKI";
        statisticsFrameStatsTitle = "<html><font size=+2>Statistics";
        statisticsFramePlayer = "Player:";
        statisticsFramePosition1 = "1st place";
        statisticsFramePosition2 = "2nd place";
        statisticsFramePosition3 = "3rd place";
        statisticsFramePosition4 = "4th place";
        statisticsFrameWins = "Rounds won:";
        statisticsFrameLoses = "Rounds lost:";
        statisticsFramePoints = "Points:";
        statisticsFrameRank = "Rank:";
        statisticsFrameBack = "<html><font size=+1 face=\"arial narrow\">Back";
        statisticsFrameWindowName = "Statistics";

        gameStarterFrameTitle = "<html><font size=+2>BOMBKI";
        gameStarterFrameWaitingTitle = "<html><font size=+2>Waiting for the start of the game";
        gameStarterFrameReady = "Ready";
        gameStarterFrameNotReady = "Not ready";
        gameStarterFrameExit = "<html><font size=+1 face=\"arial narrow\">Exit";
        gameStarterFrameWindowName = "Waiting for the start of the game";
    }
}
