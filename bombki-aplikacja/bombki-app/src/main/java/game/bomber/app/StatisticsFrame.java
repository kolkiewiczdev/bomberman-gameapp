package game.bomber.app;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.Map;

/**
 * @author KOlkiewicz
 */
public class StatisticsFrame extends JFrame {
	private JLabel title, statsTitle;
	private JLabel player, position1, position2, position3, position4, wins, loses,
			points, rank;
	private JLabel playerValue, position1Value, position2Value, position3Value,
			position4Value, winsValue, losesValue, pointsValue, rankValue;
	private JButton back;
	private JPanel backJp;
	private JFrame parent;

	public StatisticsFrame(final JFrame parent, LanguagePack language, String username, Map <String,String> params) {
		setTitle(language.statisticsFrameWindowName);
		title = new JLabel(language.statisticsFrameTitle);
		statsTitle = new JLabel(language.statisticsFrameStatsTitle);
		player = new JLabel(language.statisticsFramePlayer);
		position1 = new JLabel(language.statisticsFramePosition1);
		position2 = new JLabel(language.statisticsFramePosition2);
		position3 = new JLabel(language.statisticsFramePosition3);
		position4 = new JLabel(language.statisticsFramePosition4);
		wins = new JLabel(language.statisticsFrameWins);
		loses = new JLabel(language.statisticsFrameLoses);
		points = new JLabel(language.statisticsFramePoints);
		rank = new JLabel(language.statisticsFrameRank);
		back = new JButton(language.statisticsFrameBack);
		playerValue = new JLabel(username);
		position1Value = new JLabel(params.get("pozycja 1"));
		position2Value = new JLabel(params.get("pozycja 2"));
		position3Value = new JLabel(params.get("pozycja 3"));
		position4Value = new JLabel(params.get("pozycja 4"));
		winsValue = new JLabel(params.get("wygrane rundy"));
		losesValue = new JLabel(params.get("przegrane rundy"));
		pointsValue = new JLabel(params.get("punkty"));
		rankValue = new JLabel(params.get("pozycja w rankingu"));
		backJp = new JPanel();
		this.parent = parent;

		back.setPreferredSize(new Dimension(140, 40));

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		gbc.insets = new Insets(10,10,0,10);
		add(title, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(10,10,30,10);
		add(statsTitle, gbc);

		gbc.gridy++;
		gbc.gridwidth = 1;
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.insets = new Insets(0,10,30,10);
		add(player, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0,10,10,10);
		add(position1, gbc);

		gbc.gridy++;
		add(position2, gbc);

		gbc.gridy++;
		add(position3, gbc);

		gbc.gridy++;
		add(position4, gbc);

		gbc.gridy++;
		add(wins, gbc);

		gbc.gridy++;
		add(loses, gbc);

		gbc.gridy++;
		add(points, gbc);

		gbc.gridy++;
		add(rank, gbc);
		
		gbc.gridy = 2;
		gbc.gridx++;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(0,0,30,0);
		add(playerValue, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(0,0,10,0);
		add(position1Value, gbc);

		gbc.gridy++;
		add(position2Value, gbc);

		gbc.gridy++;
		add(position3Value, gbc);

		gbc.gridy++;
		add(position4Value, gbc);

		gbc.gridy++;
		add(winsValue, gbc);

		gbc.gridy++;
		add(losesValue, gbc);

		gbc.gridy++;
		add(pointsValue, gbc);

		gbc.gridy++;
		add(rankValue, gbc);
		
		gbc.gridy = 2;
		gbc.gridx++;
		gbc.insets = new Insets(0,0,30,0);
		gbc.weightx = 1;
		gbc.gridheight=9;
		add(new JPanel(), gbc);

		gbc.gridy=11;
		gbc.gridx = 0;
		gbc.gridheight=1;
		gbc.gridwidth = 3;
		gbc.weighty = 1;
		gbc.insets = new Insets(0,0,10,0);
		gbc.anchor = GridBagConstraints.PAGE_END;
		backJp.add(back);
		add(backJp, gbc);

		setSize(400, 550);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				parent.setVisible(true);
				dispose();
			}
		});
		setVisible(true);
		setLocationRelativeTo(parent);
		
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.setVisible(true);
				dispose();
				
			}
		});
	}

	public void changeLanguage(LanguagePack language){
		setTitle(language.statisticsFrameWindowName);
		title.setText(language.statisticsFrameTitle);
		statsTitle.setText(language.statisticsFrameStatsTitle);
		player.setText(language.statisticsFramePlayer);
		position1.setText(language.statisticsFramePosition1);
		position2.setText(language.statisticsFramePosition2);
		position3.setText(language.statisticsFramePosition3);
		position4.setText(language.statisticsFramePosition4);
		wins.setText(language.statisticsFrameWins);
		loses.setText(language.statisticsFrameLoses);
		points.setText(language.statisticsFramePoints);
		rank.setText(language.statisticsFrameRank);
		back.setText(language.statisticsFrameBack);
	}
}
