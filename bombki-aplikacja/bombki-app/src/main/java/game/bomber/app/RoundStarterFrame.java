package game.bomber.app;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;

/**
 * @author KOlkiewicz
 */
public class RoundStarterFrame extends JFrame {
	private JLabel title, waitingTitle, wins;
	private JButton ready, exit;
	private JPanel exitJp, panel;
	private LinkedList<UserBeforeNewRound> users;

	public RoundStarterFrame(LanguagePack language,
			LinkedList<UserBeforeNewRound> usernames) {
		setTitle(language.roundStarterFrameWindowName);
		title = new JLabel(language.roundStarterFrameTitle);
		waitingTitle = new JLabel(language.roundStarterFrameWaitingTitle);
		wins = new JLabel(language.roundStarterFrameWins);
		ready = new JButton(language.roundStarterFrameReady);
		exit = new JButton(language.roundStarterFrameExit);
		users = usernames;
		exitJp = new JPanel();
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());

		exit.setPreferredSize(new Dimension(140, 40));

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 10, 0, 10);
		add(title, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(10, 10, 20, 10);
		add(waitingTitle, gbc);

		gbc.gridy++;
		gbc.insets = new Insets(10, 10, 10, 10);
		gbc.weightx = 0.5;
		gbc.anchor = GridBagConstraints.LINE_END;
		add(wins, gbc);

		gbc.gridy++;
		gbc.weighty = 1;
		gbc.weightx = 1;
		gbc.anchor = GridBagConstraints.PAGE_START;
		add(panel, gbc);

		gbc.gridy++;
		gbc.weighty = 0;
		gbc.weightx = 0;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(10, 10, 10, 10);
		exitJp.add(exit);
		add(exitJp, gbc);

		setSize(400, 550);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
		setLocationRelativeTo(null);
		refresh(language);
	}

	public void refresh(final LanguagePack language) {
		panel.removeAll();
		if (users.size() > 0) {
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.insets = new Insets(5, 20, 5, 20);
			for (int i = 0; i < users.size(); i++) {
				gbc.gridx=0;
				if (i == users.size() - 1) {
					gbc.weighty = 1;
				}
				JPanel nick = new JPanel();
				nick.setLayout(new GridBagLayout());
				nick.add(new JLabel(users.get(i).getUsername()));
				panel.add(nick, gbc);

				if (i == users.size() - 1) {
					gbc.anchor = GridBagConstraints.PAGE_START;
				}
				gbc.gridx++;
				Icon icon = new ImageIcon("wait.gif");

				int tmp = 0;
				switch (users.get(i).getStatus()) {
				case UserBeforeNewRound.WAITING:
					icon = new ImageIcon("wait.gif");
					tmp = 1;
					break;
				case UserBeforeNewRound.READY:
					icon = new ImageIcon("accept.png");
					tmp = 2;
					break;
				case UserBeforeNewRound.ERROR:
					icon = new ImageIcon("reject.png");
					tmp = 3;
					break;
				}
				JLabel iconLabel = new JLabel(icon);
				panel.add(iconLabel, gbc);

				gbc.gridx++;
				if (users.get(i).isOwner()) {
					if (tmp == 1) {
						JButton ready = new JButton(
								language.roundStarterFrameReady);
						ready.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								int x;
								for (x = 0; x < users.size(); x++) {
									if (users.get(x).isOwner()) {
										users.get(x).setStatus(
												UserBeforeNewRound.READY);
									}
								}
								refresh(language);
							}
						});
						panel.add(ready, gbc);
					}
					if (tmp == 2) {
						JButton notReady = new JButton(
								language.roundStarterFrameNotReady);
						notReady.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								int x;
								for (x = 0; x < users.size(); x++) {
									if (users.get(x).isOwner()) {
										users.get(x).setStatus(
												UserBeforeNewRound.WAITING);
									}
								}
								refresh(language);
							}
						});
						panel.add(notReady, gbc);
					}

				}

				gbc.gridx++;
				JPanel win = new JPanel();
				win.setLayout(new GridBagLayout());
				win.add(new JLabel(String.valueOf(users.get(i).getWins())));
				panel.add(win, gbc);

				gbc.gridy++;
			}
		}
		panel.revalidate();
	}
}
